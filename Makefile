#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

PYTHON=python3
TREE=HEAD
PROGRAM=python3-hwloc

VERSION := $(shell rpm -q --qf '%{VERSION} ' --specfile $(PROGRAM).spec | cut -d' ' -f1)
ifndef VERSION
version=$(shell sed -n '/^Version:\s\+/s/^Version:\s\+\(.[0-9]\)/\1/p' < $(PROGRAM).spec)
VERSION=$(shell /bin/echo $(version))
endif
ifndef VERSION
$(error VERSION must be set!)
endif

RELEASE := $(shell rpmspec -q --qf '%{RELEASE}' --srpm $(PROGRAM).spec | sed -e 's/\.centos$$//' -e 's/\.[^.]*$$//')

RPMDIRS=$(addprefix rpm/, SPECS SOURCES BUILD RPMS SRPMS)

DEB_BUILDDIR=build_debian
DEB_TARBALL=$(PROGRAM)_$(VERSION).orig.tar.gz
DEB_SRCDIR=$(DEB_BUILDDIR)/$(PROGRAM)-$(VERSION)

setup: src/chwloc.pyx src/linuxsched.pyx
	$(PYTHON) ./setup.py build_ext --inplace

$(RPMDIRS):
	mkdir -p $(RPMDIRS) || :

gz: $(RPMDIRS)
	git archive --format=tar --prefix=$(PROGRAM)-$(VERSION)-$(RELEASE)/ $(TREE) | gzip -9 > rpm/SOURCES/$(PROGRAM)-$(VERSION)-$(RELEASE).tar.gz

rpm: gz
	rpmbuild -ba --define "_topdir $(PWD)/rpm" --define "_source_filedigest_algorithm 1" --define "_binary_filedigest_algorithm 1" --define "_binary_payload w9.gzdio" --define "_source_payload w9.gzdio" $(PROGRAM).spec

srpm: gz
	rpmbuild -bs --define "_topdir $(PWD)/rpm" --define "_source_filedigest_algorithm 1" --define "_binary_filedigest_algorithm 1" --define "_binary_payload w9.gzdio" --define "_source_payload w9.gzdio" $(PROGRAM).spec

$(DEB_BUILDDIR):
	mkdir -p $@

$(DEB_BUILDDIR)/$(DEB_TARBALL): $(DEB_BUILDDIR)
	git archive --format=tar --prefix=$(PROGRAM)-$(VERSION)/ HEAD | gzip -9 > $(DEB_BUILDDIR)/$(DEB_TARBALL)

$(DEB_SRCDIR): $(DEB_BUILDDIR)/$(DEB_TARBALL)
	cd $(DEB_BUILDDIR); tar -xf $(DEB_TARBALL)

deb: $(DEB_SRCDIR)
	cd $(DEB_SRCDIR); dpkg-buildpackage -us -uc

clean:
	rm -rf rpm build src/linuxsched.c src/hwloc/*.so src/chwloc.c doc/guide.pdf doc/.guide.md.html dist \
	 python*_hwloc.egg-info $(DEB_BUILDDIR)
	find . -type f \( -name \*~ -o -name \*.pyc -o -name \*.o \) -delete

git-tag:
	git tag -s $(VERSION)-$(RELEASE)

doc/guide.html: doc/guide.md
	pandoc -o doc/guide.html $<

doc/guide.pdf: doc/guide.md
	pandoc -o doc/guide.pdf -V fontfamily:utopia --toc $<

doc: doc/guide.html doc/guide.pdf
