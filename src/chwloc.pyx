# -*- cython -*-
# cython: language_level=2

#
# Copyright 2019-2020 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

DEF WITH_x86_64 = UNAME_MACHINE in [u'x86_64']

__author = 'Guy Streeter <guy.streeter@gmail.com>'
__license = 'GPLv2'
__version = '2.2.0'
__description = 'Python bindings for hwloc version 2'
__URL = 'https://gitlab.com/guystreeter/python3-hwloc.git'

from libc.stdio cimport FILE
from posix.unistd cimport pid_t
from libc.stdio cimport fopen as CFopen
from libc.stdio cimport fclose as CFclose
from libc.errno cimport errno as CErrno
from libc.string cimport strerror as bCStrerror
from libc.string cimport memcpy as CMemcpy
from libc.string cimport strdup as CStrdup
from libc.stdlib cimport malloc as CMalloc
from libc.stdlib cimport free as CFree

def _utfate(text):
	if isinstance(text, unicode):
		return text.encode('utf8')
	raise ValueError('requires text input, got %s' % type(text))

def CStrerror(code):
	return str(bCStrerror(code).decode('utf8'))

class HwlocError(Exception):
	pass

class LoadError(HwlocError):
	pass

class NULLError(HwlocError):
	pass

class ArgError(HwlocError):
	"""Exception raised when a calling argument is invalid"""
	def __init__(self, string):
		self.msg = string
	def __str__(self):
		return self.msg

cdef extern from "limits.h":
	enum: INT_MAX
	cdef unsigned int UINT_MAX
	cdef unsigned long ULONG_MAX

HWLOC_INT_MAX = INT_MAX
HWLOC_UINT_MAX = UINT_MAX
HWLOC_ULONG_MAX = ULONG_MAX

cdef extern from "stdint.h":
	ctypedef int uint64_t

cdef extern from "pthread.h":
	ctypedef unsigned long int pthread_t

# getting around anonymous struct/union stuff
cdef extern from 'hwloc.h':
	cdef struct hwloc_pcidev_attr_s:
		unsigned short domain
		unsigned char bus, dev, func
		unsigned short class_id
		unsigned short vendor_id, device_id, subvendor_id, subdevice_id
		unsigned char revision
		float linkspeed

cdef union ba_upstream:
	hwloc_pcidev_attr_s pci
cdef struct ba_ds_pci:
	unsigned short domain
	unsigned char secondary_bus, subordinate_bus
cdef union ba_downstream:
	ba_ds_pci pci

cdef extern from 'hwloc.h':

####
# hwlocality_bitmap The bitmap API
####
	cdef struct hwloc_bitmap_s:
		pass
	ctypedef hwloc_bitmap_s* hwloc_bitmap_t
	ctypedef const hwloc_bitmap_s* hwloc_const_bitmap_t

	hwloc_bitmap_t hwloc_bitmap_alloc()
	hwloc_bitmap_t hwloc_bitmap_alloc_full()
	void hwloc_bitmap_free(hwloc_bitmap_t bitmap)
	hwloc_bitmap_t hwloc_bitmap_dup(hwloc_const_bitmap_t bitmap)
	void hwloc_bitmap_copy(hwloc_bitmap_t dst, hwloc_const_bitmap_t src)

	int hwloc_bitmap_asprintf(char** strp, hwloc_const_bitmap_t bitmap)
	int hwloc_bitmap_sscanf(hwloc_bitmap_t bitmap, const char* string)
	int hwloc_bitmap_list_asprintf(char** strp, hwloc_const_bitmap_t bitmap)
	int hwloc_bitmap_list_sscanf(hwloc_bitmap_t bitmap, const char* string)
	int hwloc_bitmap_taskset_asprintf(char** strp, hwloc_const_bitmap_t bitmap)
	int hwloc_bitmap_taskset_sscanf(hwloc_bitmap_t bitmap, const char* string)

	void hwloc_bitmap_zero(hwloc_bitmap_t bitmap)
	void hwloc_bitmap_fill(hwloc_bitmap_t bitmap)
	void hwloc_bitmap_only(hwloc_bitmap_t bitmap, unsigned id1)
	void hwloc_bitmap_allbut(hwloc_bitmap_t bitmap, unsigned id1)
	void hwloc_bitmap_from_ith_ulong(hwloc_bitmap_t bitmap, unsigned i,
									 unsigned long mask)

	void hwloc_bitmap_set(hwloc_bitmap_t bitmap, unsigned id1)
	void hwloc_bitmap_set_range(hwloc_bitmap_t bitmap, unsigned begin, int end)
	void hwloc_bitmap_set_ith_ulong(hwloc_bitmap_t bitmap, unsigned i,
									unsigned long mask)
	void hwloc_bitmap_clr(hwloc_bitmap_t bitmap, unsigned id1)
	void hwloc_bitmap_clr_range(hwloc_bitmap_t bitmap, unsigned begin, int end)
	void hwloc_bitmap_singlify(hwloc_bitmap_t bitmap)

	unsigned long hwloc_bitmap_to_ith_ulong(hwloc_const_bitmap_t bitmap, unsigned i)
	int hwloc_bitmap_isset(hwloc_const_bitmap_t bitmap, unsigned id1)
	int hwloc_bitmap_iszero(hwloc_const_bitmap_t bitmap)
	int hwloc_bitmap_isfull(hwloc_const_bitmap_t bitmap)
	int hwloc_bitmap_first(hwloc_const_bitmap_t bitmap)
	int hwloc_bitmap_next(hwloc_const_bitmap_t bitmap, int prev)
	int hwloc_bitmap_last(hwloc_const_bitmap_t bitmap)
	int hwloc_bitmap_weight(hwloc_const_bitmap_t bitmap)
	int hwloc_bitmap_first_unset(hwloc_const_bitmap_t bitmap)
	int hwloc_bitmap_next_unset(hwloc_const_bitmap_t bitmap, int prev)
	int hwloc_bitmap_last_unset(hwloc_const_bitmap_t bitmap)
	void hwloc_bitmap_or(hwloc_bitmap_t res, hwloc_const_bitmap_t bitmap1,
						 hwloc_const_bitmap_t bitmap2)
	void hwloc_bitmap_and(hwloc_bitmap_t res, hwloc_const_bitmap_t bitmap1,
						  hwloc_const_bitmap_t bitmap2)
	void hwloc_bitmap_andnot(hwloc_bitmap_t res, hwloc_const_bitmap_t bitmap1,
							 hwloc_const_bitmap_t bitmap2)
	void hwloc_bitmap_xor(hwloc_bitmap_t res, hwloc_const_bitmap_t bitmap1,
						  hwloc_const_bitmap_t bitmap2)
	void hwloc_bitmap_not(hwloc_bitmap_t res, hwloc_const_bitmap_t bitmap)
	int hwloc_bitmap_intersects(hwloc_const_bitmap_t bitmap1,
								hwloc_const_bitmap_t bitmap2)
	int hwloc_bitmap_isincluded(hwloc_const_bitmap_t sub_bitmap,
								hwloc_const_bitmap_t super_bitmap)
	int hwloc_bitmap_isequal(hwloc_const_bitmap_t bitmap1,
							 hwloc_const_bitmap_t bitmap2)
	int hwloc_bitmap_compare_first(hwloc_const_bitmap_t bitmap1,
								   hwloc_const_bitmap_t bitmap2)
	int hwloc_bitmap_compare(hwloc_const_bitmap_t bitmap1,
							 hwloc_const_bitmap_t bitmap2)

####
# API version
####
	enum: HWLOC_API_VERSION
	unsigned hwloc_get_api_version()

####
# Topology context
####
	cdef struct hwloc_topology:
		pass
	ctypedef hwloc_topology* hwloc_topology_t

####
# Object sets
####
	ctypedef hwloc_bitmap_t hwloc_cpuset_t
	ctypedef hwloc_const_bitmap_t hwloc_const_cpuset_t
	ctypedef hwloc_bitmap_t hwloc_nodeset_t
	ctypedef hwloc_const_bitmap_t hwloc_const_nodeset_t

####
# Topology Object types
####
	ctypedef enum hwloc_obj_type_t:
		HWLOC_OBJ_MACHINE
		HWLOC_OBJ_PACKAGE
		HWLOC_OBJ_CORE
		HWLOC_OBJ_PU
		HWLOC_OBJ_L1CACHE
		HWLOC_OBJ_L2CACHE
		HWLOC_OBJ_L3CACHE
		HWLOC_OBJ_L4CACHE
		HWLOC_OBJ_L5CACHE
		HWLOC_OBJ_L1ICACHE
		HWLOC_OBJ_L2ICACHE
		HWLOC_OBJ_L3ICACHE
		HWLOC_OBJ_GROUP
		HWLOC_OBJ_NUMANODE
		HWLOC_OBJ_BRIDGE
		HWLOC_OBJ_PCI_DEVICE
		HWLOC_OBJ_OS_DEVICE
		HWLOC_OBJ_MISC
		HWLOC_OBJ_TYPE_MAX
	cdef int HWLOC_OBJ_TYPE_MIN = HWLOC_OBJ_MACHINE
	ctypedef enum hwloc_obj_cache_type_t:
		HWLOC_OBJ_CACHE_UNIFIED
		HWLOC_OBJ_CACHE_DATA
		HWLOC_OBJ_CACHE_INSTRUCTION
	ctypedef enum hwloc_obj_bridge_type_t:
		HWLOC_OBJ_BRIDGE_HOST
		HWLOC_OBJ_BRIDGE_PCI
	ctypedef enum hwloc_obj_osdev_type_t:
		HWLOC_OBJ_OSDEV_BLOCK
		HWLOC_OBJ_OSDEV_GPU
		HWLOC_OBJ_OSDEV_NETWORK
		HWLOC_OBJ_OSDEV_OPENFABRICS
		HWLOC_OBJ_OSDEV_DMA
		HWLOC_OBJ_OSDEV_COPROC

	int hwloc_compare_types (hwloc_obj_type_t type_, hwloc_obj_type_t type2)
	cdef int HWLOC_TYPE_UNORDERED = INT_MAX
	cdef unsigned HWLOC_UNKNOWN_INDEX = <unsigned>-1

####
# Topology Objects
####
	cdef struct hwloc_memory_page_type_s:
		uint64_t size
		uint64_t count
	cdef struct hwloc_numanode_attr_s:
		uint64_t local_memory
		unsigned page_types_len
		hwloc_memory_page_type_s* page_types
	cdef struct hwloc_cache_attr_s:
		uint64_t size
		unsigned depth
		unsigned linesize
		int associativity
		hwloc_obj_cache_type_t type
	cdef struct hwloc_group_attr_s:
		unsigned depth
		unsigned kind
		unsigned subkind
	cdef struct hwloc_bridge_attr_s:
		ba_upstream upstream
		hwloc_obj_bridge_type_t upstream_type
		ba_downstream downstream
		hwloc_obj_bridge_type_t downstream_type
		unsigned depth
	cdef struct hwloc_osdev_attr_s:
		hwloc_obj_osdev_type_t type
	cdef union hwloc_obj_attr_u:
		hwloc_numanode_attr_s numanode
		hwloc_cache_attr_s cache
		hwloc_group_attr_s group
		hwloc_pcidev_attr_s pcidev
		hwloc_bridge_attr_s bridge
		hwloc_osdev_attr_s osdev
	cdef struct hwloc_info_s:
		char* name
		char* value
	cdef struct hwloc_obj:
		hwloc_obj_type_t type
		char* subtype
		unsigned os_index
		char* name
		uint64_t total_memory
		hwloc_obj_attr_u *attr
		int depth
		unsigned logical_index
		hwloc_obj* next_cousin
		hwloc_obj* prev_cousin
		hwloc_obj* parent
		unsigned sibling_rank
		hwloc_obj* next_sibling
		hwloc_obj* prev_sibling
		unsigned arity
		hwloc_obj** children
		hwloc_obj* first_child
		hwloc_obj* last_child
		int symmetric_subtree
		unsigned memory_arity
		hwloc_obj *memory_first_child
		unsigned io_arity
		hwloc_obj *io_first_child
		unsigned misc_arity
		hwloc_obj *misc_first_child
		hwloc_cpuset_t cpuset
		hwloc_cpuset_t complete_cpuset
		hwloc_nodeset_t nodeset
		hwloc_nodeset_t complete_nodeset
		hwloc_info_s* infos
		unsigned infos_count
		void *userdata
		uint64_t gp_index
	ctypedef hwloc_obj* hwloc_obj_t

	ctypedef pid_t hwloc_pid_t
	ctypedef pthread_t hwloc_thread_t

####
# Topology Creation and Destruction
####
	int hwloc_topology_init(hwloc_topology_t* topologyp)
	int hwloc_topology_load(hwloc_topology_t topology)
	void hwloc_topology_destroy(hwloc_topology_t topology)
	int hwloc_topology_dup(hwloc_topology_t *newtopology,
						   hwloc_topology_t oldtopology)
	int hwloc_topology_abi_check(hwloc_topology_t topology)
	void hwloc_topology_check(hwloc_topology_t topology)

####
# Object levels, depths and types
####
	unsigned int hwloc_topology_get_depth(hwloc_topology_t topology)
	int hwloc_get_type_depth(hwloc_topology_t topology, hwloc_obj_type_t type_)

	cdef enum hwloc_get_type_depth_e:
		HWLOC_TYPE_DEPTH_UNKNOWN = -1
		HWLOC_TYPE_DEPTH_MULTIPLE = -2
		HWLOC_TYPE_DEPTH_NUMANODE = -3
		HWLOC_TYPE_DEPTH_BRIDGE = -4
		HWLOC_TYPE_DEPTH_PCI_DEVICE = -5
		HWLOC_TYPE_DEPTH_OS_DEVICE = -6
		HWLOC_TYPE_DEPTH_MISC = -7

	int hwloc_get_memory_parents_depth (hwloc_topology_t topology)
	int hwloc_get_type_or_below_depth(hwloc_topology_t topology,
									  hwloc_obj_type_t type_)
	int hwloc_get_type_or_above_depth(hwloc_topology_t topology,
									  hwloc_obj_type_t type_)
	hwloc_obj_type_t hwloc_get_depth_type(hwloc_topology_t topology, unsigned depth)
	unsigned hwloc_get_nbobjs_by_depth(hwloc_topology_t topology, int depth)
	int hwloc_get_nbobjs_by_type(hwloc_topology_t topology, hwloc_obj_type_t type_)
	hwloc_obj_t hwloc_get_root_obj(hwloc_topology_t topology)
	hwloc_obj_t hwloc_get_obj_by_depth(hwloc_topology_t topology, int depth,
									   unsigned idx)
	hwloc_obj_t hwloc_get_obj_by_type(hwloc_topology_t topology,
									  hwloc_obj_type_t type_, unsigned idx)
	hwloc_obj_t hwloc_get_next_obj_by_depth(hwloc_topology_t topology,
											int depth, hwloc_obj_t prev)
	hwloc_obj_t hwloc_get_next_obj_by_type(hwloc_topology_t topology,
										   hwloc_obj_type_t type_, hwloc_obj_t prev)

####
# Converting between Object Types and Attributes, and Strings
####
	const char* hwloc_obj_type_string (hwloc_obj_type_t type_)
	int hwloc_obj_type_snprintf(char* string, size_t size, hwloc_obj_t obj,
								int verbose)
	int hwloc_obj_attr_snprintf(char* string, size_t size, hwloc_obj_t obj,
								const char* separator, int verbose)
	int hwloc_type_sscanf(const char *string, hwloc_obj_type_t *typep,
						  hwloc_obj_attr_u *type_p, size_t attrsize)
	int hwloc_type_sscanf_as_depth(const char *string,
								   hwloc_obj_type_t *typep,
								   hwloc_topology_t topology, int *depthp)

####
# Consulting and Adding Key-Value Info Attributes
####
	const char* hwloc_obj_get_info_by_name(hwloc_obj_t obj, const char* name)
	int hwloc_obj_add_info(hwloc_obj_t obj, const char *name, const char* value)

####
# CPU binding
####
	ctypedef enum hwloc_cpubind_flags_t:
		HWLOC_CPUBIND_PROCESS = (1<<0)
		HWLOC_CPUBIND_THREAD = (1<<1)
		HWLOC_CPUBIND_STRICT = (1<<2)
		HWLOC_CPUBIND_NOMEMBIND = (1<<3)
	int hwloc_set_cpubind(hwloc_topology_t topology, hwloc_const_cpuset_t set_, int flags)
	int hwloc_get_cpubind(hwloc_topology_t topology, hwloc_cpuset_t set_, int flags)
	int hwloc_set_proc_cpubind(hwloc_topology_t topology, hwloc_pid_t pid,
							   hwloc_const_cpuset_t set_, int flags)
	int hwloc_get_proc_cpubind(hwloc_topology_t topology, hwloc_pid_t pid,
							   hwloc_cpuset_t set_, int flags)
	int hwloc_set_thread_cpubind(hwloc_topology_t topology, hwloc_thread_t thread,
								 hwloc_const_cpuset_t set_, int flags)
	int hwloc_get_thread_cpubind(hwloc_topology_t topology, hwloc_thread_t thread,
								 hwloc_cpuset_t set_, int flags)
	int hwloc_get_last_cpu_location(hwloc_topology_t topology, hwloc_cpuset_t set_,
									int flags)
	int hwloc_get_proc_last_cpu_location(hwloc_topology_t topology,
										 hwloc_pid_t pid, hwloc_cpuset_t set_,
										 int flags)

####
# Memory binding policy
####
	ctypedef enum hwloc_membind_policy_t:
		HWLOC_MEMBIND_DEFAULT
		HWLOC_MEMBIND_FIRSTTOUCH
		HWLOC_MEMBIND_BIND
		HWLOC_MEMBIND_INTERLEAVE
		HWLOC_MEMBIND_NEXTTOUCH
		HWLOC_MEMBIND_MIXED = -1
	ctypedef enum hwloc_membind_flags_t:
		HWLOC_MEMBIND_PROCESS = (1<<0)
		HWLOC_MEMBIND_THREAD = (1<<1)
		HWLOC_MEMBIND_STRICT = (1<<2)
		HWLOC_MEMBIND_MIGRATE = (1<<3)
		HWLOC_MEMBIND_NOCPUBIND = (1<<4)
		HWLOC_MEMBIND_BYNODESET = (1<<5)
	int hwloc_set_membind(hwloc_topology_t topology, hwloc_const_bitmap_t set_,
						  hwloc_membind_policy_t policy, int flags)
	int hwloc_get_membind(hwloc_topology_t topology, hwloc_bitmap_t set,
						  hwloc_membind_policy_t* policy, int flags)
	int hwloc_set_proc_membind(hwloc_topology_t topology, hwloc_pid_t pid,
							   hwloc_const_bitmap_t set_,
							   hwloc_membind_policy_t policy, int flags)
	int hwloc_get_proc_membind(hwloc_topology_t topology, hwloc_pid_t pid,
							   hwloc_bitmap_t set_,
							   hwloc_membind_policy_t* policy, int flags)
	int hwloc_set_area_membind(hwloc_topology_t topology, const void* addr,
							   size_t len_, hwloc_const_bitmap_t set_,
							   hwloc_membind_policy_t policy, int flags)
	int hwloc_get_area_membind(hwloc_topology_t topology, const void* addr,
							   size_t len_, hwloc_bitmap_t set_,
							   hwloc_membind_policy_t* policy, int flags)
	int hwloc_get_area_memlocation(hwloc_topology_t topology, const void *addr,
								   size_t len_, hwloc_bitmap_t set_, int flags)
	void* hwloc_alloc(hwloc_topology_t topology, size_t len_)
	void* hwloc_alloc_membind(hwloc_topology_t topology, size_t len_,
							  hwloc_const_bitmap_t set_,
							  hwloc_membind_policy_t policy, int flags)
	void* hwloc_alloc_membind_policy(hwloc_topology_t topology, size_t len_,
									 hwloc_const_bitmap_t set_,
									 hwloc_membind_policy_t policy, int flags)
	int hwloc_free(hwloc_topology_t topology, void* addr, size_t len_)

####
# Changing the Source of Topology Discovery
####
	int hwloc_topology_set_pid(hwloc_topology_t topology, hwloc_pid_t pid)
	int hwloc_topology_set_synthetic(hwloc_topology_t topology,
									 const char* description)
	int hwloc_topology_set_xml(hwloc_topology_t topology, const char* xmlpath)
	int hwloc_topology_set_xmlbuffer(hwloc_topology_t topology, const char* buffer_,
									 int size)

####
# Topology Detection Configuration and Query
####
	cdef enum hwloc_topology_flags_e:
		HWLOC_TOPOLOGY_FLAG_WHOLE_SYSTEM = (1UL<<0)
		HWLOC_TOPOLOGY_FLAG_IS_THISSYSTEM = (1UL<<1)
		HWLOC_TOPOLOGY_FLAG_THISSYSTEM_ALLOWED_RESOURCES = (1UL<<2)
	int hwloc_topology_set_flags(hwloc_topology_t topology,
								 unsigned long flags)
	unsigned long hwloc_topology_get_flags(hwloc_topology_t topology)
	int hwloc_topology_is_thissystem(hwloc_topology_t topology)
	cdef struct hwloc_topology_discovery_support:
		unsigned char pu
		unsigned char numa
		unsigned char numa_memory
	cdef struct hwloc_topology_cpubind_support:
		unsigned char set_thisproc_cpubind
		unsigned char get_thisproc_cpubind
		unsigned char set_proc_cpubind
		unsigned char get_proc_cpubind
		unsigned char set_thisthread_cpubind
		unsigned char get_thisthread_cpubind
		unsigned char set_thread_cpubind
		unsigned char get_thread_cpubind
		unsigned char get_thisproc_last_cpu_location
		unsigned char get_proc_last_cpu_location
		unsigned char get_thisthread_last_cpu_location
	cdef struct hwloc_topology_membind_support:
		unsigned char set_thisproc_membind
		unsigned char get_thisproc_membind
		unsigned char set_proc_membind
		unsigned char get_proc_membind
		unsigned char set_thisthread_membind
		unsigned char get_thisthread_membind
		unsigned char set_area_membind
		unsigned char get_area_membind
		unsigned char alloc_membind
		unsigned char firsttouch_membind
		unsigned char bind_membind
		unsigned char interleave_membind
		unsigned char nexttouch_membind
		unsigned char migrate_membind
		unsigned char get_area_memlocation
	cdef struct hwloc_topology_support:
		hwloc_topology_discovery_support* discovery
		hwloc_topology_cpubind_support* cpubind
		hwloc_topology_membind_support* membind
	const hwloc_topology_support* hwloc_topology_get_support(hwloc_topology_t topology)
	cdef enum hwloc_type_filter_e:
		HWLOC_TYPE_FILTER_KEEP_ALL = 0
		HWLOC_TYPE_FILTER_KEEP_NONE = 1
		HWLOC_TYPE_FILTER_KEEP_STRUCTURE = 2
		HWLOC_TYPE_FILTER_KEEP_IMPORTANT = 3
	int hwloc_topology_set_type_filter(hwloc_topology_t topology,
									   hwloc_obj_type_t type_,
									   hwloc_type_filter_e filter)
	int hwloc_topology_get_type_filter(hwloc_topology_t topology,
									   hwloc_obj_type_t type_,
									   hwloc_type_filter_e *filter)
	int hwloc_topology_set_all_types_filter(hwloc_topology_t topology,
											hwloc_type_filter_e filter)
	int hwloc_topology_set_cache_types_filter(hwloc_topology_t topology,
											  hwloc_type_filter_e filter)
	int hwloc_topology_set_icache_types_filter(hwloc_topology_t topology,
											   hwloc_type_filter_e filter)
	int hwloc_topology_set_io_types_filter(hwloc_topology_t topology,
										   hwloc_type_filter_e filter)
	void hwloc_topology_set_userdata(hwloc_topology_t topology,
									 const void *userdata)
	void * hwloc_topology_get_userdata(hwloc_topology_t topology)

####
# Modifying a loaded Topology
####
	cdef enum hwloc_restrict_flags_e:
		HWLOC_RESTRICT_FLAG_REMOVE_CPULESS = (1<<0)
		HWLOC_RESTRICT_FLAG_ADAPT_MISC = (1<<1)
		HWLOC_RESTRICT_FLAG_ADAPT_IO = (1<<2)
	int hwloc_topology_restrict(hwloc_topology_t topology,
								hwloc_const_cpuset_t cpuset,
								unsigned long flags)
	hwloc_obj_t hwloc_topology_insert_misc_object(hwloc_topology_t topology,
												  hwloc_obj_t parent,
												  const char *name)
	hwloc_obj_t hwloc_topology_alloc_group_object(hwloc_topology_t topology)
	hwloc_obj_t hwloc_topology_insert_group_object(hwloc_topology_t topology,
												   hwloc_obj_t group)
	int hwloc_obj_add_other_obj_sets(hwloc_obj_t dst, hwloc_obj_t src)
####
# Finding Objects Inside a CPU set
####
	hwloc_obj_t hwloc_get_first_largest_obj_inside_cpuset(hwloc_topology_t topology,
													  hwloc_const_cpuset_t set_)
	int hwloc_get_largest_objs_inside_cpuset(hwloc_topology_t topology,
											 hwloc_const_cpuset_t set_,
											 hwloc_obj_t* objs, int max_)
	hwloc_obj_t hwloc_get_next_obj_inside_cpuset_by_depth(hwloc_topology_t topology,
														  hwloc_const_cpuset_t set_,
														  int depth,
														  hwloc_obj_t prev)
	hwloc_obj_t hwloc_get_next_obj_inside_cpuset_by_type(hwloc_topology_t topology,
														 hwloc_const_cpuset_t set_,
														 hwloc_obj_type_t type_,
														 hwloc_obj_t prev)
	hwloc_obj_t hwloc_get_obj_inside_cpuset_by_depth(hwloc_topology_t topology,
													 hwloc_const_cpuset_t set_,
													 int depth, unsigned idx)
	hwloc_obj_t hwloc_get_obj_inside_cpuset_by_type(hwloc_topology_t topology,
													hwloc_const_cpuset_t set_,
													hwloc_obj_type_t type_,
													unsigned idx)
	unsigned hwloc_get_nbobjs_inside_cpuset_by_depth(hwloc_topology_t topology,
													 hwloc_const_cpuset_t set_,
													 int depth)
	int hwloc_get_nbobjs_inside_cpuset_by_type(hwloc_topology_t topology,
											   hwloc_const_cpuset_t set_,
											   hwloc_obj_type_t type_)
	int hwloc_get_obj_index_inside_cpuset(hwloc_topology_t topology,
										  hwloc_const_cpuset_t set_,
										  hwloc_obj_t obj)

####
# Finding Objects covering at least CPU set
####
	hwloc_obj_t hwloc_get_child_covering_cpuset(hwloc_topology_t topology,
												hwloc_const_cpuset_t set,
												hwloc_obj_t parent)
	hwloc_obj_t hwloc_get_obj_covering_cpuset(hwloc_topology_t topology,
											  hwloc_const_cpuset_t set_)
	hwloc_obj_t hwloc_get_next_obj_covering_cpuset_by_depth(hwloc_topology_t topology,
															hwloc_const_cpuset_t set_,
															int depth,
															hwloc_obj_t prev)
	hwloc_obj_t hwloc_get_next_obj_covering_cpuset_by_type(hwloc_topology_t topology,
														   hwloc_const_cpuset_t set_,
														   hwloc_obj_type_t type_,
														   hwloc_obj_t prev)

####
# Looking at Ancestor and Child Objects
####
	hwloc_obj_t hwloc_get_ancestor_obj_by_depth(hwloc_topology_t topology,
												int depth, hwloc_obj_t obj)
	hwloc_obj_t hwloc_get_ancestor_obj_by_type(hwloc_topology_t topology,
											   hwloc_obj_type_t type_,
											   hwloc_obj_t obj)
	hwloc_obj_t hwloc_get_common_ancestor_obj(hwloc_topology_t topology,
											  hwloc_obj_t obj1, hwloc_obj_t obj2)
	int hwloc_obj_is_in_subtree(hwloc_topology_t topology, hwloc_obj_t obj,
								hwloc_obj_t subtree_root)
	hwloc_obj_t hwloc_get_next_child(hwloc_topology_t topology, hwloc_obj_t parent,
									 hwloc_obj_t prev)

####
# Kinds of object Type
####
	int hwloc_obj_type_is_normal(hwloc_obj_type_t type_)
	int hwloc_obj_type_is_io(hwloc_obj_type_t type_)
	int hwloc_obj_type_is_memory(hwloc_obj_type_t type_)
	int hwloc_obj_type_is_cache(hwloc_obj_type_t type_)
	int hwloc_obj_type_is_dcache(hwloc_obj_type_t type_)
	int hwloc_obj_type_is_icache(hwloc_obj_type_t type_)

####
# Looking at Cache Objects
####
	int hwloc_get_cache_type_depth(hwloc_topology_t topology,
								   unsigned cachelevel,
								   hwloc_obj_cache_type_t cachetype)
	hwloc_obj_t hwloc_get_cache_covering_cpuset(hwloc_topology_t topology,
												hwloc_const_cpuset_t set_)
	hwloc_obj_t hwloc_get_shared_cache_covering_obj(hwloc_topology_t topology,
													hwloc_obj_t obj)

####
# Finding objects, miscellaneous helpers
####
	hwloc_obj_t hwloc_get_pu_obj_by_os_index(hwloc_topology_t topology,
											 unsigned os_index)
	hwloc_obj_t hwloc_get_numanode_obj_by_os_index(hwloc_topology_t topology,
												   unsigned os_index)
	unsigned hwloc_get_closest_objs(hwloc_topology_t topology, hwloc_obj_t src,
									hwloc_obj_t* objs, unsigned max_)
	hwloc_obj_t hwloc_get_obj_below_by_type(hwloc_topology_t topology,
											hwloc_obj_type_t type_, unsigned idx1,
											hwloc_obj_type_t type2, unsigned idx2)
	hwloc_obj_t hwloc_get_obj_below_array_by_type(hwloc_topology_t topology, int nr,
												  hwloc_obj_type_t *typev,
												  unsigned *idxv)

####
# Distributing items over a topology
####
	cdef enum hwloc_distrib_flags_e:
		HWLOC_DISTRIB_FLAG_REVERSE = (1<<0)
	int hwloc_distrib(hwloc_topology_t topology, hwloc_obj_t *roots,
					  unsigned n_roots, hwloc_cpuset_t *set_, unsigned n,
					  unsigned until, unsigned long flags)

####
# CPU and node sets of entire topologies
####
	hwloc_const_cpuset_t hwloc_topology_get_complete_cpuset(hwloc_topology_t topology)
	hwloc_const_cpuset_t hwloc_topology_get_topology_cpuset(hwloc_topology_t topology)
	hwloc_const_cpuset_t hwloc_topology_get_allowed_cpuset(hwloc_topology_t topology)
	hwloc_const_nodeset_t hwloc_topology_get_complete_nodeset(hwloc_topology_t topology)
	hwloc_const_nodeset_t hwloc_topology_get_topology_nodeset(hwloc_topology_t topology)
	hwloc_const_nodeset_t hwloc_topology_get_allowed_nodeset(hwloc_topology_t topology)

####
# Converting between CPU sets and node sets
####
	int hwloc_cpuset_to_nodeset(hwloc_topology_t topology,
								hwloc_const_cpuset_t _cpuset,
								hwloc_nodeset_t nodeset)
	int hwloc_cpuset_from_nodeset(hwloc_topology_t topology,
								  hwloc_cpuset_t _cpuset,
								  hwloc_const_nodeset_t nodeset)

####
# Finding I/O objects
####
	hwloc_obj_t hwloc_get_non_io_ancestor_obj(hwloc_topology_t topology,
											  hwloc_obj_t ioobj)
	hwloc_obj_t hwloc_get_next_pcidev(hwloc_topology_t topology, hwloc_obj_t prev)
	hwloc_obj_t hwloc_get_pcidev_by_busid(hwloc_topology_t topology,
										  unsigned domain, unsigned bus,
										  unsigned dev, unsigned func)
	hwloc_obj_t hwloc_get_pcidev_by_busidstring(hwloc_topology_t topology,
												const char *busid)
	hwloc_obj_t hwloc_get_next_osdev(hwloc_topology_t topology, hwloc_obj_t prev)
	hwloc_obj_t hwloc_get_next_bridge(hwloc_topology_t topology, hwloc_obj_t prev)
	int hwloc_bridge_covers_pcibus(hwloc_obj_t bridge, unsigned domain,
								   unsigned bus)

####
# Exporting Topologies to XML
####
	cdef enum hwloc_topology_export_xml_flags_e:
		HWLOC_TOPOLOGY_EXPORT_XML_FLAG_V1 = (1UL<<0)
	int hwloc_topology_export_xml(hwloc_topology_t topology,
								  const char* xmlpath,
								  unsigned long flags)
	int hwloc_topology_export_xmlbuffer(hwloc_topology_t topology,
										char** xmlbuffer,
										int *buflen,
										unsigned long flags)
	void hwloc_free_xmlbuffer(hwloc_topology_t topology, char* xmlbuffer)
	void hwloc_topology_set_userdata_export_callback(hwloc_topology_t topology,
													 void (*export_cb)(void* reserved,
																	   hwloc_topology_t topology,
																	   hwloc_obj_t obj))
	int hwloc_export_obj_userdata(void* reserved, hwloc_topology_t topology,
								  hwloc_obj_t obj, const char* name,
								  const void* buffer_, size_t length)
	int hwloc_export_obj_userdata_base64(void *reserved, hwloc_topology_t topology,
										 hwloc_obj_t obj, const char *name,
										 const void *buffer_, size_t length)
	void hwloc_topology_set_userdata_import_callback(hwloc_topology_t topology,
													 void (*import_cb)(hwloc_topology_t topology,
																	   hwloc_obj_t obj,
																	   const char* name,
																	   const void* buffer_,
																	   size_t length))

####
# Exporting Topologies to Synthetic
####
	cdef enum hwloc_topology_export_synthetic_flags_e:
		HWLOC_TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_EXTENDED_TYPES = (1<<0)
		HWLOC_TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_ATTRS = (1<<1)
		HWLOC_TOPOLOGY_EXPORT_SYNTHETIC_FLAG_V1 = (1UL<<2)
		HWLOC_TOPOLOGY_EXPORT_SYNTHETIC_FLAG_IGNORE_MEMORY = (1UL<<3)
	int hwloc_topology_export_synthetic(hwloc_topology_t topology, char *buffer_,
										size_t buflen, unsigned long flags)

####
# Retrieve distances between objects
####
	cdef struct hwloc_distances_s:
		unsigned nbobjs
		hwloc_obj_t *objs
		unsigned long kind
		uint64_t *values
	cdef enum hwloc_distances_kind_e:
		HWLOC_DISTANCES_KIND_FROM_OS = (1UL<<0)
		HWLOC_DISTANCES_KIND_FROM_USER = (1UL<<1)
		HWLOC_DISTANCES_KIND_MEANS_LATENCY = (1UL<<2)
		HWLOC_DISTANCES_KIND_MEANS_BANDWIDTH = (1UL<<3)
	int hwloc_distances_get(hwloc_topology_t topology, unsigned *nr,
							hwloc_distances_s **distances, unsigned long kind,
							unsigned long flags)
	int hwloc_distances_get_by_depth(hwloc_topology_t topology, int depth,
									 unsigned *nr,
									 hwloc_distances_s **distances,
									 unsigned long kind, unsigned long flags)
	int hwloc_distances_get_by_type(hwloc_topology_t topology,
									hwloc_obj_type_t type,
									unsigned *nr,
									hwloc_distances_s **distances,
									unsigned long kind, unsigned long flags)
	void hwloc_distances_release(hwloc_topology_t topology,
								 hwloc_distances_s *distances)
	int hwloc_distances_obj_index(hwloc_distances_s *distances,
								  hwloc_obj_t obj)
	int hwloc_distances_obj_pair_values(hwloc_distances_s *distances,
										hwloc_obj_t obj1, hwloc_obj_t obj2,
										uint64_t *value1to2,
										uint64_t *value2to1)
	cdef enum hwloc_distances_add_flag_e:
		HWLOC_DISTANCES_ADD_FLAG_GROUP = (1UL<<0)
		HWLOC_DISTANCES_ADD_FLAG_GROUP_INACCURATE = (1UL<<1)
	int hwloc_distances_add(hwloc_topology_t topology, unsigned nbobjs,
							hwloc_obj_t *objs, uint64_t *values,
							unsigned long kind, unsigned long flags)
	int hwloc_distances_remove(hwloc_topology_t topology)
	int hwloc_distances_remove_by_depth(hwloc_topology_t topology, int depth)
	int hwloc_distances_remove_by_type(hwloc_topology_t topology,
									   hwloc_obj_type_t type_)

####
# Topology differences
####
	cdef enum hwloc_topology_diff_obj_attr_type_e:
		HWLOC_TOPOLOGY_DIFF_OBJ_ATTR_SIZE
		HWLOC_TOPOLOGY_DIFF_OBJ_ATTR_NAME
		HWLOC_TOPOLOGY_DIFF_OBJ_ATTR_INFO
	ctypedef hwloc_topology_diff_obj_attr_type_e hwloc_topology_diff_obj_attr_type_t
	cdef struct hwloc_topology_diff_obj_attr_generic_s:
		hwloc_topology_diff_obj_attr_type_t type
	cdef struct hwloc_topology_diff_obj_attr_uint64_s:
		hwloc_topology_diff_obj_attr_type_t type
		uint64_t index
		uint64_t oldvalue
		uint64_t newvalue
	cdef struct hwloc_topology_diff_obj_attr_string_s:
		hwloc_topology_diff_obj_attr_type_t type
		char *name
		char *oldvalue
		char *newvalue
	cdef union hwloc_topology_diff_obj_attr_u:
		hwloc_topology_diff_obj_attr_generic_s generic
		hwloc_topology_diff_obj_attr_uint64_s attr_uint64 "uint64"
		hwloc_topology_diff_obj_attr_string_s string
	cdef enum hwloc_topology_diff_type_e:
		HWLOC_TOPOLOGY_DIFF_OBJ_ATTR
		HWLOC_TOPOLOGY_DIFF_TOO_COMPLEX
	ctypedef hwloc_topology_diff_type_e hwloc_topology_diff_type_t
	cdef union hwloc_topology_diff_u
	cdef struct hwloc_topology_diff_generic_s:
		hwloc_topology_diff_type_t type
		hwloc_topology_diff_u *next
	cdef struct hwloc_topology_diff_obj_attr_s:
		hwloc_topology_diff_type_t type
		hwloc_topology_diff_u *next
		unsigned obj_depth
		unsigned obj_index
		hwloc_topology_diff_obj_attr_u diff
	cdef struct hwloc_topology_diff_too_complex_s:
		hwloc_topology_diff_type_t type
		hwloc_topology_diff_u *next
		unsigned obj_depth
		unsigned obj_index
	cdef union hwloc_topology_diff_u:
		hwloc_topology_diff_generic_s generic
		hwloc_topology_diff_obj_attr_s obj_attr
		hwloc_topology_diff_too_complex_s too_complex
	ctypedef hwloc_topology_diff_u *hwloc_topology_diff_t
	int hwloc_topology_diff_build(hwloc_topology_t topology,
								  hwloc_topology_t newtopology,
								  unsigned long flags,
								  hwloc_topology_diff_t *diff)
	cdef enum hwloc_topology_diff_apply_flags_e:
		HWLOC_TOPOLOGY_DIFF_APPLY_REVERSE = (1<<0)
	int hwloc_topology_diff_apply(hwloc_topology_t topology,
								  hwloc_topology_diff_t diff,
								  unsigned long flags)
	int hwloc_topology_diff_destroy(hwloc_topology_diff_t diff)
	int hwloc_topology_diff_load_xml(const char *xmlpath,
									 hwloc_topology_diff_t *diff,
									 char **refname)
	int hwloc_topology_diff_export_xml(hwloc_topology_diff_t diff,
									   const char *refname,
									   const char *xmlpath)
	int hwloc_topology_diff_load_xmlbuffer(const char *xmlbuffer, int buflen,
										   hwloc_topology_diff_t *diff,
										   char **refname)
	int hwloc_topology_diff_export_xmlbuffer(hwloc_topology_diff_t diff,
											 const char *refname,
											 char **xmlbuffer, int *buflen)

####
# Linux-specific helpers
####
	int hwloc_linux_set_tid_cpubind(hwloc_topology_t topology, pid_t tid,
									hwloc_const_cpuset_t set_)
	int hwloc_linux_get_tid_cpubind(hwloc_topology_t topology, pid_t tid,
									hwloc_cpuset_t set_)
	int hwloc_linux_get_tid_last_cpu_location(hwloc_topology_t topology,
											  pid_t tid, hwloc_bitmap_t set_)
	int hwloc_linux_read_path_as_cpumask(const char *path, hwloc_bitmap_t set_)


cdef extern from 'hwloc/gl.h':
####
# Interoperability with OpenGL displays
####
	hwloc_obj_t hwloc_gl_get_display_osdev_by_port_device(hwloc_topology_t topology,
														  unsigned port,
														  unsigned device)
	hwloc_obj_t hwloc_gl_get_display_osdev_by_name(hwloc_topology_t topology,
												   const char* name)
	int hwloc_gl_get_display_by_osdev(hwloc_topology_t topology, hwloc_obj_t osdev,
									  unsigned *port, unsigned *device)


IF WITH_x86_64:
	####
	# Linux libnuma interaction
	####
	cdef extern from "numa.h":
		cdef struct libnuma_bitmask "bitmask":
			pass

	cdef extern from "hwloc/linux-libnuma.h":
		# Helpers for manipulating Linux libnuma unsigned long masks
		int hwloc_cpuset_to_linux_libnuma_ulongs(hwloc_topology_t topology,
												 hwloc_const_cpuset_t cpuset,
												 unsigned long* mask,
												 unsigned long* maxnode)
		int hwloc_nodeset_to_linux_libnuma_ulongs(hwloc_topology_t topology,
												  hwloc_const_nodeset_t nodeset,
												  unsigned long* mask,
												  unsigned long* maxnode)
		int hwloc_cpuset_from_linux_libnuma_ulongs(hwloc_topology_t topology,
												   hwloc_cpuset_t cpuset,
												   const unsigned long* mask,
												   unsigned long maxnode)
		int hwloc_nodeset_from_linux_libnuma_ulongs(hwloc_topology_t topology,
													hwloc_nodeset_t nodeset,
													const unsigned long* mask,
													unsigned long maxnode)
		# Helpers for manipulating Linux libnuma bitmask
		libnuma_bitmask* hwloc_cpuset_to_linux_libnuma_bitmask(hwloc_topology_t topology,
															   hwloc_const_cpuset_t cpuset)
		libnuma_bitmask* hwloc_nodeset_to_linux_libnuma_bitmask(hwloc_topology_t topology,
																hwloc_const_nodeset_t nodeset)
		int hwloc_cpuset_from_linux_libnuma_bitmask(hwloc_topology_t topology,
													hwloc_cpuset_t cpuset,
													const libnuma_bitmask* bitmask)
		int hwloc_nodeset_from_linux_libnuma_bitmask(hwloc_topology_t topology,
													 hwloc_nodeset_t nodeset,
													 const libnuma_bitmask* bitmask)
#ENDIF


IF WITH_x86_64:
####
# Interoperability with OpenFabrics
####
#
	cdef extern from "hwloc/openfabrics-verbs.h":
		hwloc_obj_t hwloc_ibv_get_device_osdev_by_name(hwloc_topology_t topology,
													   const char *ibname)
#ENDIF


OBJ_MACHINE = HWLOC_OBJ_MACHINE
OBJ_TYPE_MIN = HWLOC_OBJ_TYPE_MIN
OBJ_PACKAGE = HWLOC_OBJ_PACKAGE
OBJ_CORE = HWLOC_OBJ_CORE
OBJ_PU = HWLOC_OBJ_PU
OBJ_L1CACHE = HWLOC_OBJ_L1CACHE
OBJ_L2CACHE = HWLOC_OBJ_L2CACHE
OBJ_L3CACHE = HWLOC_OBJ_L3CACHE
OBJ_L4CACHE = HWLOC_OBJ_L4CACHE
OBJ_L5CACHE = HWLOC_OBJ_L5CACHE
OBJ_L1ICACHE = HWLOC_OBJ_L1ICACHE
OBJ_L2ICACHE = HWLOC_OBJ_L2ICACHE
OBJ_L3ICACHE = HWLOC_OBJ_L3ICACHE
OBJ_GROUP = HWLOC_OBJ_GROUP
OBJ_NUMANODE = HWLOC_OBJ_NUMANODE
OBJ_BRIDGE = HWLOC_OBJ_BRIDGE
OBJ_PCI_DEVICE = HWLOC_OBJ_PCI_DEVICE
OBJ_OS_DEVICE = HWLOC_OBJ_OS_DEVICE
OBJ_MISC = HWLOC_OBJ_MISC
OBJ_TYPE_MAX = HWLOC_OBJ_TYPE_MAX

OBJ_CACHE_UNIFIED = HWLOC_OBJ_CACHE_UNIFIED
OBJ_CACHE_DATA = HWLOC_OBJ_CACHE_DATA
OBJ_CACHE_INSTRUCTION = HWLOC_OBJ_CACHE_INSTRUCTION

OBJ_BRIDGE_HOST = HWLOC_OBJ_BRIDGE_HOST
OBJ_BRIDGE_PCI = HWLOC_OBJ_BRIDGE_PCI

OBJ_OSDEV_BLOCK = HWLOC_OBJ_OSDEV_BLOCK
OBJ_OSDEV_GPU = HWLOC_OBJ_OSDEV_GPU
OBJ_OSDEV_NETWORK = HWLOC_OBJ_OSDEV_NETWORK
OBJ_OSDEV_OPENFABRICS = HWLOC_OBJ_OSDEV_OPENFABRICS
OBJ_OSDEV_DMA = HWLOC_OBJ_OSDEV_DMA
OBJ_OSDEV_COPROC = HWLOC_OBJ_OSDEV_COPROC

UNKNOWN_INDEX = HWLOC_UNKNOWN_INDEX

TYPE_UNORDERED = HWLOC_TYPE_UNORDERED

TYPE_DEPTH_UNKNOWN = HWLOC_TYPE_DEPTH_UNKNOWN
TYPE_DEPTH_MULTIPLE = HWLOC_TYPE_DEPTH_MULTIPLE
TYPE_DEPTH_NUMANODE = HWLOC_TYPE_DEPTH_NUMANODE
TYPE_DEPTH_BRIDGE = HWLOC_TYPE_DEPTH_BRIDGE
TYPE_DEPTH_PCI_DEVICE = HWLOC_TYPE_DEPTH_PCI_DEVICE
TYPE_DEPTH_OS_DEVICE = HWLOC_TYPE_DEPTH_OS_DEVICE
TYPE_DEPTH_MISC = HWLOC_TYPE_DEPTH_MISC

CPUBIND_PROCESS = HWLOC_CPUBIND_PROCESS
CPUBIND_THREAD = HWLOC_CPUBIND_THREAD
CPUBIND_STRICT = HWLOC_CPUBIND_STRICT
CPUBIND_NOMEMBIND = HWLOC_CPUBIND_NOMEMBIND

MEMBIND_DEFAULT = HWLOC_MEMBIND_DEFAULT
MEMBIND_FIRSTTOUCH = HWLOC_MEMBIND_FIRSTTOUCH
MEMBIND_BIND = HWLOC_MEMBIND_BIND
MEMBIND_INTERLEAVE = HWLOC_MEMBIND_INTERLEAVE
MEMBIND_NEXTTOUCH = HWLOC_MEMBIND_NEXTTOUCH
MEMBIND_MIXED = HWLOC_MEMBIND_MIXED

MEMBIND_PROCESS = HWLOC_MEMBIND_PROCESS
MEMBIND_THREAD = HWLOC_MEMBIND_THREAD
MEMBIND_STRICT = HWLOC_MEMBIND_STRICT
MEMBIND_MIGRATE = HWLOC_MEMBIND_MIGRATE
MEMBIND_NOCPUBIND = HWLOC_MEMBIND_NOCPUBIND
MEMBIND_BYNODESET = HWLOC_MEMBIND_BYNODESET

TOPOLOGY_FLAG_WHOLE_SYSTEM = HWLOC_TOPOLOGY_FLAG_WHOLE_SYSTEM
TOPOLOGY_FLAG_IS_THISSYSTEM = HWLOC_TOPOLOGY_FLAG_IS_THISSYSTEM
TOPOLOGY_FLAG_THISSYSTEM_ALLOWED_RESOURCES = HWLOC_TOPOLOGY_FLAG_THISSYSTEM_ALLOWED_RESOURCES

TYPE_FILTER_KEEP_ALL = HWLOC_TYPE_FILTER_KEEP_ALL
TYPE_FILTER_KEEP_NONE = HWLOC_TYPE_FILTER_KEEP_NONE
TYPE_FILTER_KEEP_STRUCTURE = HWLOC_TYPE_FILTER_KEEP_STRUCTURE
TYPE_FILTER_KEEP_IMPORTANT = HWLOC_TYPE_FILTER_KEEP_IMPORTANT

RESTRICT_FLAG_REMOVE_CPULESS = HWLOC_RESTRICT_FLAG_REMOVE_CPULESS
RESTRICT_FLAG_ADAPT_MISC = HWLOC_RESTRICT_FLAG_ADAPT_MISC
RESTRICT_FLAG_ADAPT_IO = HWLOC_RESTRICT_FLAG_ADAPT_IO

DISTRIB_FLAG_REVERSE = HWLOC_DISTRIB_FLAG_REVERSE

TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_EXTENDED_TYPES = HWLOC_TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_EXTENDED_TYPES
TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_ATTRS = HWLOC_TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_ATTRS
TOPOLOGY_EXPORT_SYNTHETIC_FLAG_V1 = HWLOC_TOPOLOGY_EXPORT_SYNTHETIC_FLAG_V1
TOPOLOGY_EXPORT_SYNTHETIC_FLAG_IGNORE_MEMORY = HWLOC_TOPOLOGY_EXPORT_SYNTHETIC_FLAG_IGNORE_MEMORY

DISTANCES_KIND_FROM_OS = HWLOC_DISTANCES_KIND_FROM_OS
DISTANCES_KIND_FROM_USER = HWLOC_DISTANCES_KIND_FROM_USER
DISTANCES_KIND_MEANS_LATENCY = HWLOC_DISTANCES_KIND_MEANS_LATENCY
DISTANCES_KIND_MEANS_BANDWIDTH = HWLOC_DISTANCES_KIND_MEANS_BANDWIDTH

DISTANCES_ADD_FLAG_GROUP = HWLOC_DISTANCES_ADD_FLAG_GROUP
DISTANCES_ADD_FLAG_GROUP_INACCURATE = HWLOC_DISTANCES_ADD_FLAG_GROUP_INACCURATE

TOPOLOGY_DIFF_OBJ_ATTR_SIZE = HWLOC_TOPOLOGY_DIFF_OBJ_ATTR_SIZE
TOPOLOGY_DIFF_OBJ_ATTR_NAME = HWLOC_TOPOLOGY_DIFF_OBJ_ATTR_NAME
TOPOLOGY_DIFF_OBJ_ATTR_INFO = HWLOC_TOPOLOGY_DIFF_OBJ_ATTR_INFO

TOPOLOGY_DIFF_OBJ_ATTR = HWLOC_TOPOLOGY_DIFF_OBJ_ATTR
TOPOLOGY_DIFF_TOO_COMPLEX = HWLOC_TOPOLOGY_DIFF_TOO_COMPLEX

TOPOLOGY_DIFF_APPLY_REVERSE = HWLOC_TOPOLOGY_DIFF_APPLY_REVERSE

# A way to pass a C pointer type to __init__()
cdef PP(const void* pointer):
	as_integer = <unsigned long>pointer
	def getter():
		return as_integer
	return getter

cdef class __ptr(object):
	cdef const void* void_pointer
	def __cinit__(self, ptr_func=None, *_args, **_kargs):
		if ptr_func:
			self.void_pointer = <void*><unsigned long>ptr_func()
		else:
			self.void_pointer = NULL
	cdef set(self, const void* v):
		self.void_pointer = v
	cdef unsigned long _Ulong(self):
		return <unsigned long>self.void_pointer
	def __richcmp__(x, y, op):
		if isinstance(x, __ptr) and isinstance(y, __ptr) and op == 2:
			return (<__ptr>x)._Ulong() == (<__ptr>y)._Ulong()
		else:
			return NotImplemented
	#def __int__(self):  # JUST FOR DEBUGGING!!
	#	return int(self._Ulong())

__userdata_export_callbacks = {}
__userdata_import_callbacks = {}

cdef void __userdata_export_callback(void* reserved, hwloc_topology_t topology, hwloc_obj_t obj):
	t = topology_ptr(PP(topology))
	cb = __userdata_export_callbacks[<unsigned long>t.get()]
	cb(int(<unsigned long>reserved), t, __omake(obj))

cdef void __userdata_import_callback(hwloc_topology_t topology, hwloc_obj_t  obj, const char* name, const void* buf, size_t length):
	t = topology_ptr(PP(topology))
	cb = __userdata_import_callbacks[<unsigned long>t.get()]
	cdef char* p = <char*>buf
	b = p[:length].decode('utf8')
	n = name.decode('utf8')
	cb(t, __omake(obj), str(n), str(b))

def __checkneg1(int value):
	if value == -1:
		raise OSError(CErrno, CStrerror(CErrno))
	return value

cdef const void* __checknull(const void* p) except NULL:
	if p == NULL:
		raise OSError(CErrno, CStrerror(CErrno))
	return p

cdef const void* __checknone(const void* p) except NULL:
	if p == NULL:
		raise NULLError()
	return p


####
# hwlocality_bitmap The bitmap API
####
cdef class __bitmap_ptr(__ptr):
	cdef hwloc_bitmap_t type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_bitmap_t>self.void_pointer
	cdef hwloc_bitmap_t get(self):
		return self.type_p

cdef class volatile_bitmap_ptr(__bitmap_ptr):
	def __init__(self, *_args, **_kargs):
		super(volatile_bitmap_ptr, self).__init__()
	def __dealloc__(self):
		if self.type_p != NULL:
			hwloc_bitmap_free(self.type_p)

cdef volatile_bitmap_ptr __bnew(hwloc_bitmap_t bitmap):
	b = volatile_bitmap_ptr(PP(__checknull(bitmap)))
	return b

cdef volatile_bitmap_ptr __bmake(hwloc_const_bitmap_t bitmap):
	__checknone(bitmap)
	return __bnew(hwloc_bitmap_dup(bitmap))

cdef volatile_bitmap_ptr __bitmap_alloc():
	return __bnew(hwloc_bitmap_alloc())

def bitmap_alloc():
	return __bitmap_alloc()

cdef volatile_bitmap_ptr __bitmap_alloc_full():
	return __bnew(hwloc_bitmap_alloc_full())

def bitmap_alloc_full():
	return __bitmap_alloc_full()

cdef volatile_bitmap_ptr __bitmap_dup(__bitmap_ptr bitmap):
	return __bnew(hwloc_bitmap_dup(bitmap.type_p))

def bitmap_dup(bitmap):
	return __bitmap_dup(bitmap)

def bitmap_copy(__bitmap_ptr dst, __bitmap_ptr src):
	hwloc_bitmap_copy(dst.type_p, src.type_p)

# no reason for bitmap_snprintf

def bitmap_asprintf(__bitmap_ptr bitmap):
	cdef char* p
	__checkneg1(hwloc_bitmap_asprintf(&p, bitmap.type_p))
	b = p.decode('utf8')
	CFree(p)
	return str(b)

def bitmap_sscanf(__bitmap_ptr bitmap, string):
	s = _utfate(string)
	if hwloc_bitmap_sscanf(bitmap.type_p, s) == -1:
		raise ArgError('hwloc_bitmap_sscanf: ' + string)

# no reason for bitmap_list_snprintf

def bitmap_list_asprintf(__bitmap_ptr bitmap):
	cdef char* p
	__checkneg1(hwloc_bitmap_list_asprintf(&p, bitmap.type_p))
	b = p.decode('utf8')
	CFree(p)
	return str(b)

def bitmap_list_sscanf(__bitmap_ptr bitmap, string):
	s = _utfate(string)
	if hwloc_bitmap_list_sscanf(bitmap.type_p, s) == -1:
		raise ArgError('hwloc_bitmap_list_sscanf: ' + string)

# no reason for bitmap_taskset_snprintf

def bitmap_taskset_asprintf(__bitmap_ptr bitmap):
	cdef char* p
	__checkneg1(hwloc_bitmap_taskset_asprintf(&p, bitmap.type_p))
	b = p.decode('utf8')
	CFree(p)
	return str(b)

def bitmap_taskset_sscanf(__bitmap_ptr bitmap, string):
	s = _utfate(string)
	if hwloc_bitmap_taskset_sscanf(bitmap.type_p, s) == -1:
		raise ArgError('hwloc_bitmap_taskset_sscanf: ' + string)

def bitmap_zero(__bitmap_ptr bitmap):
	hwloc_bitmap_zero(bitmap.type_p)

def bitmap_fill(__bitmap_ptr bitmap):
	hwloc_bitmap_fill(bitmap.type_p)

def bitmap_only(__bitmap_ptr bitmap, unsigned id1):
	hwloc_bitmap_only(bitmap.type_p, id1)

def bitmap_allbut(__bitmap_ptr bitmap, unsigned id1):
	hwloc_bitmap_allbut(bitmap.type_p, id1)

# bitmap_from_ulong is bitmap_from_ith_ulong(,1,)

def bitmap_from_ith_ulong(__bitmap_ptr bitmap, unsigned i, unsigned long mask):
	hwloc_bitmap_from_ith_ulong(bitmap.type_p, i, mask)

def bitmap_set(__bitmap_ptr bitmap, id1):
	hwloc_bitmap_set(bitmap.type_p, int(id1))

def bitmap_set_range(__bitmap_ptr bitmap, unsigned begin, int end):
	hwloc_bitmap_set_range(bitmap.type_p, begin, end)

def bitmap_set_ith_ulong(__bitmap_ptr bitmap, unsigned i, unsigned long mask):
	hwloc_bitmap_set_ith_ulong(bitmap.type_p, i, mask)

def bitmap_clr(__bitmap_ptr bitmap, unsigned id1):
	hwloc_bitmap_clr(bitmap.type_p, id1)

def bitmap_clr_range(__bitmap_ptr bitmap, unsigned begin, int end):
	hwloc_bitmap_clr_range(bitmap.type_p, begin, end)

def bitmap_singlify(__bitmap_ptr bitmap):
	hwloc_bitmap_singlify(bitmap.type_p)

# bitmap_to_ulong is bitmap_to_ith_ulong(,1)

def bitmap_to_ith_ulong(__bitmap_ptr bitmap, unsigned i):
	return int(hwloc_bitmap_to_ith_ulong(bitmap.type_p, i))

def bitmap_isset(__bitmap_ptr bitmap, unsigned id1):
	return int(hwloc_bitmap_isset(bitmap.type_p, id1))

def bitmap_iszero(__bitmap_ptr bitmap):
	return int(hwloc_bitmap_iszero(bitmap.type_p))

def bitmap_isfull(__bitmap_ptr bitmap):
	return int(hwloc_bitmap_isfull(bitmap.type_p))

def bitmap_first(__bitmap_ptr bitmap):
	return int(hwloc_bitmap_first(bitmap.type_p))

def bitmap_next(__bitmap_ptr bitmap, int prev):
	return int(hwloc_bitmap_next(bitmap.type_p, prev))

def bitmap_last(__bitmap_ptr bitmap):
	return int(hwloc_bitmap_last(bitmap.type_p))

def bitmap_weight(__bitmap_ptr bitmap):
	return int(hwloc_bitmap_weight(bitmap.type_p))

def bitmap_first_unset(__bitmap_ptr bitmap):
	return int(hwloc_bitmap_first_unset(bitmap.type_p))

def bitmap_next_unset(__bitmap_ptr bitmap, int prev):
	return int(hwloc_bitmap_next_unset(bitmap.type_p, prev))

def bitmap_last_unset(__bitmap_ptr bitmap):
	return int(hwloc_bitmap_last_unset(bitmap.type_p))

def bitmap_or(__bitmap_ptr res, __bitmap_ptr bitmap1, __bitmap_ptr bitmap2):
	hwloc_bitmap_or(res.type_p, bitmap1.type_p, bitmap2.type_p)

def bitmap_and(__bitmap_ptr res, __bitmap_ptr bitmap1, __bitmap_ptr bitmap2):
	hwloc_bitmap_and(res.type_p, bitmap1.type_p, bitmap2.type_p)

def bitmap_andnot(__bitmap_ptr res, __bitmap_ptr bitmap1, __bitmap_ptr bitmap2):
	hwloc_bitmap_andnot(res.type_p, bitmap1.type_p, bitmap2.type_p)

def bitmap_xor(__bitmap_ptr res, __bitmap_ptr bitmap1, __bitmap_ptr bitmap2):
	hwloc_bitmap_xor(res.type_p, bitmap1.type_p, bitmap2.type_p)

def bitmap_not(__bitmap_ptr res, __bitmap_ptr bitmap):
	hwloc_bitmap_not(res.type_p, bitmap.type_p)

def bitmap_intersects(__bitmap_ptr bitmap1, __bitmap_ptr bitmap2):
	return int(hwloc_bitmap_intersects(bitmap1.type_p, bitmap2.type_p))

def bitmap_isincluded(__bitmap_ptr sub_bitmap, __bitmap_ptr super_bitmap):
	return int(hwloc_bitmap_isincluded(sub_bitmap.type_p, super_bitmap.type_p))

def bitmap_isequal(__bitmap_ptr bitmap1, __bitmap_ptr bitmap2):
	return int(hwloc_bitmap_isequal(bitmap1.type_p, bitmap2.type_p))

def bitmap_compare_first(__bitmap_ptr bitmap1, __bitmap_ptr bitmap2):
	return int(hwloc_bitmap_compare_first(bitmap1.type_p, bitmap2.type_p))

def bitmap_compare(__bitmap_ptr bitmap1, __bitmap_ptr bitmap2):
	return int(hwloc_bitmap_compare(bitmap1.type_p, bitmap2.type_p))

####
# API version
####
def get_api_version():
	return int(hwloc_get_api_version())

####
# Topology Object structures
####
cdef class tds_ptr(__ptr):
	cdef hwloc_topology_discovery_support* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_topology_discovery_support*>self.void_pointer

	property pu:
		def __get__(self):
			return int(self.type_p.pu)

	property numa:
		def __get__(self):
			return int(self.type_p.numa)

	property numa_memory:
		def __get__(self):
			return int(self.type_p.numa_memory)

cdef class cbs_ptr(__ptr):
	cdef hwloc_topology_cpubind_support* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_topology_cpubind_support*>self.void_pointer

	property set_thisproc_cpubind:
		def __get__(self):
			return int(self.type_p.set_thisproc_cpubind)

	property get_thisproc_cpubind:
		def __get__(self):
			return int(self.type_p.get_thisproc_cpubind)

	property set_proc_cpubind:
		def __get__(self):
			return int(self.type_p.set_proc_cpubind)

	property get_proc_cpubind:
		def __get__(self):
			return int(self.type_p.get_proc_cpubind)

	property set_thisthread_cpubind:
		def __get__(self):
			return int(self.type_p.set_thisthread_cpubind)

	property get_thisthread_cpubind:
		def __get__(self):
			return int(self.type_p.get_thisthread_cpubind)

	property set_thread_cpubind:
		def __get__(self):
			return int(self.type_p.set_thread_cpubind)

	property get_thread_cpubind:
		def __get__(self):
			return int(self.type_p.get_thread_cpubind)

	property get_thisproc_last_cpu_location:
		def __get__(self):
			return int(self.type_p.get_thisproc_last_cpu_location)

	property get_proc_last_cpu_location:
		def __get__(self):
			return int(self.type_p.get_proc_last_cpu_location)

	property get_thisthread_last_cpu_location:
		def __get__(self):
			return int(self.type_p.get_thisthread_last_cpu_location)

cdef class mbs_ptr(__ptr):
	cdef hwloc_topology_membind_support* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_topology_membind_support*>self.void_pointer

	property set_thisproc_membind:
		def __get__(self):
			return int(self.type_p.set_thisproc_membind)

	property get_thisproc_membind:
		def __get__(self):
			return int(self.type_p.get_thisproc_membind)

	property set_proc_membind:
		def __get__(self):
			return int(self.type_p.set_proc_membind)

	property get_proc_membind:
		def __get__(self):
			return int(self.type_p.get_proc_membind)

	property set_thisthread_membind:
		def __get__(self):
			return int(self.type_p.set_thisthread_membind)

	property get_thisthread_membind:
		def __get__(self):
			return int(self.type_p.get_thisthread_membind)

	property set_area_membind:
		def __get__(self):
			return int(self.type_p.set_area_membind)

	property get_area_membind:
		def __get__(self):
			return int(self.type_p.get_area_membind)

	property alloc_membind:
		def __get__(self):
			return int(self.type_p.alloc_membind)

	property firsttouch_membind:
		def __get__(self):
			return int(self.type_p.firsttouch_membind)

	property bind_membind:
		def __get__(self):
			return int(self.type_p.bind_membind)

	property interleave_membind:
		def __get__(self):
			return int(self.type_p.interleave_membind)

	property nexttouch_membind:
		def __get__(self):
			return int(self.type_p.nexttouch_membind)

	property migrate_membind:
		def __get__(self):
			return int(self.type_p.migrate_membind)

	property get_area_memlocation:
		def __get__(self):
			return int(self.type_p.get_area_memlocation)

cdef class ts_ptr(__ptr):
	cdef hwloc_topology_support* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_topology_support*>self.void_pointer

	property discovery:
		def __get__(self):
			return tds_ptr(PP(self.type_p.discovery))

	property cpubind:
		def __get__(self):
			return cbs_ptr(PP(self.type_p.cpubind))

	property membind:
		def __get__(self):
			return mbs_ptr(PP(self.type_p.membind))

cdef class mpt_ptr(__ptr):
	cdef hwloc_memory_page_type_s* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_memory_page_type_s*>self.void_pointer

	property size:
		def __get__(self):
			return int(self.type_p.size)

	property count:
		def __get__(self):
			return int(self.type_p.count)

cdef class nattr_ptr(__ptr):
	cdef hwloc_numanode_attr_s* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_numanode_attr_s*>self.void_pointer

	property local_memory:
		def __get__(self):
			return int(self.type_p.local_memory)
		def __set__(self, uint64_t value):
			self.type_p.local_memory = value

	property page_types_len:
		def __get__(self):
			return int(PP(self.type_p))

	property page_types:
		def __get__(self):
			cdef hwloc_memory_page_type_s* p
			p = self.type_p.page_types
			return tuple([nattr_ptr(PP(&p[i])) for i in range(self.page_types_len)])

cdef class cattr_ptr(__ptr):
	cdef hwloc_cache_attr_s* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_cache_attr_s*>self.void_pointer

	property size:
		def __get__(self):
			return int(self.type_p.size)

	property depth:
		def __get__(self):
			return int(self.type_p.depth)

	property linesize:
		def __get__(self):
			return int(self.type_p.linesize)

	property associativity:
		def __get__(self):
			return int(self.type_p.associativity)

	property type:
		def __get__(self):
			return int(<int>self.type_p.type)

cdef class gattr_ptr(__ptr):
	cdef hwloc_group_attr_s* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_group_attr_s*>self.void_pointer

	property depth:
		def __get__(self):
			return int(self.type_p.depth)

	property kind:
		def __get__(self):
			return int(self.type_p.kind)

	property subkind:
		def __get__(self):
			return int(self.type_p.subkind)

cdef class pattr_ptr(__ptr):
	cdef hwloc_pcidev_attr_s* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_pcidev_attr_s*>self.void_pointer

	property domain:
		def __get__(self):
			return int(self.type_p.domain)

	property bus:
		def __get__(self):
			return int(self.type_p.bus)

	property dev:
		def __get__(self):
			return int(self.type_p.dev)

	property func:
		def __get__(self):
			return int(self.type_p.func)

	property class_id:
		def __get__(self):
			return int(self.type_p.class_id)

	property vendor_id:
		def __get__(self):
			return int(self.type_p.vendor_id)

	property device_id:
		def __get__(self):
			return int(self.type_p.device_id)

	property subvendor_id:
		def __get__(self):
			return int(self.type_p.subvendor_id)

	property subdevice_id:
		def __get__(self):
			return int(self.type_p.subdevice_id)

	property revision:
		def __get__(self):
			return int(self.type_p.revision)

	property linkspeed:
		def __get__(self):
			return float(self.type_p.linkspeed)

cdef class bups_ptr(__ptr):
	property pci:
		def __get__(self):
			return pattr_ptr(PP(&(<ba_upstream*>self.void_pointer).pci))

cdef class bdowns_ptr(__ptr):
	cdef ba_ds_pci* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <ba_ds_pci*>self.void_pointer

	property domain:
		def __get__(self):
			return int(self.type_p.domain)

	property secondary_bus:
		def __get__(self):
			return int(self.type_p.secondary_bus)

	property subordinate_bus:
		def __get__(self):
			return int(self.type_p.subordinate_bus)

cdef class bd_pci_ptr(__ptr):
	property pci:
		def __get__(self):
			return bdowns_ptr(PP(self.void_pointer))

cdef class battr_ptr(__ptr):
	cdef hwloc_bridge_attr_s* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_bridge_attr_s*>self.void_pointer

	property upstream:
		def __get__(self):
			return bups_ptr(PP(&self.type_p.upstream))

	property upstream_type:
		def __get__(self):
			return int(self.type_p.upstream_type)

	property downstream:
		def __get__(self):
			return bd_pci_ptr(PP(&self.type_p.downstream))

	property downstream_type:
		def __get__(self):
			return int(self.type_p.downstream_type)

	property depth:
		def __get__(self):
			return int(<int>self.type_p.depth)

cdef class oattr_ptr(__ptr):
	property type:
		def __get__(self):
			return int(<int>(<hwloc_osdev_attr_s*>self.void_pointer).type)

cdef class attr_ptr(__ptr):
	cdef hwloc_obj_attr_u* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_obj_attr_u*>self.void_pointer

	property numanode:
		def __get__(self):
			return nattr_ptr(PP(&self.type_p.numanode))

	property cache:
		def __get__(self):
			return cattr_ptr(PP(&self.type_p.cache))

	property group:
		def __get__(self):
			return gattr_ptr(PP(&self.type_p.group))

	property pcidev:
		def __get__(self):
			return pattr_ptr(PP(&self.type_p.pcidev))

	property bridge:
		def __get__(self):
			return battr_ptr(PP(&self.type_p.bridge))

	property osdev:
		def __get__(self):
			return oattr_ptr(PP(&self.type_p.osdev))

cdef class volatile_attr_ptr(attr_ptr):
	def __init__(self, *args, **kwargs):
		super(volatile_attr_ptr, self).__init__()
	def __dealloc__(self):
		if self.type_p != NULL:
			CFree(self.type_p)

cdef class info_ptr(__ptr):
	cdef hwloc_info_s* type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_info_s*>self.void_pointer

	property name:
		def __get__(self):
			return <bytes> self.type_p.name

	property value:
		def __get__(self):
			return <bytes> self.type_p.value

cdef obj_ptr __omake(const void* v):
	return obj_ptr(PP(__checknone(v)))

cdef hwloc_obj_t __oget(obj_ptr instance):
	if instance is None:
		return <hwloc_obj_t>NULL
	return <hwloc_obj_t>instance.void_pointer

####
# Topology Object types
####
cdef class obj_ptr(__ptr):
	cdef hwloc_obj_t type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_obj_t>self.void_pointer

	property type:
		def __get__(self):
			return int(self.type_p.type)
	property subtype:
		def __get__(self):
			if self.type_p.subtype == NULL:
				return None
			return <bytes>self.type_p.subtype
	property os_index:
		def __get__(self):
			# this is kind of hacky but the library returns -1 when the index
			# is unknown or not applicable. We have to convert it to the right
			# size signed value so we can compare it against -1 later.
			return int(<int>self.type_p.os_index)
	property name:
		def __get__(self):
			if self.type_p.name == NULL:
				return None
			return <bytes>self.type_p.name
		def __set__(self, value):
			value = _utfate(value)
			if self.type_p.name != NULL:
				CFree(self.type_p.name)
			self.type_p.name = CStrdup(value)
	property total_memory:
		def __get__(self):
			return int(self.type_p.total_memory)
	property attr:
		def __get__(self):
			if self.type_p.attr == NULL:
				return None
			return attr_ptr(PP(self.type_p.attr))
	property depth:
		def __get__(self):
			return int(<int>self.type_p.depth)
	property logical_index:
		def __get__(self):
			return int(self.type_p.logical_index)
	property next_cousin:
		def __get__(self):
			return __omake(self.type_p.next_cousin)
	property prev_cousin:
		def __get__(self):
			return __omake(self.type_p.prev_cousin)
	property parent:
		def __get__(self):
			return __omake(self.type_p.parent)
	property sibling_rank:
		def __get__(self):
			return int(self.type_p.sibling_rank)
	property next_sibling:
		def __get__(self):
			return __omake(self.type_p.next_sibling)
	property prev_sibling:
		def __get__(self):
			return __omake(self.type_p.prev_sibling)
	property arity:
		def __get__(self):
			return int(self.type_p.arity)
	property children:
		def __get__(self):
			cdef hwloc_obj** p = self.type_p.children
			cdef size_t i
			return tuple([__omake(p[i]) for i in range(self.arity)])
	property first_child:
		def __get__(self):
			return __omake(self.type_p.first_child)
	property last_child:
		def __get__(self):
			return __omake(self.type_p.last_child)
	property memory_arity:
		def __get__(self):
			return int(self.type_p.memory_arity)
	property memory_first_child:
		def __get__(self):
			return __omake(self.type_p.memory_first_child)
	property io_arity:
		def __get__(self):
			return int(self.type_p.io_arity)
	property io_first_child:
		def __get__(self):
			return __omake(self.type_p.io_first_child)
	property misc_arity:
		def __get__(self):
			return int(self.type_p.misc_arity)
	property misc_first_child:
		def __get__(self):
			return __omake(self.type_p.misc_first_child)
	property cpuset:
		def __get__(self):
			return __bmake(self.type_p.cpuset)
		def __set__(self, value):
			if self.type_p.cpuset:
				hwloc_bitmap_free(self.type_p.cpuset)
				self.type_p.cpuset = NULL
			cdef hwloc_bitmap_t ptr = (<__bitmap_ptr>value).get()
			self.type_p.cpuset = hwloc_bitmap_dup(ptr)
	property complete_cpuset:
		def __get__(self):
			return __bmake(self.type_p.complete_cpuset)
	property nodeset:
		def __get__(self):
			return __bmake(self.type_p.nodeset)
		def __set__(self, value):
			if self.type_p.nodeset:
				hwloc_bitmap_free(self.type_p.nodeset)
				self.type_p.nodeset = NULL
			cdef hwloc_bitmap_t ptr = (<__bitmap_ptr>value).get()
			self.type_p.nodeset = hwloc_bitmap_dup(ptr)
	property complete_nodeset:
		def __get__(self):
			return __bmake(self.type_p.complete_nodeset)
	property infos:
		def __get__(self):
			cdef hwloc_info_s* p = self.type_p.infos
			cdef size_t i
			return tuple([info_ptr(PP(&p[i])) for i in range(self.infos_count)])
	property infos_count:
		def __get__(self):
			return int(self.type_p.infos_count)
	property userdata:
		def __get__(self):
			return int(<unsigned long>self.type_p.userdata)
		def __set__(self, unsigned long data):
			self.type_p.userdata = <void*>data
	property gp_index:
		def __get__(self):
			return int(self.type_p.gp_index)


def compare_types(hwloc_obj_type_t type_, hwloc_obj_type_t type2):
	return int(hwloc_compare_types(type_, type2))

####
# Create and Destroy Topologies
####
cdef class topology_ptr(__ptr):
	cdef hwloc_topology_t __delete_this
	def __init__(self, *_args, **_kwargs):
		cdef hwloc_topology_t tptr = NULL
		if self.void_pointer == NULL:
			__checkneg1(hwloc_topology_init(<hwloc_topology_t*>&tptr))
			__userdata_export_callbacks[<unsigned long>tptr] = None
			__userdata_import_callbacks[<unsigned long>tptr] = None
			self.void_pointer = <void*>tptr
			self.__delete_this = tptr

	cdef hwloc_topology_t get(self):
		return <hwloc_topology_t>self.void_pointer

	def __dealloc__(self):
		if self.__delete_this == NULL:
			return
		cdef hwloc_topology_t tptr = self.get()
		try:
			del __userdata_export_callbacks[<unsigned long>tptr]
		except:
			pass
		try:
			del __userdata_import_callbacks[<unsigned long>tptr]
		except:
			pass
		hwloc_topology_destroy(self.__delete_this)


def topology_load(topology_ptr topology):
	__checkneg1(hwloc_topology_load(topology.get()))

def topology_abi_check(topology_ptr topology):
	__checkneg1(hwloc_topology_abi_check(topology.get()))

def topology_check(topology_ptr topology):
	hwloc_topology_check(topology.get())

####
# Configure Topology Detection
####
def topology_set_flags(topology_ptr topology, unsigned long flags):
	hwloc_topology_set_flags(topology.get(), flags)

def topology_get_flags(topology_ptr topology):
	return int(hwloc_topology_get_flags(topology.get()))

def topology_set_pid(topology_ptr topology, hwloc_pid_t pid):
	__checkneg1(hwloc_topology_set_pid(topology.get(), pid))

def topology_set_synthetic(topology_ptr topology, description):
	description = _utfate(description)
	__checkneg1(hwloc_topology_set_synthetic(topology.get(), description))

def topology_set_xml(topology_ptr topology, xmlpath):
	xmlpath = _utfate(xmlpath)
	__checkneg1(hwloc_topology_set_xml(topology.get(), xmlpath))

def topology_set_xmlbuffer(topology_ptr topology, buffer_):
	buffer_ = _utfate(buffer_)
	__checkneg1(hwloc_topology_set_xmlbuffer(topology.get(), buffer_, len(buffer_)))

def topology_is_thissystem(topology_ptr topology):
	return int(hwloc_topology_is_thissystem(topology.get()))

def topology_get_support(topology_ptr topology):
	return ts_ptr(PP(hwloc_topology_get_support(topology.get())))

def topology_set_type_filter(topology_ptr topology, hwloc_obj_type_t type_, hwloc_type_filter_e filter):
	__checkneg1(hwloc_topology_set_type_filter(topology.get(), type_, filter))

def topology_get_type_filter(topology_ptr topology, hwloc_obj_type_t type_):
	cdef hwloc_type_filter_e filter
	__checkneg1(hwloc_topology_get_type_filter(topology.get(), type_, &filter))
	return int(filter)

def topology_set_all_types_filter(topology_ptr topology, hwloc_type_filter_e filter):
	__checkneg1(hwloc_topology_set_all_types_filter(topology.get(), filter))

def topology_set_cache_types_filter(topology_ptr topology, hwloc_type_filter_e filter):
	__checkneg1(hwloc_topology_set_cache_types_filter(topology.get(), filter))

def topology_set_icache_types_filter(topology_ptr topology, hwloc_type_filter_e filter):
	__checkneg1(hwloc_topology_set_icache_types_filter(topology.get(), filter))

def topology_set_io_types_filter(topology_ptr topology, hwloc_type_filter_e filter):
	__checkneg1(hwloc_topology_set_io_types_filter(topology.get(), filter))

# There is really no point in implementing these bindings. If we just store and
# return the value in our topology object instance, we can even make the
# userdata a Python object.
def topology_set_userdata(topology_ptr topology, userdata):
	hwloc_topology_set_userdata(topology.get(), <const void*>userdata)

def topology_get_userdata(topology_ptr topology):
	return int(<unsigned long>hwloc_topology_get_userdata(topology.get()))

####
# Object levels, depths and types
####
def topology_get_depth(topology_ptr topology):
	return int(hwloc_topology_get_depth(topology.get()))

def get_type_depth(topology_ptr topology, int type_):
	return int(hwloc_get_type_depth(topology.get(), <hwloc_obj_type_t>type_))

def get_memory_parents_depth(topology_ptr topology):
	return int(hwloc_get_memory_parents_depth(topology.get()))

def get_type_or_below_depth(topology_ptr topology, hwloc_obj_type_t type_):
	return int(hwloc_get_type_or_below_depth(topology.get(), type_))

def get_type_or_above_depth(topology_ptr topology, hwloc_obj_type_t type_):
	return int(hwloc_get_type_or_above_depth(topology.get(), type_))

def get_depth_type(topology_ptr topology, int depth):
	cdef hwloc_obj_type_t t
	t = hwloc_get_depth_type(topology.get(), depth)
	if <int>t == -1:
		raise ArgError('hwloc_get_depth_type')
	return t

def get_nbobjs_by_depth(topology_ptr topology, int depth):
	return int(hwloc_get_nbobjs_by_depth(topology.get(), depth))

def get_nbobjs_by_type(topology_ptr topology, hwloc_obj_type_t type_):
	return int(hwloc_get_nbobjs_by_type(topology.get(), type_))

def get_root_obj(topology_ptr topology):
	return __omake(hwloc_get_root_obj(topology.get()))

def get_obj_by_depth(topology_ptr topology, int depth, unsigned idx):
	return __omake(hwloc_get_obj_by_depth(topology.get(), depth, idx))

def get_obj_by_type(topology_ptr topology, hwloc_obj_type_t type_, unsigned idx):
	return __omake(hwloc_get_obj_by_type(topology.get(), type_, idx))

def get_next_obj_by_depth(topology_ptr topology, int depth, obj_ptr prev):
	# something about nesting function calls made this code ignore a raised exception
	cdef hwloc_obj_t ret_ptr = hwloc_get_next_obj_by_depth(topology.get(), depth, __oget(prev))
	return __omake(ret_ptr)

def get_next_obj_by_type(topology_ptr topology, hwloc_obj_type_t type_,
						 obj_ptr prev):
	return __omake(hwloc_get_next_obj_by_type(topology.get(), type_,
											  __oget(prev)))

####
# Object Type, Sets and Attributes as Strings
####
def obj_type_string(hwloc_obj_type_t type_):
	return str(hwloc_obj_type_string(type_)[0:].decode('utf-8'))

def obj_type_asprintf(obj_ptr obj, int verbose):
	cdef char* string = NULL
	cdef size_t size = 0
	size = hwloc_obj_type_snprintf(string, size, __oget(obj), verbose) + 1
	string = <char*>CMalloc(size+1)
	if string == NULL:
		raise OSError(CErrno, CStrerror(CErrno))
	size = hwloc_obj_type_snprintf(string, size, __oget(obj), verbose)
	b = string[:int(size)].decode('utf8')
	CFree(string)
	return str(b)

def obj_attr_asprintf(obj_ptr obj, separator, int verbose):
	cdef char* string = NULL
	cdef size_t size = 0
	separator = _utfate(separator)
	size = hwloc_obj_attr_snprintf(string, size, __oget(obj), separator, verbose) + 1
	string = <char*>CMalloc(size+1)
	if string == NULL:
		raise OSError(CErrno, CStrerror(CErrno))
	size = hwloc_obj_attr_snprintf(string, size, __oget(obj), separator, verbose)
	b = string[:int(size)].decode('utf8')
	CFree(string)
	return str(b)

def type_sscanf(string):
	string = _utfate(string)
	cdef hwloc_obj_type_t type_
	cdef size_t attrsize = (sizeof(hwloc_obj_attr_u))
	attr = volatile_attr_ptr(PP(__checknull(CMalloc(attrsize))))
	__checkneg1(hwloc_type_sscanf(string, &type_, attr.type_p, attrsize))
	return type_, attr

def type_sscanf_as_depth(topology_ptr topology, string):
	cdef int depth
	cdef hwloc_obj_type_t type_
	string = _utfate(string)
	__checkneg1(hwloc_type_sscanf_as_depth(string, &type_, topology.get(), &depth))
	return type_, int(depth)

def obj_get_info_by_name(obj_ptr obj, name):
	name = _utfate(name)
	cdef const char* c = hwloc_obj_get_info_by_name(__oget(obj), name)
	if c == NULL:
		return None
	return str(c.decode('utf8'))

def obj_add_info(obj_ptr obj, name, value):
	value = _utfate(value)
	name = _utfate(name)
	__checkneg1(hwloc_obj_add_info(__oget(obj), name, value))

####
# CPU binding
####
def set_cpubind(topology_ptr topology, __bitmap_ptr set_, int flags):
	__checkneg1(hwloc_set_cpubind(topology.get(), set_.type_p, flags))

def get_cpubind(topology_ptr topology, int flags):
	b = __bitmap_alloc()
	__checkneg1(hwloc_get_cpubind(topology.get(), b.type_p, flags))
	return b

def set_proc_cpubind(topology_ptr topology, hwloc_pid_t pid, __bitmap_ptr set_, int flags):
	__checkneg1(hwloc_set_proc_cpubind(topology.get(), pid, set_.type_p, flags))

def get_proc_cpubind(topology_ptr topology, hwloc_pid_t pid, int flags):
	b = __bitmap_alloc()
	__checkneg1(hwloc_get_proc_cpubind(topology.get(), pid, b.type_p, flags))
	return b

def set_thread_cpubind(topology_ptr topology, hwloc_thread_t thread,
					   __bitmap_ptr set_, int flags):
	__checkneg1(hwloc_set_thread_cpubind(topology.get(), thread, set_.type_p,
										 flags))
def get_thread_cpubind(topology_ptr topology, hwloc_thread_t thread, int flags):
	b = __bitmap_alloc()
	__checkneg1(hwloc_get_thread_cpubind(topology.get(), thread, b.type_p,
										 flags))
	return b

def get_last_cpu_location(topology_ptr topology, int flags):
	b = __bitmap_alloc()
	__checkneg1(hwloc_get_last_cpu_location(topology.get(), b.type_p, flags))
	return b

def get_proc_last_cpu_location(topology_ptr topology, hwloc_pid_t pid, int flags):
	b = __bitmap_alloc()
	__checkneg1(hwloc_get_proc_last_cpu_location(topology.get(), pid, b.type_p,
											flags))
	return b

####
# Memory binding policy
####
def set_membind(topology_ptr topology, __bitmap_ptr cpuset,
				hwloc_membind_policy_t policy, int flags):
	__checkneg1(hwloc_set_membind(topology.get(), cpuset.type_p, policy, flags))

def get_membind(topology_ptr topology, int flags):
	cdef hwloc_membind_policy_t p
	b = __bitmap_alloc()
	__checkneg1(hwloc_get_membind(topology.get(), b.type_p, &p, flags))
	return b, int(p)

def set_proc_membind(topology_ptr topology, hwloc_pid_t pid, __bitmap_ptr cpuset,
							 hwloc_membind_policy_t policy, int flags):
	__checkneg1(hwloc_set_proc_membind(topology.get(), pid,
									   cpuset.type_p, policy, flags))

def get_proc_membind(topology_ptr topology, hwloc_pid_t pid, int flags):
	cdef hwloc_membind_policy_t p
	b = __bitmap_alloc()
	__checkneg1(hwloc_get_proc_membind(topology.get(), pid, b.type_p, &p, flags))
	return b, int(p)

def set_area_membind(topology_ptr topology, unsigned long addr, size_t len1,
					 __bitmap_ptr set_, hwloc_membind_policy_t policy, int flags):
	__checkneg1(hwloc_set_area_membind(topology.get(), <void*>addr, len1,
									   set_.type_p, policy, flags))

def get_area_membind(topology_ptr topology, unsigned long addr, size_t len1,
							 int flags):
	cdef hwloc_membind_policy_t p
	b = __bitmap_alloc()
	__checkneg1(hwloc_get_area_membind(topology.get(), <void*>addr, len1,
									   b.type_p, &p, flags))
	return b, int(p)

def get_area_memlocation(topology_ptr topology, unsigned long addr,
						 size_t len_, int flags):
	b = __bitmap_alloc()
	__checkneg1(hwloc_get_area_memlocation(topology.get(), <void*>addr, len_,
										   b.type_p, flags))

def alloc(topology_ptr topology, size_t len1):
	return int(<unsigned long>__checknull(hwloc_alloc(topology.get(), len1)))

def alloc_membind(topology_ptr topology, size_t len1, __bitmap_ptr set_,
						  hwloc_membind_policy_t policy, int flags):
	return int(<unsigned long>
			   __checknull(hwloc_alloc_membind(topology.get(), len1,
											   set_.type_p, policy, flags)))

def alloc_membind_policy(topology_ptr topology, size_t len1, __bitmap_ptr set_,
						 hwloc_membind_policy_t policy, int flags):
	return int(<unsigned long>
			   __checknull(hwloc_alloc_membind_policy(topology.get(), len1,
													  set_.type_p, policy, flags)))

def free(topology_ptr topology, unsigned long addr, size_t len1):
	__checkneg1(hwloc_free(topology.get(), <void*>addr, len1))

####
# Modifying a loaded Topology
####
def topology_restrict(topology_ptr topology, __bitmap_ptr cpuset,
					  unsigned long flags):
	__checkneg1(hwloc_topology_restrict(topology.get(), cpuset.type_p, flags))

def topology_insert_misc_object(topology_ptr topology, obj_ptr parent, name):
	name = _utfate(name)
	return __omake(hwloc_topology_insert_misc_object(topology.get(), __oget(parent), name))

def topology_alloc_group_object(topology_ptr topology):
	return __omake(hwloc_topology_alloc_group_object(topology.get()))

def insert_group_object(topology_ptr topology, obj_ptr group):
	return __omake(hwloc_topology_insert_group_object(topology.get(), __oget(group)))

def obj_add_other_obj_sets(obj_ptr dst, obj_ptr src):
	hwloc_obj_add_other_obj_sets(__oget(dst), __oget(src))

def topology_dup(topology_ptr oldtopology):
	cdef hwloc_topology_t tptr
	__checkneg1(hwloc_topology_dup(<hwloc_topology_t*>&tptr,
								   oldtopology.get()))
	return topology_ptr(PP(tptr))

####
# Exporting Topologies to XML
####
def topology_export_xml(topology_ptr topology, xmlpath,
						hwloc_topology_export_xml_flags_e flags):
	xmlpath = _utfate(xmlpath)
	__checkneg1(hwloc_topology_export_xml(topology.get(), xmlpath, flags))

def topology_export_xmlbuffer(topology_ptr topology,
							  hwloc_topology_export_xml_flags_e flags):
	cdef char* xmlbuffer
	cdef int buflen
	__checkneg1(hwloc_topology_export_xmlbuffer(topology.get(), &xmlbuffer,
												&buflen, flags))
	s = xmlbuffer[:buflen].decode('utf8')
	hwloc_free_xmlbuffer(topology.get(), xmlbuffer)
	return str(s)

def topology_set_userdata_export_callback(topology_ptr topology, cb):
	__userdata_export_callbacks[<unsigned long>topology.get()] = cb
	if cb is None:
		hwloc_topology_set_userdata_export_callback(topology.get(), NULL)
	else:
		hwloc_topology_set_userdata_export_callback(topology.get(),
													&__userdata_export_callback)


def export_obj_userdata(unsigned long reserved, topology_ptr topology, obj_ptr obj,
	name, buffer_):
	name = _utfate(name)
	buffer_ = _utfate(buffer_)
	__checkneg1(hwloc_export_obj_userdata(<void*>reserved, topology.get(),
										  __oget(obj), name, <char*>buffer_, len(buffer_)))

def export_obj_userdata_base64(unsigned long reserved, topology_ptr topology,
						  obj_ptr obj, name, buffer_):
	name = _utfate(name)
	buffer_ = _utfate(buffer_)
	__checkneg1(hwloc_export_obj_userdata_base64(<void*>reserved, topology.get(),
										  __oget(obj), name, <char*>buffer_, len(buffer_)))

def topology_set_userdata_import_callback(topology_ptr topology, cb):
	__userdata_import_callbacks[<unsigned long>topology.get()] = cb
	if cb is None:
		hwloc_topology_set_userdata_import_callback(topology.get(), NULL)
	else:
		hwloc_topology_set_userdata_import_callback(topology.get(),
													&__userdata_import_callback)

####
# Exporting Topologies to Synthetic
####
def topology_export_synthetic(topology_ptr topology, int flags, int size_):
	string = <char*>__checknull(CMalloc(size_ + 1))
	try:
		size = __checkneg1(hwloc_topology_export_synthetic(topology.get(),
														   string, size_, flags))
		b = string[:int(size)].decode('utf8')
	finally:
		CFree(string)
	return str(b)

####
# Retrieve distance between objects
####
cdef class distances_ptr(__ptr):
	cdef hwloc_distances_s* type_p
	def __init__(self):
		self.type_p = <hwloc_distances_s*>self.void_pointer

	property nbobjs:
		def __get__(self):
			return int(<int>self.type_p.nbobjs)

	property objs:
		def __get__(self):
			cdef size_t i
			return tuple([obj_ptr(PP(self.type_p.objs[i])) for i in range(self.nbobjs)])

	property kind:
		def __get__(self):
			return int(self.type_p.kind)

	property values:
		def __get__(self):
			cdef size_t i
			return tuple(int(self.type_p.values[i]) for i in range(self.nbobjs*self.nbobjs))

cdef class volatile_distances_ptr(distances_ptr):
	def __init__(self, *_args, **_kargs):
		super(volatile_distances_ptr, self).__init__()
	def __dealloc__(self):
		if self.type_p != NULL:
			hwloc_distances_release(NULL, self.type_p)

cdef volatile_distances_ptr __dnew(hwloc_distances_s* distances):
	d = volatile_distances_ptr(PP(__checknull(distances)))
	return d


def distances_get(topology_ptr topology, unsigned long kind, unsigned long flags):
	cdef unsigned nr = 0
	cdef hwloc_distances_s** distances
	hwloc_distances_get(topology.get(), &nr, NULL, kind, flags)
	if nr == 0:
		return ()
	distances = <hwloc_distances_s**>__checknull(CMalloc(sizeof(hwloc_distances_s*)*nr))
	hwloc_distances_get(topology.get(), &nr, distances, kind, flags)
	dlist = []
	for i in range(nr):
		dlist.append(__dnew(distances[i]))
	return tuple(dlist)

def distances_get_by_depth(topology_ptr topology, int depth, unsigned long kind, unsigned long flags):
	cdef unsigned nr = 0
	cdef hwloc_distances_s** distances
	hwloc_distances_get_by_depth(topology.get(), depth, &nr, NULL, kind, flags)
	if nr == 0:
		return ()
	distances = <hwloc_distances_s**>__checknull(CMalloc(sizeof(hwloc_distances_s*)*nr))
	hwloc_distances_get_by_depth(topology.get(), depth, &nr, distances, kind, flags)
	dlist = []
	for i in range(nr):
		dlist.append(__dnew(distances[i]))
	return tuple(dlist)

def distances_get_by_type(topology_ptr topology, hwloc_obj_type_t type_, unsigned long kind, unsigned long flags):
	cdef unsigned nr = 0
	cdef hwloc_distances_s** distances
	hwloc_distances_get_by_type(topology.get(), type_, &nr, NULL, kind, flags)
	if nr == 0:
		return ()
	distances = <hwloc_distances_s**>__checknull(CMalloc(sizeof(hwloc_distances_s*)*nr))
	hwloc_distances_get_by_type(topology.get(), type_, &nr, distances, kind, flags)
	dlist = []
	for i in range(nr):
		dlist.append(__dnew(distances[i]))
	return tuple(dlist)

def distances_obj_index(distances_ptr distances, obj_ptr obj):
	return int(hwloc_distances_obj_index(distances.type_p, __oget(obj)))

def distances_obj_pair_values(distances_ptr distances, obj_ptr obj1, obj_ptr obj2):
	cdef uint64_t value1to2, value2to1
	# The library doesn't set errno on error
	err = hwloc_distances_obj_pair_values(distances.type_p, __oget(obj1), __oget(obj2), &value1to2, &value2to1)
	if err == -1:
		return None, None
	return int(value1to2), int(value2to1)

def distances_add(topology_ptr topology, obj_list, value_list, unsigned long kind, unsigned long flags):
	cdef hwloc_obj_t* objs
	cdef unsigned nbobjs = len(obj_list)
	objs = <hwloc_obj_t*>__checknull(CMalloc(sizeof(hwloc_obj_t)*nbobjs))
	cdef uint64_t* values
	try:
		values = <uint64_t*>__checknull(CMalloc(sizeof(uint64_t)*len(value_list)))
	except Exception as e:
		CFree(objs)
		raise e
	for i in range(nbobjs):
		objs[i] = __oget(obj_list[i])
	for i in range(len(value_list)):
		values[i] = value_list[i]
	try:
		__checkneg1(hwloc_distances_add(topology.get(), nbobjs, objs, values, kind, flags))
	finally:
		CFree(objs)
		CFree(values)

def distances_remove(topology_ptr topology):
	__checkneg1(hwloc_distances_remove(topology.get()))

def distances_remove_by_depth(topology_ptr topology, int depth):
	__checkneg1(hwloc_distances_remove_by_depth(topology.get(), depth))

def distances_remove_by_type(topology_ptr topology, hwloc_obj_type_t type_):
	__checkneg1(hwloc_distances_remove_by_type(topology.get(), type_))


####
# Finding Objects Inside a CPU set
####
def get_first_largest_obj_inside_cpuset(topology_ptr topology, __bitmap_ptr set_):
	return __omake(hwloc_get_first_largest_obj_inside_cpuset(topology.get(),
															 set_.type_p))

def get_largest_objs_inside_cpuset(topology_ptr topology, __bitmap_ptr set_,
								   int max_):
	cdef hwloc_obj_t* objs
	objs = <hwloc_obj_t*>__checknull(CMalloc(sizeof(hwloc_obj_t)*max_))
	res = int(hwloc_get_largest_objs_inside_cpuset(topology.get(), set_.type_p,
												   objs, max_))
	if res == -1:
		CFree(objs)
		raise ArgError('hwloc_get_largest_objs_inside_cpuset')
	cdef size_t i
	l = [obj_ptr(PP(objs[i])) for i in range(res)]
	CFree(objs)
	return tuple(l)

def get_next_obj_inside_cpuset_by_depth(topology_ptr topology, __bitmap_ptr set_,
										int depth, obj_ptr prev):
	return __omake(hwloc_get_next_obj_inside_cpuset_by_depth(topology.get(),
															 set_.type_p,
															 depth,
															 __oget(prev)))

def get_next_obj_inside_cpuset_by_type(topology_ptr topology, __bitmap_ptr set_,
									   hwloc_obj_type_t type_, obj_ptr prev):
	return __omake(hwloc_get_next_obj_inside_cpuset_by_type(topology.get(),
															set_.type_p,
															type_,
															__oget(prev)))

def get_obj_inside_cpuset_by_depth(topology_ptr topology, __bitmap_ptr set_, int depth,
								   unsigned idx):
	return __omake(hwloc_get_obj_inside_cpuset_by_depth(topology.get(),
														set_.type_p, depth,
														idx))

def get_obj_inside_cpuset_by_type(topology_ptr topology, __bitmap_ptr set_,
								  hwloc_obj_type_t type_, unsigned idx):
	return __omake(hwloc_get_obj_inside_cpuset_by_type(topology.get(),
													   set_.type_p, type_,
													   idx))

def get_nbobjs_inside_cpuset_by_depth(topology_ptr topology, __bitmap_ptr set_, int depth):
	return int(hwloc_get_nbobjs_inside_cpuset_by_depth(topology.get(),
													   set_.type_p, depth))

def get_nbobjs_inside_cpuset_by_type(topology_ptr topology, __bitmap_ptr set_,
									 hwloc_obj_type_t type_):
	return int(hwloc_get_nbobjs_inside_cpuset_by_type(topology.get(),
													  set_.type_p, type_))

def get_obj_index_inside_cpuset(topology_ptr topology, __bitmap_ptr set_,
								obj_ptr obj):
	return int(hwloc_get_obj_index_inside_cpuset(topology.get(), set_.type_p,
												 __oget(obj)))

####
# Finding a single Object covering at least a CPU set
####
def get_child_covering_cpuset(topology_ptr topology, __bitmap_ptr set_,
							  obj_ptr parent):
	return __omake(hwloc_get_child_covering_cpuset(topology.get(),
												   set_.type_p,
												   __oget(parent)))

def get_obj_covering_cpuset(topology_ptr topology, __bitmap_ptr set_):
	return __omake(hwloc_get_obj_covering_cpuset(topology.get(),
												 set_.type_p))

####
# Finding a set of similar Objects covering at least a CPU set
####
def get_next_obj_covering_cpuset_by_depth(topology_ptr topology, __bitmap_ptr set_,
										  int depth, obj_ptr prev):
	return __omake(hwloc_get_next_obj_covering_cpuset_by_depth(topology.get(),
															   set_.type_p,
															   depth,
															   __oget(prev)))

def get_next_obj_covering_cpuset_by_type(topology_ptr topology, __bitmap_ptr set_,
										 hwloc_obj_type_t type_, obj_ptr prev):
	return __omake(hwloc_get_next_obj_covering_cpuset_by_type(topology.get(),
															  set_.type_p,
															  type_,
															  __oget(prev)))

####
# Looking at Ancestor and Child Objects
####
def get_ancestor_obj_by_depth(topology_ptr topology, int depth, obj_ptr obj):
	return __omake(hwloc_get_ancestor_obj_by_depth(topology.get(),
												   depth,
												   __oget(obj)))

def get_ancestor_obj_by_type(topology_ptr topology, hwloc_obj_type_t type_, obj_ptr obj):
	return __omake(hwloc_get_ancestor_obj_by_type(topology.get(),
												  type_,
												  __oget(obj)))

def get_common_ancestor_obj(topology_ptr topology, obj_ptr obj1, obj_ptr obj2):
	return __omake(hwloc_get_common_ancestor_obj(topology.get(),
												 __oget(obj1),
												 __oget(obj2)))

def obj_is_in_subtree(topology_ptr topology, obj_ptr obj, obj_ptr subtree_root):
	return int(hwloc_obj_is_in_subtree(topology.get(), __oget(obj),
									   __oget(subtree_root)))

def get_next_child(topology_ptr topology, obj_ptr parent, obj_ptr prev):
	return __omake(hwloc_get_next_child(topology.get(),
											   __oget(parent),
											   __oget(prev)))

####
# Kinds of object Type
####
def obj_type_is_normal(hwloc_obj_type_t type_):
	return bool(hwloc_obj_type_is_normal(type_))

def obj_type_is_io(hwloc_obj_type_t type_):
	return bool(hwloc_obj_type_is_io(type_))

def obj_type_is_memory(hwloc_obj_type_t type_):
	return bool(hwloc_obj_type_is_memory(type_))

def obj_type_is_cache(hwloc_obj_type_t type_):
	return bool(hwloc_obj_type_is_cache(type_))

def obj_type_is_dcache(hwloc_obj_type_t type_):
	return bool(hwloc_obj_type_is_dcache(type_))

def obj_type_is_icache(hwloc_obj_type_t type_):
	return bool(hwloc_obj_type_is_icache(type_))

####
# Looking at Cache Objects
####
def get_cache_type_depth(topology_ptr topology, unsigned cachelevel,
						 hwloc_obj_cache_type_t cachetype):
	return int(hwloc_get_cache_type_depth(topology.get(), cachelevel, cachetype))

def get_cache_covering_cpuset(topology_ptr topology, __bitmap_ptr set_):
	return __omake(hwloc_get_cache_covering_cpuset(topology.get(),
												   set_.type_p))

def get_shared_cache_covering_obj(topology_ptr topology, obj_ptr obj):
	return __omake(hwloc_get_shared_cache_covering_obj(topology.get(),
													   __oget(obj)))

####
# Finding objects, miscellaneous helpers
####
def get_pu_obj_by_os_index(topology_ptr topology, unsigned os_index):
	return __omake(hwloc_get_pu_obj_by_os_index(topology.get(),
												os_index))

def get_numanode_obj_by_os_index(topology_ptr topology, unsigned os_index):
	return __omake(hwloc_get_numanode_obj_by_os_index(topology.get(),
													  os_index))

def get_closest_objs(topology_ptr topology, obj_ptr src, unsigned max_):
	cdef hwloc_obj_t* objs
	objs = <hwloc_obj_t*>__checknull(CMalloc(sizeof(hwloc_obj_t)*max_))
	res = int(hwloc_get_closest_objs(topology.get(), __oget(src), objs, max_))
	cdef size_t i
	l = [obj_ptr(PP(objs[i])) for i in range(res)]
	CFree(objs)
	return tuple(l)

def get_obj_below_by_type(topology_ptr topology, hwloc_obj_type_t type_,
						  unsigned idx1, hwloc_obj_type_t type2, unsigned idx2):
	return __omake(hwloc_get_obj_below_by_type(topology.get(),
											   type_, idx1,
											   type2, idx2))

def get_obj_below_array_by_type(topology_ptr topology, typev, idxv):
	if len(typev) != len(idxv):
		raise ArgError('get_obj_below_array_by_type')
	nr = len(typev)
	cdef hwloc_obj_type_t* typep
	typep = <hwloc_obj_type_t*>__checknull(CMalloc(sizeof(hwloc_obj_type_t)*nr))
	cdef unsigned* idxp
	idxp = <unsigned*>__checknull(CMalloc(sizeof(unsigned)*nr))
	cdef size_t i
	for i in range(nr):
		typep[i] = typev[i]
		idxp[i] = idxv[i]
	try:
		p = __omake(hwloc_get_obj_below_array_by_type(topology.get(),
													  nr, typep, idxp))
	finally:
		CFree(typep)
		CFree(idxp)
	return p

####
# Distributing items over a topology
####
def distrib(topology_ptr topology, roots, unsigned n, unsigned until, unsigned flags):
	cdef hwloc_cpuset_t* cpuset
	sets = <hwloc_cpuset_t*>__checknull(CMalloc(sizeof(hwloc_bitmap_t)*n))
	cdef hwloc_obj_t* rootp
	n_roots = len(roots)
	try:
		rootp = <hwloc_obj_t*>__checknull(CMalloc(sizeof(hwloc_obj_t)*n_roots))
	except OSError as e:
		CFree(sets)
		raise e
	cdef size_t i
	for i in range(n_roots):
		rootp[i] = __oget(<obj_ptr>roots[i])
	hwloc_distrib(topology.get(), rootp, n_roots, sets, n, until, flags)
	CFree(rootp)
	try:
		l = [__bnew(sets[i]) for i in range(n)]
	finally:
		CFree(sets)
	return tuple(l)


####
# CPU and node sets of entire topologies
####
def topology_get_complete_cpuset(topology_ptr topology):
	return __bmake(hwloc_topology_get_complete_cpuset(topology.get()))

def topology_get_topology_cpuset(topology_ptr topology):
	return __bmake(hwloc_topology_get_topology_cpuset(topology.get()))

def topology_get_allowed_cpuset(topology_ptr topology):
	return __bmake(hwloc_topology_get_allowed_cpuset(topology.get()))

def topology_get_complete_nodeset(topology_ptr topology):
	return __bmake(hwloc_topology_get_complete_nodeset(topology.get()))

def topology_get_topology_nodeset(topology_ptr topology):
	return __bmake(hwloc_topology_get_topology_nodeset(topology.get()))

def topology_get_allowed_nodeset(topology_ptr topology):
	return __bmake(hwloc_topology_get_allowed_nodeset(topology.get()))


####
# Converting between CPU sets and node sets
####
def cpuset_to_nodeset(topology_ptr topology, __bitmap_ptr cpuset):
	"""Convert a CPU set into a NUMA node set and handle non-NUMA cases"""
	nodeset = __bitmap_alloc()
	hwloc_cpuset_to_nodeset(topology.get(), cpuset.type_p, nodeset.type_p)
	return nodeset

def cpuset_from_nodeset(topology_ptr topology, __bitmap_ptr nodeset):
	"""Convert a NUMA node set into a CPU set and handle non-NUMA cases"""
	cpuset = __bitmap_alloc()
	hwloc_cpuset_from_nodeset(topology.get(), cpuset.type_p,
								   nodeset.type_p)
	return cpuset


####
# Finding I/O objects
####
def get_non_io_ancestor_obj(topology_ptr topology, obj_ptr ioobj):
	"""Get the first non-I/O ancestor object"""
	return __omake(hwloc_get_non_io_ancestor_obj(topology.get(), __oget(ioobj)))

def get_next_pcidev(topology_ptr topology, obj_ptr prev):
	"""Get the next PCI device in the system"""
	return __omake(hwloc_get_next_pcidev(topology.get(), __oget(prev)))

def get_pcidev_by_busid(topology_ptr topology, unsigned domain, unsigned bus, unsigned dev,
						unsigned func):
	"""Find the PCI device object matching the PCI bus id
	given domain, bus device and function PCI bus id"""
	return __omake(hwloc_get_pcidev_by_busid(topology.get(), domain, bus, dev,
											 func))

def get_pcidev_by_busidstring(topology_ptr topology, busid):
	"""Find the PCI device object matching the PCI bus id
	given as a string xxxx:yy:zz.t or yy:zz.t"""
	busid = _utfate(busid)
	return __omake(hwloc_get_pcidev_by_busidstring(topology.get(), busid))

def get_next_osdev(topology_ptr topology, obj_ptr prev):
	"""Get the next OS device in the system"""
	return __omake(hwloc_get_next_osdev(topology.get(), __oget(prev)))

def get_next_bridge(topology_ptr topology, obj_ptr prev):
	return __omake(hwloc_get_next_bridge(topology.get(), __oget(prev)))

def bridge_covers_pcibus(obj_ptr bridge, unsigned domain, unsigned bus):
	return int(hwloc_bridge_covers_pcibus(__oget(bridge), domain, bus))


####
# Topology differences
####
cdef class __ptr_backref(__ptr):
	cdef __ptr _backref

	def __init__(self, pointer, ref=None):
		# hold a reference to keep __dealloc__ from being called on the diff
		# structure intances as long as this instance of an embedded struct
		# remains.
		self._backref = ref

cdef class diff_obj_attr_generic_ptr(__ptr_backref):
	cdef hwloc_topology_diff_obj_attr_generic_s* type_p
	def __init__(self, ptr_func, ref):
		self.type_p = <hwloc_topology_diff_obj_attr_generic_s*>self.void_pointer

	property type:
		def __get__(self):
			return int(self.type_p.type)

cdef class diff_obj_attr_uint64_ptr(__ptr_backref):
	cdef hwloc_topology_diff_obj_attr_uint64_s* type_p
	def __init__(self, ptr_func, ref):
		self.type_p = <hwloc_topology_diff_obj_attr_uint64_s*>self.void_pointer

	property type:
		def __get__(self):
			return int(self.type_p.type)

	property index:
		def __get__(self):
			return int(self.type_p.index)

	property oldvalue:
		def __get__(self):
			return int(self.type_p.oldvalue)

	property newvalue:
		def __get__(self):
			return int(self.type_p.newvalue)

cdef class diff_obj_attr_string_ptr(__ptr_backref):
	cdef hwloc_topology_diff_obj_attr_string_s* type_p
	def __init__(self, ptr_func, ref):
		self.type_p = <hwloc_topology_diff_obj_attr_string_s*>self.void_pointer

	property type:
		def __get__(self):
			return int(self.type_p.type)

	property name:
		def __get__(self):
			return str(<bytes>self.type_p.name.decode('utf8'))

	property oldvalue:
		def __get__(self):
			return str(<bytes>self.type_p.oldvalue.decode('utf8'))

	property newvalue:
		def __get__(self):
			return str(<bytes>self.type_p.newvalue.decode('utf8'))

cdef class diff_obj_attr_u_ptr(__ptr_backref):
	cdef hwloc_topology_diff_obj_attr_u* type_p
	def __init__(self, ptr_func, ref):
		self.type_p = <hwloc_topology_diff_obj_attr_u*>self.void_pointer

	property generic:
		def __get__(self):
			p = diff_obj_attr_generic_ptr(PP(&self.type_p.generic), ref=self)
			return p

	property uint64:
		def __get__(self):
			p = diff_obj_attr_uint64_ptr(PP(&self.type_p.attr_uint64), ref=self)
			return p

	property string:
		def __get__(self):
			p = diff_obj_attr_string_ptr(PP(&self.type_p.string), ref=self)
			return p

cdef class diff_generic_ptr(__ptr_backref):
	cdef hwloc_topology_diff_generic_s* type_p
	def __init__(self, ptr_func, ref):
		self.type_p = <hwloc_topology_diff_generic_s*>self.void_pointer

	property type:
		def __get__(self):
			return int(self.type_p.type)

	property next:
		def __get__(self):
			if self.type_p.next == NULL:
				return None
			return topology_diff_ptr(PP(self.type_p.next))

cdef class diff_obj_attr_s_ptr(__ptr_backref):
	cdef hwloc_topology_diff_obj_attr_s* type_p
	def __init__(self, ptr_func, ref):
		self.type_p = <hwloc_topology_diff_obj_attr_s*>self.void_pointer

	property type:
		def __get__(self):
			return int(self.type_p.type)

	property next:
		def __get__(self):
			if self.type_p.next == NULL:
				return None
			return topology_diff_ptr(PP(self.type_p.next))

	property obj_depth:
		def __get__(self):
			# is unsigned, but can be negative
			return <int>(self.type_p.obj_depth)

	property obj_index:
		def __get__(self):
			return int(self.type_p.obj_index)

	property diff:
		def __get__(self):
			return diff_obj_attr_u_ptr(PP(&self.type_p.diff), self)

cdef class diff_too_complex_ptr(__ptr_backref):
	cdef hwloc_topology_diff_too_complex_s* type_p
	def __init__(self, ptr_func, ref):
		self.type_p = <hwloc_topology_diff_too_complex_s*>self.void_pointer

	property type:
		def __get__(self):
			return int(self.type_p.type)

	property next:
		def __get__(self):
			if self.type_p.next == NULL:
				return None
			return topology_diff_ptr(PP(self.type_p.next))

	property obj_depth:
		def __get__(self):
			# is unsigned, but can be negative
			return <int>(self.type_p.obj_depth)

	property obj_index:
		def __get__(self):
			return int(self.type_p.obj_index)

cdef class topology_diff_ptr(__ptr):
	cdef hwloc_topology_diff_t type_p
	def __init__(self, *_args, **_kargs):
		self.type_p = <hwloc_topology_diff_t>self.void_pointer

	property generic:
		def __get__(self):
			return diff_generic_ptr(PP(&self.type_p.generic), self)

	property obj_attr:
		def __get__(self):
			return diff_obj_attr_s_ptr(PP(&self.type_p.obj_attr), self)

	property too_complex:
		def __get__(self):
			return diff_too_complex_ptr(PP(&self.type_p.too_complex), self)

cdef class volatile_diff_ptr(topology_diff_ptr):
	def __dealloc__(self):
		if self.type_p != NULL:
			hwloc_topology_diff_destroy(self.type_p)

def topology_diff_build(topology_ptr topology,
						topology_ptr newtopology, unsigned long flags = 0):
	"""returns topology_diff_ptr, (boolean) is too complex"""
	cdef hwloc_topology_diff_t diffp
	cdef int ret
	ret = hwloc_topology_diff_build(topology.get(), newtopology.get(),
									flags, &diffp)
	__checkneg1(ret)
	if diffp == NULL:
		return None, False
	diff = volatile_diff_ptr(PP(diffp))
	return diff, ret == 1

def topology_diff_apply(topology_ptr topology, topology_diff_ptr diff,
						unsigned long flags):
	return int(hwloc_topology_diff_apply(topology.get(), diff.type_p, flags))

def topology_diff_load_xml(const char *xmlpath):
	"""returns topology_diff_ptr, refname"""
	cdef hwloc_topology_diff_t diffp
	cdef char *refname
	x = _utfate(xmlpath)
	__checkneg1(hwloc_topology_diff_load_xml(x, &diffp, &refname))
	try:
		r = refname.decode('utf8')
	finally:
		CFree(refname)
	diff = volatile_diff_ptr(PP(diffp))
	return diff, str(r)

def topology_diff_export_xml(topology_diff_ptr diff, const char* refname,
							 const char* xmlpath):
	r = None
	if refname is not None:
		r = _utfate(refname)
	x = _utfate(xmlpath)
	__checkneg1(hwloc_topology_diff_export_xml(diff.type_p, r, x))

def topology_diff_load_xmlbuffer(xmlbuffer):
	"""returns topology_diff_ptr, refname"""
	cdef hwloc_topology_diff_t diffp
	cdef char *refname
	xmlbuffer = _utfate(xmlbuffer)
	__checkneg1(hwloc_topology_diff_load_xmlbuffer(xmlbuffer,
												   len(xmlbuffer), &diffp,
												   &refname))
	if refname == NULL:
		r = None
	else:
		r = refname.decode('utf8')
		CFree(refname)
		r = str(r)
	diff = volatile_diff_ptr(PP(diffp))
	return diff, r

def topology_diff_export_xmlbuffer(topology_diff_ptr diff, refname):
	cdef char* xmlbuffer
	cdef int buflen
	cdef char* r = NULL
	if refname is not None:
		refname = _utfate(refname)
		r = refname
	__checkneg1(hwloc_topology_diff_export_xmlbuffer(diff.type_p, r,
													 &xmlbuffer, &buflen))
	try:
		x = xmlbuffer[:buflen].decode('utf8')
	finally:
		hwloc_free_xmlbuffer(NULL, xmlbuffer)
	return str(x)


####
# Linux-only helpers
####
def linux_set_tid_cpubind(topology_ptr topology, pid_t tid, __bitmap_ptr set_):
	__checkneg1(hwloc_linux_set_tid_cpubind(topology.get(), tid, set_.type_p))

def linux_get_tid_cpubind(topology_ptr topology, pid_t tid):
	set_ = __bitmap_alloc()
	__checkneg1(hwloc_linux_get_tid_cpubind(topology.get(), tid, set_.type_p))
	return set_

def linux_get_tid_last_cpu_location(topology_ptr topology, pid_t tid):
	set_ = __bitmap_alloc()
	__checkneg1(hwloc_linux_get_tid_last_cpu_location(topology.get(), tid,
													  set_.type_p))
	return set_

def linux_read_path_as_cpumask(path):
	path = _utfate(path)
	set_ = __bitmap_alloc()
	__checkneg1(hwloc_linux_read_path_as_cpumask(path, set_.type_p))
	return set_


####
# OpenGL display specific functions
####
def gl_get_display_osdev_by_port_device(topology_ptr topology, unsigned port,
										unsigned device):
	"""Get the hwloc OS device object corresponding to the
	OpenGL display given by port and device index"""
	return __omake(__checknull(hwloc_gl_get_display_osdev_by_port_device(topology.get(),
																		 port,
																		 device)))

def gl_get_display_osdev_by_name(topology_ptr topology, name):
	"""Get the hwloc OS device object corresponding to the
	OpenGL display given by name"""
	name = _utfate(name)
	return __omake(__checknull(hwloc_gl_get_display_osdev_by_name(topology.get(),
																   name)))

def gl_get_display_by_osdev(topology_ptr topology, obj_ptr osdev):
	"""Get the OpenGL display port and device corresponding
	to the given hwloc OS object.
	Returns (port, device)"""
	cdef unsigned port, device
	if hwloc_gl_get_display_by_osdev(topology.get(), __oget(osdev), &port,
									 &device) == -1:
		raise OSError
	return int(port), int(device)


IF WITH_x86_64:
	####
	# Helpers for manipulating Linux libnuma unsigned long masks
	####
	import libnuma

	def cpuset_to_linux_libnuma_ulongs(topology_ptr topology, __bitmap_ptr cpuset):
		"""Convert hwloc CPU set into a tuple of unsigned long
		Returns: tuple of ints, maxnode"""
		cdef unsigned long* mask
		cdef unsigned long maxnode
		maxnode = libnuma.NumConfiguredNodes()
		size = (maxnode+8*sizeof(unsigned long)-1) & ~(8*sizeof(unsigned long)-1)
		mask = <unsigned long*>__checknull(CMalloc(size*sizeof(unsigned long)))
		cdef size_t i
		try:
			hwloc_cpuset_to_linux_libnuma_ulongs(topology.get(), cpuset.type_p, mask,
												&maxnode)
			size = (maxnode+8*sizeof(unsigned long)-1) & ~(8*sizeof(unsigned long)-1)
			l = []
			for i in range(size):
				l.append(int(mask[i]))
		finally:
			CFree(mask)
		return tuple(l), int(maxnode)

	def nodeset_to_linux_libnuma_ulongs(topology_ptr topology, __bitmap_ptr nodeset):
		"""Convert hwloc NUMA node set into the array of unsigned long
		Returns: tuple of ints, maxnode"""
		cdef unsigned long* mask
		cdef unsigned long maxnode
		maxnode = libnuma.NumConfiguredNodes()
		size = (maxnode+8*sizeof(unsigned long)-1) & ~(8*sizeof(unsigned long)-1)
		mask = <unsigned long*>__checknull(CMalloc(size*sizeof(unsigned long)))
		cdef size_t i
		try:
			hwloc_nodeset_to_linux_libnuma_ulongs(topology.get(), nodeset.type_p, mask,
												&maxnode)
			size = (maxnode+8*sizeof(unsigned long)-1) & ~(8*sizeof(unsigned long)-1)
			l = []
			for i in range(size):
				l.append(int(mask[i]))
		finally:
			CFree(mask)
		return tuple(l), maxnode

	def cpuset_from_linux_libnuma_ulongs(topology_ptr topology, mask,
											maxnode=None):
		"""Convert a list of unsigned long into hwloc CPU set"""
		cdef unsigned long* cmask
		if maxnode is None:
			maxnode = libnuma.NumConfiguredNodes()
		b = __bitmap_alloc()
		cmask = <unsigned long*>__checknull(CMalloc(len(mask)*sizeof(unsigned long)))
		cdef size_t i
		try:
			for i in range(len(mask)):
				cmask[i] = mask[i]
			hwloc_cpuset_from_linux_libnuma_ulongs(topology.get(), b.type_p, cmask,
													maxnode)
		finally:
			CFree(cmask)
		return b

	def nodeset_from_linux_libnuma_ulongs(topology_ptr topology, mask,
											maxnode=None):
		"""Convert a list of unsigned long into hwloc NUMA node set"""
		cdef unsigned long* cmask
		if maxnode is None:
			maxnode = libnuma.NumConfiguredNodes()
		b = __bitmap_alloc()
		cmask = <unsigned long*>__checknull(CMalloc(len(mask)*sizeof(unsigned long)))
		cdef size_t i
		try:
			for i in range(len(mask)):
				cmask[i] = mask[i]
			hwloc_nodeset_from_linux_libnuma_ulongs(topology.get(), b.type_p, cmask,
													maxnode)
		finally:
			CFree(cmask)
		return b

	def cpuset_to_linux_libnuma_bitmask(topology_ptr topology,
										__bitmap_ptr cpuset):
		"""Convert hwloc CPU set into a python-libnuma Bitmask"""
		cdef libnuma_bitmask* b
		b = <libnuma_bitmask*>__checknull(hwloc_cpuset_to_linux_libnuma_bitmask(topology.get(),
																		cpuset.type_p))
		n = libnuma.Bitmask()
		n.SetPtr(<unsigned long>b)
		return n

	def nodeset_to_linux_libnuma_bitmask(topology_ptr topology,
										__bitmap_ptr nodeset):
		"""Convert hwloc NUMA node set into a python-libnuma Bitmask"""
		cdef libnuma_bitmask* b
		b = <libnuma_bitmask*>__checknull(hwloc_nodeset_to_linux_libnuma_bitmask(topology.get(),
																		nodeset.type_p))
		n = libnuma.Bitmask()
		n.SetPtr(<unsigned long>b)
		return n


	cdef const libnuma_bitmask* __bmget(unsigned long ptr):
		return <libnuma_bitmask*>ptr

	def cpuset_from_linux_libnuma_bitmask(topology_ptr topology, bitmask):
		"""Convert python-libnuma Bitmask into hwloc CPU set"""
		b = __bitmap_alloc()
		hwloc_cpuset_from_linux_libnuma_bitmask(topology.get(), b.type_p,
												__bmget(bitmask.Ptr()))
		return b

	def nodeset_from_linux_libnuma_bitmask(topology_ptr topology, bitmask):
		"""Convert python-libnuma Bitmask into hwloc NUMA node set set"""
		b = __bitmap_alloc()
		hwloc_nodeset_from_linux_libnuma_bitmask(topology.get(), b.type_p,
												__bmget(bitmask.Ptr()))
		return b
#ENDIF
