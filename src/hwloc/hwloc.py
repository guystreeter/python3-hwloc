# -*- python -*-
#
# Copyright 2019-2020 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

__author = 'Guy Streeter <guy.streeter@gmail.com>'
__license = 'GPLv2'
__version = '3.1.0'
__description = 'Python bindings for hwloc version 2'
__URL = 'https://gitlab.com/guystreeter/python2-hwloc'

from typing import List, Optional, Union, Tuple, Generator

from hwloc import chwloc as C


INT_MAX: int = C.HWLOC_INT_MAX
UINT_MAX: int = C.HWLOC_UINT_MAX
ULONG_MAX: int = C.HWLOC_ULONG_MAX

OBJ_MACHINE: int = C.OBJ_MACHINE
OBJ_PACKAGE: int = C.OBJ_PACKAGE
OBJ_CORE: int = C.OBJ_CORE
OBJ_PU: int = C.OBJ_PU
OBJ_L1CACHE: int = C.OBJ_L1CACHE
OBJ_L2CACHE: int = C.OBJ_L2CACHE
OBJ_L3CACHE: int = C.OBJ_L3CACHE
OBJ_L4CACHE: int = C.OBJ_L4CACHE
OBJ_L5CACHE: int = C.OBJ_L5CACHE
OBJ_L1ICACHE: int = C.OBJ_L1ICACHE
OBJ_L2ICACHE: int = C.OBJ_L2ICACHE
OBJ_L3ICACHE: int = C.OBJ_L3ICACHE
OBJ_GROUP: int = C.OBJ_GROUP
OBJ_NUMANODE: int = C.OBJ_NUMANODE
OBJ_BRIDGE: int = C.OBJ_BRIDGE
OBJ_PCI_DEVICE: int = C.OBJ_PCI_DEVICE
OBJ_OS_DEVICE: int = C.OBJ_OS_DEVICE
OBJ_MISC: int = C.OBJ_MISC
OBJ_TYPE_MAX: int = C.OBJ_TYPE_MAX
OBJ_TYPE_MIN: int = C.OBJ_TYPE_MIN

OBJ_CACHE_UNIFIED: int = C.OBJ_CACHE_UNIFIED
OBJ_CACHE_DATA: int = C.OBJ_CACHE_DATA
OBJ_CACHE_INSTRUCTION: int = C.OBJ_CACHE_INSTRUCTION

OBJ_BRIDGE_HOST: int = C.OBJ_BRIDGE_HOST
OBJ_BRIDGE_PCI: int = C.OBJ_BRIDGE_PCI

OBJ_OSDEV_BLOCK: int = C.OBJ_OSDEV_BLOCK
OBJ_OSDEV_GPU: int = C.OBJ_OSDEV_GPU
OBJ_OSDEV_NETWORK: int = C.OBJ_OSDEV_NETWORK
OBJ_OSDEV_OPENFABRICS: int = C.OBJ_OSDEV_OPENFABRICS
OBJ_OSDEV_DMA: int = C.OBJ_OSDEV_DMA
OBJ_OSDEV_COPROC: int = C.OBJ_OSDEV_COPROC

TYPE_UNORDERED: int = C.TYPE_UNORDERED
UNKNOWN_INDEX: int = C.UNKNOWN_INDEX

TYPE_DEPTH_UNKNOWN: int = C.TYPE_DEPTH_UNKNOWN
TYPE_DEPTH_MULTIPLE: int = C.TYPE_DEPTH_MULTIPLE
TYPE_DEPTH_NUMANODE: int = C.TYPE_DEPTH_NUMANODE
TYPE_DEPTH_BRIDGE: int = C.TYPE_DEPTH_BRIDGE
TYPE_DEPTH_PCI_DEVICE: int = C.TYPE_DEPTH_PCI_DEVICE
TYPE_DEPTH_OS_DEVICE: int = C.TYPE_DEPTH_OS_DEVICE
TYPE_DEPTH_MISC: int = C.TYPE_DEPTH_MISC

CPUBIND_PROCESS: int = C.CPUBIND_PROCESS
CPUBIND_THREAD: int = C.CPUBIND_THREAD
CPUBIND_STRICT: int = C.CPUBIND_STRICT
CPUBIND_NOMEMBIND: int = C.CPUBIND_NOMEMBIND

MEMBIND_DEFAULT: int = C.MEMBIND_DEFAULT
MEMBIND_FIRSTTOUCH: int = C.MEMBIND_FIRSTTOUCH
MEMBIND_BIND: int = C.MEMBIND_BIND
MEMBIND_INTERLEAVE: int = C.MEMBIND_INTERLEAVE
MEMBIND_NEXTTOUCH: int = C.MEMBIND_NEXTTOUCH
MEMBIND_MIXED: int = C.MEMBIND_MIXED

MEMBIND_PROCESS: int = C.MEMBIND_PROCESS
MEMBIND_THREAD: int = C.MEMBIND_THREAD
MEMBIND_STRICT: int = C.MEMBIND_STRICT
MEMBIND_MIGRATE: int = C.MEMBIND_MIGRATE
MEMBIND_NOCPUBIND: int = C.MEMBIND_NOCPUBIND
MEMBIND_BYNODESET: int = C.MEMBIND_BYNODESET

TOPOLOGY_FLAG_WHOLE_SYSTEM: int = C.TOPOLOGY_FLAG_WHOLE_SYSTEM
TOPOLOGY_FLAG_IS_THISSYSTEM: int = C.TOPOLOGY_FLAG_IS_THISSYSTEM
TOPOLOGY_FLAG_THISSYSTEM_ALLOWED_RESOURCES: int = C.TOPOLOGY_FLAG_THISSYSTEM_ALLOWED_RESOURCES

TYPE_FILTER_KEEP_ALL: int = C.TYPE_FILTER_KEEP_ALL
TYPE_FILTER_KEEP_NONE: int = C.TYPE_FILTER_KEEP_NONE
TYPE_FILTER_KEEP_STRUCTURE: int = C.TYPE_FILTER_KEEP_STRUCTURE
TYPE_FILTER_KEEP_IMPORTANT: int = C.TYPE_FILTER_KEEP_IMPORTANT

RESTRICT_FLAG_REMOVE_CPULESS: int = C.RESTRICT_FLAG_REMOVE_CPULESS
RESTRICT_FLAG_ADAPT_MISC: int = C.RESTRICT_FLAG_ADAPT_MISC
RESTRICT_FLAG_ADAPT_IO: int = C.RESTRICT_FLAG_ADAPT_IO

DISTRIB_FLAG_REVERSE: int = C.DISTRIB_FLAG_REVERSE

TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_EXTENDED_TYPES: int = C.TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_EXTENDED_TYPES
TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_ATTRS: int = C.TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_ATTRS
TOPOLOGY_EXPORT_SYNTHETIC_FLAG_V1: int = C.TOPOLOGY_EXPORT_SYNTHETIC_FLAG_V1
TOPOLOGY_EXPORT_SYNTHETIC_FLAG_IGNORE_MEMORY: int = C.TOPOLOGY_EXPORT_SYNTHETIC_FLAG_IGNORE_MEMORY

DISTANCES_KIND_FROM_OS: int = C.DISTANCES_KIND_FROM_OS
DISTANCES_KIND_FROM_USER: int = C.DISTANCES_KIND_FROM_USER
DISTANCES_KIND_MEANS_LATENCY: int = C.DISTANCES_KIND_MEANS_LATENCY
DISTANCES_KIND_MEANS_BANDWIDTH: int = C.DISTANCES_KIND_MEANS_BANDWIDTH

DISTANCES_ADD_FLAG_GROUP: int = C.DISTANCES_ADD_FLAG_GROUP
DISTANCES_ADD_FLAG_GROUP_INACCURATE: int = C.DISTANCES_ADD_FLAG_GROUP_INACCURATE

TOPOLOGY_DIFF_OBJ_ATTR_SIZE: int = C.TOPOLOGY_DIFF_OBJ_ATTR_SIZE
TOPOLOGY_DIFF_OBJ_ATTR_NAME: int = C.TOPOLOGY_DIFF_OBJ_ATTR_NAME
TOPOLOGY_DIFF_OBJ_ATTR_INFO: int = C.TOPOLOGY_DIFF_OBJ_ATTR_INFO

TOPOLOGY_DIFF_OBJ_ATTR: int = C.TOPOLOGY_DIFF_OBJ_ATTR
TOPOLOGY_DIFF_TOO_COMPLEX: int = C.TOPOLOGY_DIFF_TOO_COMPLEX

TOPOLOGY_DIFF_APPLY_REVERSE: int = C.TOPOLOGY_DIFF_APPLY_REVERSE

# Linux-only stuff, shouldn't be invoked elsewhere
try:
    from . import linuxsched
except ModuleNotFoundError:
    pass


def _unfate(text) -> str:
    if isinstance(text, bytes):
        return text.decode('utf8')
    raise ValueError('requires text input, got %s' % type(text))


class Error(Exception):
    pass


class ArgError(Error):
    """Exception raised when a calling argument is invalid"""

    def __init__(self, string) -> None:
        self.msg = string

    def __str__(self) -> str:
        return self.msg


# Forward reference for parser
class Bitmap:
    pass


class Bitmap:
    """Python object representing the hwloc library's internal bitmap structure"""

    def __init__(self, bmp_ptr: Optional[object] = None) -> None:
        """Constructs a Bitmap object

        Args:
            bmp_ptr (Bitmap, optional):
                If a ``Bitmap`` is supplied, this `Bitmap` will be a copy of it.
                Otherwise an empty `Bitmap` is constructed. Defaults to None.
        """
        if bmp_ptr is None:
            self._bitmap = C.bitmap_alloc()
            return
        if isinstance(bmp_ptr, Bitmap):
            self._bitmap = C.bitmap_alloc()
            self.copy(bmp_ptr)
            return
        self._bitmap = bmp_ptr

    @classmethod
    def alloc(cls, value=None) -> Bitmap:
        """Creates a new ``Bitmap`` object

        Args:
            value (int, list of int, or str, optional):
                Initial value for the bit settings. Defaults to ``None``.
                If an ``int`` or list of ``int`` is supplied, the indexes will be set
                in the new ``Bitmap``. If a ``str`` is supplied, it will be passed to
                ``sscanf`` to set the bits.

        Returns:
            Bitmap: Newly-constructed ``Bitmap``
        """
        bitmap = Bitmap(C.bitmap_alloc())
        if value is not None:
            try:
                bitmap.set(value)
            except ValueError:
                bitmap.sscanf(value)
        else:
            bitmap.zero()
        return bitmap

    @classmethod
    def alloc_full(cls) -> Bitmap:
        """Allocates a new full ``Bitmap``.

        Returns:
            Bitmap: Full ``Bitmap``
        """
        return Bitmap(C.bitmap_alloc_full())

    def dup(self) -> Bitmap:
        """Duplicates a ``Bitmap``

        Returns:
            Bitmap: New ``Bitmap``
        """
        return Bitmap(C.bitmap_dup(self._bitmap))

    def copy(self, other: Bitmap) -> None:
        """Copies the contents of another ``Bitmap``

        Args:
            other (Bitmap): The ``Bitmap`` to be copied
        """
        C.bitmap_copy(self._bitmap, other._bitmap)

    def __copy__(self) -> Bitmap:
        return self.dup()

    def asprintf(self) -> str:
        """Returns a string representing the ``Bitmap``'s values

        Returns:
            str: The alphanumeric representation of the ``Bitmap``'s value
        """
        return C.bitmap_asprintf(self._bitmap)

    def __str__(self) -> str:
        return self.asprintf()

    def sscanf(self, string: str) -> None:
        """Sets the ``Bitmap``'s value from a string

        Args:
            string (str): A string representing the indices to be set

        Raises:
            ArgError: Raised if the string cannot be parsed
        """
        try:
            C.bitmap_sscanf(self._bitmap, string)
        except C.ArgError:
            raise ArgError('Bitmap.sscanf() error scanning ' + string)

    def list_asprintf(self) -> str:
        """Stringifies the ``Bitmap``'s set bits

        Returns:
            str: Comma-separated indices or ranges
        """
        return C.bitmap_list_asprintf(self._bitmap)

    def list_sscanf(self, string: str) -> None:
        """Parses a numeric string and sets the Bitmap to those values

        Args:
            string (str): comma-separated indices or ranges

        Raises:
            ArgError: Raised if the string cannot be parsed
        """
        try:
            C.bitmap_list_sscanf(self._bitmap, string)
        except C.ArgError:
            raise ArgError('Bitmap.list_sscanf() error scanning ' + string)

    def taskset_asprintf(self) -> str:
        """Creates a bitmap string representation for taskset() from
        this ``Bitmap``

        Returns:
            str: taskset-specific string
        """
        return C.bitmap_taskset_asprintf(self._bitmap)

    def taskset_sscanf(self, string: str):
        """Parses a taskset-specific bitmap string and sets this ``Bitmap`` to it

        Args:
            string (str): taskset-specific string

        Raises:
            ArgError: Raised if the string cannot be parsed
        """
        try:
            C.bitmap_taskset_sscanf(self._bitmap, string)
        except C.ArgError:
            raise ArgError('Bitmap.taskset_sscanf() error scanning ' + string)

    def zero(self):
        """Clears all the bits in the ``Bitmap``"""
        C.bitmap_zero(self._bitmap)

    def fill(self):
        """Fills the ``Bitmap`` with set bits"""
        C.bitmap_fill(self._bitmap)

    def only(self, idx: int):
        """Clears all the bits and sets one

        Args:
            idx (int): The single bit to be set
        """
        C.bitmap_only(self._bitmap, idx)

    def allbut(self, idx: int):
        """Sets all the bits except one

        Args:
            idx (int): The single bit to be left cleared
        """
        C.bitmap_allbut(self._bitmap, idx)

    def from_ulong(self, mask: int, idx: Optional[int] = 0):
        """Sets the ``Bitmap`` value from a ulong. All other bits are cleared.

        Args:
            mask (int): ulong value to be set.
            idx (int, optional): Index of ulong to be set. Defaults to 0.
        """
        C.bitmap_from_ith_ulong(self._bitmap, idx, mask)

    def set(self, index: Union[int, List[int]]):
        """Sets one or more indices in the ``Bitmap``

        Args:
            index (int or list of int): bits to be set.
        """
        try:
            for i in index:
                C.bitmap_set(self._bitmap, i)
        except TypeError:
            C.bitmap_set(self._bitmap, index)

    def set_range(self, begin: int, end: int):
        """Sets a range of bits in the ``Bitmap``

        Args:
            begin (int): Starting index of range to be set.
            end (int): End of range. -1 is infinite.
        """
        C.bitmap_set_range(self._bitmap, begin, end)

    def set_ulong(self, mask: int, idx: Optional[int] = 0):
        """Replaces i-th (defaults to zero) ulong subset of the
        ``Bitmap`` with ``unsigned long ``mask``

        Args:
            mask (int): ulong value to set.
            idx (int, optional): index of ulong to set. Defaults to 0.
        """
        C.bitmap_set_ith_ulong(self._bitmap, idx, mask)

    def clr(self, index: int) -> None:
        """Clears a bit in the ``Bitmap``

        Args:
            index (int): Index of bit to be cleared
        """
        C.bitmap_clr(self._bitmap, index)

    def clr_range(self, begin: int, end: int):
        """Clears a range of bits in the ``Bitmap``

        Args:
            begin (int): Starting index of range to be cleared
            end (int): End of range. -1 is infinite.
        """
        C.bitmap_clr_range(self._bitmap, begin, end)

    def singlify(self):
        """Keeps a single index among those set in the ``Bitmap``.

        Note: this should only be used on ``Bitmap`` objects created dynamically.
        """
        C.bitmap_singlify(self._bitmap)

    def ulong(self, idx: Optional[int] = 0) -> int:
        """Returns the i-th (defaults to zero) subset of
        ``Bitmap`` as an int

        Args:
            idx (int, optional): Index of the ulong to be returned.
                Defaults to 0.

        Returns:
            int: The requested ulong of the ``Bitmap``.
        """
        return C.bitmap_to_ith_ulong(self._bitmap, idx)

    def isset(self, index: int) -> bool:
        """Tests whether a specified bit is set in the ``Bitmap``

        Args:
            index (int): Index of the bit to be tested.

        Returns:
            bool: True if the bit is set.
        """
        return bool(C.bitmap_isset(self._bitmap, index))

    @property
    def iszero(self) -> bool:
        """Tests whether the ``Bitmap`` is empty (all bits are cleared)

        Returns:
            bool: True if all bits are cleared.
        """
        return bool(C.bitmap_iszero(self._bitmap))

    @property
    def isfull(self) -> bool:
        """Tests if the ``Bitmap`` is completely full

        Returns:
            bool: True if the ``Bitmap`` is full.
        """
        return bool(C.bitmap_isfull(self._bitmap))

    @property
    def first(self) -> int:
        """The index of the first set bit in the ``Bitmap``

        Returns:
            int: The index of the first set bit, or -1.
        """
        return C.bitmap_first(self._bitmap)

    def next(self, prev: int) -> int:
        """Returns the index of the first set bit following the supplied index

        Args:
            prev (int): Index of the bit before the start of the search.

        Returns:
            int: Index of the next set bot, or -1.
        """
        return C.bitmap_next(self._bitmap, prev)

    def __iter__(self) -> Generator[int, None, None]:
        """A Generator yielding the indices of the set bits

        Yields:
            int: The index of the next set bit
        """
        prev = self.first
        while prev != -1:
            yield prev
            prev = self.next(prev)

    @property
    def all_set_bits(self) -> Tuple[int, ...]:
        """A tuple of the indices of the set bits in the ``Bitmap``

        Returns:
            tuple of int: Indices of the set bits.
        """
        return tuple([x for x in self])

    @property
    def last(self) -> int:
        """Index of the last set bit in the ``Bitmap``

        Returns:
            int: bit index
        """
        return C.bitmap_last(self._bitmap)

    # not a property, just because it can be an expensive operation
    def weight(self) -> int:
        """Returns the number of set bits in the ``Bitmap``

        Returns:
            int: Number of set bits.
        """
        return C.bitmap_weight(self._bitmap)

    @property
    def first_unset(self) -> int:
        """The index of the first clear bit in the ``Bitmap``

        Returns:
            int: Index of the first clear bit.
        """
        return C.bitmap_first_unset(self._bitmap)

    def next_unset(self, prev: int) -> int:
        """Returns the index of the next clear bit after the supplied index.

        Args:
            prev (int): Index of the bit after which to search.

        Returns:
            int: Index of the next clear bit.
        """
        return C.bitmap_next_unset(self._bitmap, prev)

    @property
    def last_unset(self) -> int:
        """The index of the last clear bit of the ``Bitmap``

        Returns:
            int: Index of the last clear bit.
        """
        return C.bitmap_last_unset(self._bitmap)

    @property
    def all_unset_bits(self) -> Tuple[int, ...]:
        """A tuple of all the clear bits in the ``Bitmap``

        Returns:
            tuple of int: Indices of all the clear bits.
        """
        def unsets():
            prev = self.first_unset()
            while prev != -1:
                yield prev
                prev = self.next_unset(prev)
        return tuple([x for x in unsets()])

    def __len__(self) -> int:
        """The length of a ``Bitmap`` is the number of bits it has

        Example:
            bitmap_length = len(bitmap)
        """
        return max(0, self.last_unset, self.last)

    def __or__(self, other: Bitmap) -> Bitmap:
        """Creates a new ``Bitmap`` using the logical OR operator

        Example:
            new_bitmap = bitmap1 | bitmap2
        """
        newset = Bitmap.alloc()
        C.bitmap_or(newset._bitmap, self._bitmap, other._bitmap)
        return newset

    def __ior__(self, other: Bitmap) -> Bitmap:
        """Sets this ``Bitmap`` to the logical OR of itself and another ``Bitmap``

        Example:
            bitmap1 |= bitmap2
        """
        C.bitmap_or(self._bitmap, self._bitmap, other._bitmap)
        return self

    def __and__(self, other: Bitmap) -> Bitmap:
        """Creates a new ``Bitmap`` from the logical AND of this and
        another ``Bitmap``

        Example:
            new_bitmap = bitmap1 & bitmap2
        """
        newset = Bitmap.alloc()
        C.bitmap_and(newset._bitmap, self._bitmap, other._bitmap)
        return newset

    def __iand__(self, other):
        """Sets this ``Bitmap`` to the logical AND of itself and another ``Bitmap``

        Example:
            bitmap1 &= bitmap2
        """
        C.bitmap_and(self._bitmap, self._bitmap, other._bitmap)
        return self

    def __bool__(self):
        """When a ``Bitmap`` object is coerced to boolean, its value is True if
        the ``Bitmap`` has any bits set

        Example:
            bits_are_set = bool(bitmap1)
        """
        return not self.iszero

    def __nonzero__(self):
        """This function is only supplied to help with coercion to bool in
        some corner cases.
        """
        return not self.iszero

    def andnot(self, other: Bitmap) -> Bitmap:
        """Returns a new ``Bitmap`` object set to the logical AND NOT of this
        and another Bitmap

        Args:
            other (Bitmap): The ``Bitmap`` whose inverse will be ANDed

        Returns:
            Bitmap: new ``Bitmap``
        """
        new = Bitmap.alloc()
        try:
            C.bitmap_andnot(new._bitmap, self._bitmap, other._bitmap)
        except TypeError:
            C.bitmap_andnot(new._bitmap, self._bitmap,
                            Bitmap.alloc(other)._bitmap)
        return new

    def __xor__(self, other):
        """Returns a new ``Bitmap`` containing the logical exclusive OR of
        this and another ``Bitmap``

        Example:
            new_bitmap = bitmap1 ^ bitmap2
        """
        newset = Bitmap.alloc()
        C.bitmap_xor(newset._bitmap, self._bitmap, other._bitmap)
        return newset

    def __ixor__(self, other):
        """Sets this ``Bitmap`` to the logical exclusive OR of this and
        another ``Bitmap``

        Example:
            bitmap1 ^= bitmap2
        """
        C.bitmap_xor(self._bitmap, self._bitmap, other._bitmap)

    def __invert__(self):
        """Creates a new ``Bitmap`` containing the inverse of this one

        Example:
            new_bitmap = ~bitmap1
        """
        newset = Bitmap.alloc()
        C.bitmap_not(newset._bitmap, self._bitmap)
        return newset

    def intersects(self, other: Bitmap) -> bool:
        """Returns True if this ``Bitmap`` has any set bit in common with
        another ``Bitmap``

        Args:
            other (Bitmap): The other ``Bitmap`` to compare.

        Returns:
            bool: [description]
        """
        return bool(C.bitmap_intersects(self._bitmap, other._bitmap))

    def isincluded(self, super_bitmap: Bitmap) -> bool:
        """Returns True if this ``Bitmap`` is included in another ``Bitmap``

        Args:
            super_bitmap (Bitmap): The other ``Bitmap`` to compare.

        Returns:
            bool: True if all the set bits in this ``Bitmap`` are set in
            the other.
        """
        return bool(C.bitmap_isincluded(self._bitmap, super_bitmap._bitmap))

    def __contains__(self, other: Union[int, Bitmap]) -> bool:
        """Returns True if the index is set in this ``Bitmap``, or if the
        other ``Bitmap`` is included in this one.

        Examples:
            # if bit 3 is set...
            if 3 in bitmap1:
            
            # if bitmap2 is included in bitmap1...
            if bitmap2 in bitmap1:
        """
        if isinstance(other, int):
            return self.isset(other)
        # "sub_bitmap in super_bitmap" is sub_bitmap.isincluded(super_bitmap)
        # and super_bitmap is this bitmap (self)
        return other.isincluded(self)

    def __eq__(self, other: Bitmap) -> bool:
        """Returns True if two ``Bitmaps`` have the exact same bits set.

        Example:
            if bitmap1 == bitmap2:
        """
        return bool(C.bitmap_isequal(self._bitmap, other._bitmap))

    def __ne__(self, other: Bitmap) -> bool:
        """Returns trues if two ``Bitmaps`` have different set bits.

        Example:
            if bitmap1 != bitmap2:
        """
        return not self == other

    def compare_first(self, other: Bitmap) -> int:
        """Compares the least significant bits of this and another ``Bitmap``

        Args:
            other (Bitmap): The other ``Bitmap`` to compare

        Returns:
            int: -1 if this ``Bitmap``'s least significant bit is lower.
                 0 if they have the same least significant bit.
                 1 if the other ``Bitmap``'s least significant bit is lower.
        """
        return C.bitmap_compare_first(self._bitmap, other._bitmap)

    def compare(self, other: Bitmap) -> int:
        """Makes a lexicagraphic comparison of this and another ``Bitmap``,
        starting at their highest indexes. Bits are compared moving down
        through the indices until a difference between bits is found.

        Args:
            other (Bitmap): the other bitmap to compare

        Returns:
            int: -1 if the differing bit is set in the other ``Bitmap``.
                 0 if the ``Bitmap``s are identical.
                 1 if the differing bit is set in this ``Bitmap``.
        """
        return C.bitmap_compare(self._bitmap, other._bitmap)


####
# API version
####
Version: int = C.get_api_version()
get_api_version = C.get_api_version


def version_string() -> str:
    """Returns the ``hwloc`` API version as a string

    Returns:
        str: Version string
    """    
    v = get_api_version()
    major = v >> 16
    minor = (v >> 8) & 0xFF
    rev = v & 0xFF
    s = '%u' % major
    if minor or rev:
        s += '.%u' % minor
    if rev:
        s += '.%u' % rev
    return s


class TopologyDiscoverySupport(object):
    """booleans describing actual discovery support for this topology
    This corresponds to the ``hwloc_topology_discovery_support`` structure of
    the ``hwloc`` library. These values are read-only."""

    def __init__(self, tds_ptr) -> None:
        """This class is only instantiated by the module."""
        self._tds = tds_ptr

    @property
    def pu(self) -> bool:
        """True if counting PU objects is supported."""
        return bool(self._tds.pu)

    @property
    def numa(self) -> bool:
        """True if counting the NUMA nodes is supported."""
        return bool(self._tds.numa)

    @property
    def numa_memory(self) -> bool:
        """True if detecting the amount of NUMA memory is supported"""
        return bool(self._tds.numa_memory)


class TopologyCpubindSupport(object):
    """booleans describing support for CPU binding
    This corresponds to the ``hwloc_topology_cpubind_support`` structure of
    the ``hwloc`` library. These values are read-only"""

    def __init__(self, cpubs_ptr) -> None:
        self._cpubs_ptr = cpubs_ptr

    @property
    def set_thisproc_cpubind(self) -> bool:
        """True if binding the whole current process is supported."""
        return bool(self._cpubs_ptr.set_thisproc_cpubind)

    @property
    def get_thisproc_cpubind(self) -> bool:
        """True if getting the binding of the whole current process
        is supported."""
        return bool(self._cpubs_ptr.get_thisproc_cpubind)

    @property
    def set_proc_cpubind(self) -> bool:
        """True if binding a whole given process is supported."""
        return bool(self._cpubs_ptr.set_proc_cpubind)

    @property
    def get_proc_cpubind(self) -> bool:
        """True if getting the binding of a whole given process
        is supported."""
        return bool(self._cpubs_ptr.get_proc_cpubind)

    @property
    def set_thisthread_cpubind(self) -> bool:
        """True if binding the current thread only is supported."""
        return bool(self._cpubs_ptr.set_thisthread_cpubind)

    @property
    def get_thisthread_cpubind(self) -> bool:
        """True if getting the binding of the current thread only
        is supported."""
        return bool(self._cpubs_ptr.get_thisthread_cpubind)

    @property
    def set_thread_cpubind(self) -> bool:
        """True if binding a given thread only is supported."""
        return bool(self._cpubs_ptr.set_thread_cpubind)

    @property
    def get_thread_cpubind(self) -> bool:
        """True if getting the binding of a given thread only is supported."""
        return bool(self._cpubs_ptr.get_thread_cpubind)

    @property
    def get_thisproc_last_cpu_location(self) -> bool:
        """True if getting the last processors where the whole current process
        ran is supported."""
        return bool(self._cpubs_ptr.get_thisproc_last_cpu_location)

    @property
    def get_proc_last_cpu_location(self) -> bool:
        """True if getting the last processor where a whole process ran
        is supported."""
        return bool(self._cpubs_ptr.get_proc_last_cpu_location)

    @property
    def get_thisthread_last_cpu_location(self) -> bool:
        """True if getting the last processor where the current thread ran
        is supported."""
        return bool(self._cpubs_ptr.get_thisthread_last_cpu_location)


class TopologyMembindSupport(object):
    """booleans describing support for memory binding
    This corresponds to the ``hwloc_topology_membind_support`` structure of
    the ``hwloc`` library. These values are read-only"""

    def __init__(self, mbs_ptr) -> None:
        self._mbs_ptr = mbs_ptr

    @property
    def set_thisproc_membind(self) -> bool:
        """True if binding the whole current process is supported."""
        return bool(self._mbs_ptr.set_thisproc_membind)

    @property
    def get_thisproc_membind(self) -> bool:
        """True if getting the binding of the whole current process
        is supported."""
        return bool(self._mbs_ptr.get_thisproc_membind)

    @property
    def set_proc_membind(self) -> bool:
        """True if binding a whole given process is supported."""
        return bool(self._mbs_ptr.set_proc_membind)

    @property
    def get_proc_membind(self) -> bool:
        """True if getting the binding of a whole given process
        is supported."""
        return bool(self._mbs_ptr.get_proc_membind)

    @property
    def set_thisthread_membind(self) -> bool:
        """True if binding the current thread only is supported."""
        return bool(self._mbs_ptr.set_thisthread_membind)

    @property
    def get_thisthread_membind(self) -> bool:
        """True if getting the binding of the current thread only
        is supported."""
        return bool(self._mbs_ptr.get_thisthread_membind)

    @property
    def set_area_membind(self) -> bool:
        """True if binding a given memory area is supported."""
        return bool(self._mbs_ptr.set_area_membind)

    @property
    def get_area_membind(self) -> bool:
        """True if getting the binding of a given memory area is supported."""
        return bool(self._mbs_ptr.get_area_membind)

    @property
    def alloc_membind(self) -> bool:
        """True if allocating a bound memory area is supported."""
        return bool(self._mbs_ptr.alloc_membind)

    @property
    def firsttouch_membind(self) -> bool:
        """True if first-touch policy is supported."""
        return bool(self._mbs_ptr.firsttouch_membind)

    @property
    def bind_membind(self) -> bool:
        """True if bind policy is supported."""
        return bool(self._mbs_ptr.bind_membind)

    @property
    def interleave_membind(self) -> bool:
        """True if interleave policy is supported."""
        return bool(self._mbs_ptr.interleave_membind)

    @property
    def nexttouch_membind(self) -> bool:
        """True if next-touch migration policy is supported."""
        return bool(self._mbs_ptr.nexttouch_membind)

    @property
    def migrate_membind(self) -> bool:
        """True if migration flags is supported."""
        return bool(self._mbs_ptr.migrate_membind)

    @property
    def get_area_memlocation(self) -> bool:
        """True if getting the last NUMA nodes where a memory area was
        allocated is supported."""
        return bool(self._mbs_ptr.get_area_memlocation)


class TopologySupport(object):
    """The support structures"""

    def __init__(self, ts_ptr) -> None:
        self._support = ts_ptr

    @property
    def discovery(self) -> TopologyDiscoverySupport:
        """Discovery support booleans"""
        return TopologyDiscoverySupport(self._support.discovery)

    @property
    def cpubind(self) -> TopologyCpubindSupport:
        """CPU binding support booleans"""
        return TopologyCpubindSupport(self._support.cpubind)

    @property
    def membind(self) -> TopologyMembindSupport:
        """Memory binding support booleans"""
        return TopologyMembindSupport(self._support.membind)


class MemoryPageType(object):
    """Defines local memory pages for a NUMA node"""

    def __init__(self, ompt_ptr) -> None:
        self._page_type = ompt_ptr

    @property
    def size(self) -> int:
        """Size of pages"""
        return self._page_type.size

    @property
    def count(self) -> int:
        """Number of pages of this size"""
        return self._page_type.count


class NumanodeAttr(object):
    "NUMA node-specific Object attributes"

    def __init__(self, memory_ptr) -> None:
        self._memory = memory_ptr

    @property
    def local_memory(self) -> int:
        """Local memory in bytes. Read/write."""
        return self._memory.local_memory

    @local_memory.setter
    def local_memory(self, value: int):
        self._memory.local_memory = value

    @property
    def page_types(self) -> Generator[MemoryPageType, None, None]:
        """Generator yielding ``MemoryPageType`` objects

        Yields:
            MemoryPageType
        """
        for t in self._memory.page_types:
            yield MemoryPageType(t)


class CacheAttr(object):
    """Cache-specific Object attributes"""

    def __init__(self, cache_ptr) -> None:
        self._cache = cache_ptr

    @property
    def size(self) -> int:
        """Size of cache in bytes"""
        return self._cache.size

    @property
    def depth(self) -> int:
        """Depth of cache (e.g., L1, L2, ...etc.)"""
        return self._cache.depth

    @property
    def linesize(self) -> int:
        """Cache-line size in bytes. 0 if unknown"""
        return self._cache.linesize

    @property
    def associativity(self) -> int:
        """Ways of associativity,
        -1 if fully associative, 0 if unknown"""
        return self._cache.associativity

    @property
    def type(self) -> int:
        """Cache type"""
        return self._cache.type


class PCIDevAttr(object):
    """PCI Device specific Object attributes"""

    def __init__(self, pcidev_ptr) -> None:
        self._pcidev = pcidev_ptr

    @property
    def domain(self) -> int:
        """PCI Domain"""
        return self._pcidev.domain

    @property
    def bus(self) -> int:
        """PCI Bus"""
        return self._pcidev.bus

    @property
    def dev(self) -> int:
        """PCI Dev"""
        return self._pcidev.dev

    @property
    def func(self) -> int:
        """PCI Func"""
        return self._pcidev.func

    @property
    def class_id(self) -> int:
        """Class ID"""
        return self._pcidev.class_id

    @property
    def vendor_id(self) -> int:
        """Vendor ID"""
        return self._pcidev.vendor_id

    @property
    def device_id(self) -> int:
        """Device ID"""
        return self._pcidev.device_id

    @property
    def subvendor_id(self) -> int:
        """Subvendor ID"""
        return self._pcidev.subvendor_id

    @property
    def subdevice_id(self) -> int:
        """Subdevice ID"""
        return self._pcidev.subdevice_id

    @property
    def revision(self) -> int:
        """Revision"""
        return self._pcidev.revision

    @property
    def linkspeed(self) -> float:
        """Link speed"""
        return self._pcidev.linkspeed


class BridgeAttrUpstream(object):
    """PCI Bridge upstream type values"""

    def __init__(self, upstream_ptr) -> None:
        self._upstream = upstream_ptr

    @property
    def pci(self) -> PCIDevAttr:
        """PCI device attributes"""
        return PCIDevAttr(self._upstream.pci)


class BridgeAttrDownstreamPCI(object):
    """PCI Bridge downstream attributes"""

    def __init__(self, pci) -> None:
        self._pci = pci

    @property
    def domain(self) -> int:
        """PCI domain"""
        return self._pci.domain

    @property
    def secondary_bus(self) -> int:
        """Secondary Bus"""
        return self._pci.secondary_bus

    @property
    def subordinate_bus(self) -> int:
        "Subordinate Bus"""
        return self._pci.subordinate_bus


class BridgeAttrDownstream(object):
    """Holder for the PCI bridge downstream attributes"""

    def __init__(self, downstream) -> None:
        self._downstream = downstream

    @property
    def pci(self) -> BridgeAttrDownstreamPCI:
        """Bridge downstream attributes"""
        return BridgeAttrDownstreamPCI(self._downstream.pci)


class BridgeAttr(object):
    """This class represents the ``hwloc_bridge_attr`` structure

    Examples:
        >>> for obj in topology.bridges:
        ...     assert obj.type == hwloc.OBJ_BRIDGE
        ...     if obj.attr.bridge.upstream_type == hwloc.OBJ_BRIDGE_HOST:
        ...         assert obj.attr.bridge.downstream_type == hwloc.OBJ_BRIDGE_PCI
        ...         print(' Found host->PCI bridge for domain %04x bus %02x-%02x' % (
        ...             obj.attr.bridge.downstream.pci.domain,
        ...             obj.attr.bridge.downstream.pci.secondary_bus,
        ...             obj.attr.bridge.downstream.pci.subordinate_bus))
        ...
        Found host->PCI bridge for domain 0000 bus 00-03
    """

    def __init__(self, bridge_ptr) -> None:
        self._bridge = bridge_ptr

    @property
    def upstream(self) -> BridgeAttrUpstream:
        """Upstream attributes"""
        return BridgeAttrUpstream(self._bridge.upstream)

    @property
    def upstream_type(self) -> int:
        """Type of the upstream side"""
        return self._bridge.upstream_type

    @property
    def downstream(self) -> BridgeAttrDownstream:
        "Downstream attributes"
        return BridgeAttrDownstream(self._bridge.downstream)

    @property
    def downstream_type(self) -> int:
        """Type of the downstream side"""
        return self._bridge.downstream_type

    @property
    def depth(self) -> int:
        """Depth"""
        return self._bridge.depth


class OSDevAttr(object):
    """OS Device specific Object attributes"""

    def __init__(self, osdev) -> None:
        self._osdev = osdev

    @property
    def type(self) -> int:
        """OS Device type"""
        return self._osdev.type


class GroupAttr(object):
    """Group-specific Object attributes"""

    def __init__(self, group_ptr) -> None:
        self._group = group_ptr

    @property
    def depth(self) -> int:
        """Depth of group object
        It may change if intermediate ``Group`` objects are added"""
        return self._group.depth

    @property
    def kind(self) -> int:
        """Internally-used kind of group"""
        return self._group.kind

    @property
    def subkind(self) -> int:
        """Internally-used subkind to distinguish different levels of
        groups with same kind"""
        return self._group.subkind

    @property
    def dont_merge(self) -> int:
        """Flag preventing groups from being automatically merged
        with identical parent or children"""
        return self._group.dont_merge


class ObjAttr():
    """This class represents the ``hwloc_obj_attr`` union
    As with the C structure, accessing the wrong type of attribute structure
    will cause undefined results."""

    def __init__(self, attr_ptr) -> None:
        self._objattr = attr_ptr

    @property
    def numanode(self) -> NumanodeAttr:
        """NUMA node attributes"""
        return NumanodeAttr(self._objattr.numanode)

    @property
    def cache(self) -> CacheAttr:
        """Cache attributes"""
        return CacheAttr(self._objattr.cache)

    @property
    def group(self) -> GroupAttr:
        """Group attributes"""
        return GroupAttr(self._objattr.group)

    @property
    def pcidev(self) -> PCIDevAttr:
        """PCI device attributes"""
        return PCIDevAttr(self._objattr.pcidev)

    @property
    def bridge(self) -> BridgeAttr:
        """PCI Bridge attributes"""
        return BridgeAttr(self._objattr.bridge)

    @property
    def osdev(self) -> OSDevAttr:
        """OS Device attributes"""
        return OSDevAttr(self._objattr.osdev)


class ObjInfo(object):
    """The hwloc_info structure

    ``ObjInfo`` can be coerced to ``str`` for debug printing
    """

    def __init__(self, info_ptr) -> None:
        self._info = info_ptr

    @property
    def name(self) -> str:
        """Info name"""
        return _unfate(self._info.name)

    @property
    def value(self) -> str:
        """Info value"""
        return _unfate(self._info.value)

    def __str__(self) -> str:
        return self.name + ':' + self.value


class Obj:
    pass


class Obj(object):
    """Class representing a topology object

    The application should not instantiate an ``Obj`` object. These are
    created by the ``hwloc`` library. Use the ``Topology`` object's
    ``alloc_group_object()`` to create new group objects.
    """

    def __init__(self, obj_ptr: C.obj_ptr) -> None:
        self._obj = obj_ptr

    @property
    def type(self) -> int:
        """Type of object"""
        return self._obj.type

    @property
    def subtype(self) -> str:
        """Subtype string to better describe the type field"""
        if self._obj.subtype is None:
            return None
        return _unfate(self._obj.subtype)

    @property
    def os_index(self) -> int:
        """OS-provided physical index number"""
        return self._obj.os_index

    @property
    def name(self) -> str:
        """Object-specific name if any
        
        This property can be set for ``Group`` objects"""
        if self._obj.name is None:
            return None
        return _unfate(self._obj.name)

    @name.setter
    def name(self, value: str):
        self._obj.name = value.encode('utf8')

    @property
    def total_memory(self) -> int:
        return self._obj.total_memory

    @property
    def attr(self) -> ObjAttr:
        return ObjAttr(self._obj.attr)

    @property
    def depth(self) -> int:
        return self._obj.depth

    @property
    def logical_index(self) -> int:
        return self._obj.logical_index

    @property
    def next_cousin(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.next_cousin)
        except C.NULLError:
            return None

    @property
    def prev_cousin(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.prev_cousin)
        except C.NULLError:
            return None

    @property
    def parent(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.parent)
        except C.NULLError:
            return None

    @property
    def sibling_rank(self) -> int:
        return self._obj.sibling_rank

    @property
    def next_sibling(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.next_sibling)
        except C.NULLError:
            return None

    @property
    def prev_sibling(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.prev_sibling)
        except C.NULLError:
            return None

    @property
    def arity(self) -> int:
        return self._obj.arity

    @property
    def first_child(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.first_child)
        except C.NULLError:
            return None

    @property
    def last_child(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.last_child)
        except C.NULLError:
            return None

    @property
    def children(self) -> Generator[Obj, None, None]:
        next_child = self.first_child
        while next_child is not None:
            yield next_child
            next_child = next_child.next_sibling

    @property
    def memory_arity(self) -> int:
        return self._obj.memory_arity

    @property
    def memory_first_child(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.memory_first_child)
        except C.NULLError:
            return None

    @property
    def memory_children(self) -> Generator[Obj, None, None]:
        next_child = self.memory_first_child
        while next_child is not None:
            yield next_child
            next_child = next_child.next_sibling

    @property
    def memory_last_child(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.memory_last_child)
        except C.NULLError:
            return None

    @property
    def io_arity(self) -> int:
        return self._obj.io_arity

    @property
    def io_first_child(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.io_first_child)
        except C.NULLError:
            return None

    @property
    def io_children(self) -> Generator[Obj, None, None]:
        next_child = self.io_first_child
        while next_child is not None:
            yield next_child
            next_child = next_child.next_sibling

    @property
    def io_last_child(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.io_last_child)
        except C.NULLError:
            return None

    @property
    def misc_arity(self) -> int:
        return self._obj.misc_arity

    @property
    def misc_first_child(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.misc_first_child)
        except C.NULLError:
            return None

    @property
    def misc_children(self) -> Generator[Obj, None, None]:
        next_child = self.misc_first_child
        while next_child is not None:
            yield next_child
            next_child = next_child.next_sibling

    @property
    def misc_last_child(self) -> Optional[Obj]:
        try:
            return Obj(self._obj.misc_last_child)
        except C.NULLError:
            return None

    @property
    def cpuset(self) -> Optional[Bitmap]:
        try:
            return Bitmap(C.bitmap_dup(self._obj.cpuset))
        except C.NULLError:
            return None

    @cpuset.setter
    def cpuset(self, value: Bitmap):
        self._obj.cpuset = value._bitmap

    @property
    def complete_cpuset(self) -> Optional[Bitmap]:
        try:
            return Bitmap(C.bitmap_dup(self._obj.complete_cpuset))
        except C.NULLError:
            return None

    @property
    def nodeset(self) -> Optional[Bitmap]:
        try:
            return Bitmap(C.bitmap_dup(self._obj.nodeset))
        except C.NULLError:
            return None

    @nodeset.setter
    def nodeset(self, value: Bitmap):
        self._obj.nodeset = value._bitmap

    @property
    def complete_nodeset(self) -> Optional[Bitmap]:
        try:
            return Bitmap(C.bitmap_dup(self._obj.complete_nodeset))
        except C.NULLError:
            return None

    @property
    def infos(self) -> List[ObjInfo]:
        return tuple([ObjInfo(i) for i in self._obj.infos])

    @property
    def infos_count(self) -> int:
        return self._obj.infos_count

    def get_userdata(self):
        return self._obj.userdata

    def set_userdata(self, data) -> None:
        try:
            self._obj.userdata = data
        except TypeError:
            raise ArgError('userdata must be integer type')

    userdata = property(get_userdata, set_userdata,
                        doc='userdata can only be an integer value')

    @property
    def gp_index(self) -> int:
        return self._obj.gp_index

    # Object/String Conversion
    @property
    def type_string(self) -> str:
        return C.obj_type_string(self.type)

    @classmethod
    def type_sscanf(cls, string: str) -> Tuple[int, ObjAttr]:
        return C.type_sscanf(string)

    @classmethod
    def string_of_type(cls, type_: int) -> str:
        """return a string describing an object type"""
        return C.obj_type_string(type_)

    def type_asprintf(self, verbose: int = 0) -> str:
        return C.obj_type_asprintf(self._obj, verbose)

    def attr_asprintf(self, separator: str = '#', verbose: int = 0) -> str:
        return C.obj_attr_asprintf(self._obj, separator, verbose)

    def get_info_by_name(self, name: str) -> Optional[str]:
        return C.obj_get_info_by_name(self._obj, name)

    def add_info(self, name: str, value: str) -> None:
        return C.obj_add_info(self._obj, name, value)

    # Kinds of object type
    @classmethod
    def type_is_normal(cls, type_: int) -> bool:
        return C.obj_type_is_normal(type_)

    @classmethod
    def type_is_io(cls, type_: int) -> bool:
        return C.obj_type_is_io(type_)

    @classmethod
    def type_is_memory(cls, type_: int) -> bool:
        return C.obj_type_is_memory(type_)

    @classmethod
    def type_is_cache(cls, type_: int) -> bool:
        return C.obj_type_is_cache(type_)

    @classmethod
    def type_is_dcache(cls, type_: int) -> bool:
        return C.obj_type_is_dcache(type_)

    @classmethod
    def type_is_icache(cls, type_: int) -> bool:
        return C.obj_type_is_icache(type_)

    def add_other_obj_sets(self, src: Obj) -> None:
        C.obj_add_other_obj_sets(self._obj, src._obj)

    def __eq__(self, other) -> bool:
        """Two Obj instances are equal if they both encapsulate the same
        heloc_obj structure"""
        return self._obj == other._obj

    def __ne__(self, other) -> bool:
        return self._obj != other._obj

    def __str__(self) -> str:
        s = ''
        for i in self.infos:
            s += ' ' + str(i)
        return self.type_asprintf() + ',' + self.attr_asprintf() + s

    # Basic Traversal Helpers
    def get_ancestor_obj_by_depth(self, depth: int) -> Optional[Obj]:
        try:
            return Obj(C.get_ancestor_obj_by_depth(None, depth, self._obj))
        except C.NULLError:
            return None

    def get_ancestor_obj_by_type(self, type_: int) -> Optional[Obj]:
        try:
            return Obj(C.get_ancestor_obj_by_type(None, type_, self._obj))
        except C.NULLError:
            return None

    def get_common_ancestor_obj(self, obj2: Obj) -> Optional[Obj]:
        try:
            return Obj(C.get_common_ancestor_obj(None, self._obj, obj2._obj))
        except C.NULLError:
            return None

    @classmethod
    def get_common_ancestor(cls, obj1: Obj, obj2: Obj) -> Optional[Obj]:
        """Returns the common parent object of 2 objects"""
        try:
            return Obj(C.get_common_ancestor_obj(None, obj1._obj, obj2._obj))
        except C.NULLError:
            return None

    def is_in_subtree(self, subtree_root: Obj) -> bool:
        """Returns true if obj is inside the subtree"""
        return C.obj_is_in_subtree(None, self._obj, subtree_root._obj)

    def get_next_child(self, prev: Optional[Obj]) -> Optional[Obj]:
        prev_obj = None
        if prev is not None:
            prev_obj = prev._obj
        try:
            return Obj(C.get_next_child(None, self._obj, prev_obj))
        except C.NULLError:
            return None

    # Cache-specific Finding Helpers
    def get_shared_cache_covering(self) -> Optional[Obj]:
        """Get the first cache shared between an object and somebody else"""
        try:
            return Obj(C.get_shared_cache_covering_obj(None, self._obj))
        except C.NULLError:
            return None

    # OpenGL display specific functions
    def gl_get_display(self) -> (Optional[int], Optional[int]):
        """Get the OpenGL display port and device corresponding to this OS object"""
        try:
            return C.gl_get_display_by_osdev(None, self._obj)
        except OSError:
            return None, None

    # Advanced I/O object traversal helpers
    @property
    def non_io_ancestor(self) -> Optional[Obj]:
        """Get the first non-I/O ancestor object"""
        try:
            return Obj(C.get_non_io_ancestor_obj(None, self._obj))
        except C.NULLError:
            return None


def _typeFromString(type_: Union[int, str]) -> int:
    if isinstance(type_, int):
        return type_
    try:
        type_ = int(type_, 10)
        return type_
    except ValueError:
        pass
    except TypeError:
        pass
    try:
        type_ = Obj.type_sscanf(type_)[0]
    except C.ArgError:
        raise ArgError('unrecognized Type string: ' + type_)
    return type_


class Distances(object):
    """distances between objects"""

    def __init__(self, distances_ptr: C.distances_ptr):
        self._distances = distances_ptr

    @property
    def nbobjs(self) -> int:
        return self._distances.nbobjs

    @property
    def objs(self) -> List[Obj]:
        return [
            self._distances.objs[i] for i in range(self.nbobjs)
        ]

    @property
    def kind(self) -> int:
        return self._distances.kind

    @property
    def values(self) -> List[int]:
        """The distance from the i-th to -the j- object is
        stored in slot i*nbobjs+j"""
        return [
            self._distances.values[i] for i in range(self.nbobjs * self.nbobjs)
        ]

    def obj_index(self, obj: Obj) -> int:
        return C.distances_obj_index(self._distances, obj._obj)

    def obj_pair_values(self, obj1: Obj, obj2: Obj) -> Tuple[int, int]:
        return C.distances_obj_pair_values(self._distances, obj1._obj, obj2._obj)


class TopologyDiff:
    pass


class TopologyDiffGeneric(object):

    def __init__(self, ptr) -> None:
        self._generic = ptr

    @property
    def type(self) -> int:
        return self._generic.type

    @property
    def next(self) -> Optional[TopologyDiff]:
        if self._generic.next is None:
            return None
        return TopologyDiff(self._generic.next)


class TopologyDiffObjAttrUint64(object):

    def __init__(self, ptr) -> None:
        self._uint64 = ptr

    @property
    def type(self) -> None:
        return self._uint64.type

    @property
    def index(self) -> int:
        return self._uint64.index

    @property
    def oldvalue(self) -> int:
        return self._uint64.oldvalue

    @property
    def newvalue(self) -> int:
        return self._uint64.newvalue


class TopologyDiffObjAttrString(object):

    def __init__(self, ptr) -> None:
        self._string = ptr

    @property
    def type(self) -> int:
        return self._string.type

    @property
    def name(self) -> str:
        return self._string.name

    @property
    def oldvalue(self) -> str:
        return self._string.oldvalue

    @property
    def newvalue(self):
        return self._string.newvalue


class TopologyDiffObjAttrGeneric(object):

    def __init__(self, ptr):
        self._generic = ptr

    @property
    def type(self):
        return self._generic.type


class TopologyDiffObjAttr(object):

    def __init__(self, ptr):
        self._obj_attr = ptr

    @property
    def type(self):
        return self._obj_attr.type

    @property
    def next(self) -> Optional[TopologyDiff]:
        if self._obj_attr.next is None:
            return None
        return TopologyDiff(self._obj_attr.next)

    @property
    def obj_depth(self):
        return self._obj_attr.obj_depth

    @property
    def obj_index(self):
        return self._obj_attr.obj_index

    @property
    def diff(self):
        return TopologyDiffObjAttrU(self._obj_attr.diff)


class TopologyDiffObjAttrU(object):

    def __init__(self, ptr):
        self._obj_attr_u = ptr

    @property
    def generic(self):
        return TopologyDiffObjAttrGeneric(self._obj_attr_u.generic)

    @property
    def uint64(self):
        assert self.generic.type in (TOPOLOGY_DIFF_OBJ_ATTR_SIZE,)
        return TopologyDiffObjAttrUint64(self._obj_attr_u.uint64)

    @property
    def string(self):
        assert self.generic.type in (
            TOPOLOGY_DIFF_OBJ_ATTR_NAME, TOPOLOGY_DIFF_OBJ_ATTR_INFO)
        return TopologyDiffObjAttrString(self._obj_attr_u.string)


class TopologyDiffTooComplex(object):

    def __init__(self, ptr):
        self._too_complex = ptr

    @property
    def type(self):
        return self._too_complex.type

    @property
    def next(self) -> Optional[TopologyDiff]:
        if self._too_complex.next is None:
            return None
        return TopologyDiff(self._too_complex.next)

    @property
    def obj_depth(self):
        return self._too_complex.obj_depth

    @property
    def obj_index(self):
        return self._too_complex.obj_index


class TopologyDiff(object):

    def __init__(self, ptr: C.topology_diff_ptr):
        self._diff = ptr

    @property
    def generic(self):
        return TopologyDiffGeneric(self._diff.generic)

    @property
    def obj_attr(self):
        assert self.generic.type == TOPOLOGY_DIFF_OBJ_ATTR
        return TopologyDiffObjAttr(self._diff.obj_attr)

    @property
    def too_complex(self):
        assert self.generic.type == TOPOLOGY_DIFF_TOO_COMPLEX
        return TopologyDiffTooComplex(self._diff.too_complex)


class Topology(object):
    """ NOTE you need to hold the topology reference as long as you continue to
    hold a reference to any constituent part of it (object, difference, etc.)"""

    def __init__(self, ptr=None):
        self.userdata = None
        if ptr is None:
            self._topology = C.topology_ptr()
        else:
            self._topology = ptr

    def load(self):
        C.topology_load(self._topology)

    def abi_check(self) -> None:
        C.topology_abi_check(self._topology)

    def check(self):
        C.topology_check(self._topology)

    # Configure Topology Detection
    def ignore_type(self, type_):
        C.topology_ignore_type(self._topology, _typeFromString(type_))

    def ignore_type_keep_structure(self, type_):
        C.topology_ignore_type_keep_structure(self._topology,
                                              _typeFromString(type_))

    def ignore_all_keep_structure(self):
        C.topology_ignore_all_keep_structure(self._topology)

    def set_flags(self, flags):
        C.topology_set_flags(self._topology, flags)

    def get_flags(self) -> int:
        return C.topology_get_flags(self._topology)

    flags = property(get_flags, set_flags)

    def set_pid(self, pid):
        C.topology_set_pid(self._topology, pid)

    def set_synthetic(self, string):
        C.topology_set_synthetic(self._topology, string)

    def set_xml(self, xmlpath):
        C.topology_set_xml(self._topology, xmlpath)

    def set_xmlbuffer(self, xmlbuffer):
        C.topology_set_xmlbuffer(self._topology, xmlbuffer)

    def get_support(self):
        return TopologySupport(C.topology_get_support(self._topology))

    support = property(get_support)

    def set_type_filter(self, type_: int, filter: int) -> None:
        C.topology_set_type_filter(self._topology, type_, filter)

    def get_type_filter(self, type_: int) -> int:
        return C.topology_get_type_filter(self._topology, type_)

    def set_all_types_filter(self, filter: int) -> None:
        C.topology_set_all_types_filter(self._topology, filter)

    def set_cache_types_filter(self, filter: int) -> None:
        C.topology_set_cache_types_filter(self._topology, filter)

    def set_icache_types_filter(self, filter: int) -> None:
        C.topology_set_icache_types_filter(self._topology, filter)

    def set_io_types_filter(self, filter: int) -> None:
        C.topology_set_io_types_filter(self._topology, filter)

    # Exporting Topologies to XML
    def export_xml(self, path: str, flags: int = 0):
        C.topology_export_xml(self._topology, path, flags)

    def export_xmlbuffer(self, flags: int = 0) -> str:
        return C.topology_export_xmlbuffer(self._topology, flags)

    def set_userdata_export_callback(self, cb):
        C.topology_set_userdata_export_callback(self._topology, cb)

    def export_obj_userdata(self, reserved, obj, name, buffer1):
        """Export some object userdata to XML"""
        C.export_obj_userdata(reserved, self._topology,
                              obj._obj, name, buffer1)

    def export_obj_userdata_base64(self, reserved, obj, name, buffer1):
        """Encode and export some object userdata to XML"""
        C.export_obj_userdata_base64(reserved, self._topology, obj._obj, name,
                                     buffer1)

    def set_userdata_import_callback(self, cb):
        """Set the application-specific callback for importing userdata"""
        C.topology_set_userdata_import_callback(self._topology, cb)

    # Exporting Topologies to Synthetic
    def export_synthetic(self, flags: int, size: int = 1024) -> str:
        """Export the topology as a synthetic string"""
        return C.topology_export_synthetic(self._topology, flags, size)

    # Distances between objects
    def distances_get(self, kind: int, flags: int) -> List[Distances]:
        distances = C.distances_get(self._topology, kind, flags)
        return tuple(Distances(d) for d in distances)

    def distances_get_by_depth(self, depth: int, kind: int, flags: int) -> List[Distances]:
        distances = C.distances_get_by_depth(self._topology, depth, kind, flags)
        return tuple(Distances(d) for d in distances)

    def distances_get_by_type(self, type_: int, kind: int, flags: int) -> List[Distances]:
        distances = C.distances_get_by_type(self._topology, type_, kind, flags)
        return tuple(Distances(d) for d in distances)

    def distances_add(self, objs: List[Obj], values: List[int], kind: int, flags: int) -> None:
        obj_list = tuple(o._obj for o in objs)
        C.distances_add(self._topology, obj_list, values, kind, flags)

    def distances_remove(self) -> None:
        C.distances_remove(self._topology)

    def distances_remove_by_depth(self, depth: int) -> None:
        C.distances_remove_by_depth(self._topology, depth)

    def distances_remove_by_type(self, type_: int) -> None:
        C.distances_remove_by_type(self._topology, type_)

    # Get Some topology Information
    def get_depth(self):
        return C.topology_get_depth(self._topology)

    depth = property(get_depth)

    def get_type_depth(self, type_):
        if isinstance(type_, str):
            type_ = _typeFromString(type_)
        return C.get_type_depth(self._topology, type_)

    def get_memory_parents_depth(self) -> int:
        return C.get_memory_parents_depth(self._topology)

    memory_parents_depth = property(get_memory_parents_depth)

    def get_depth_type(self, depth):
        """Returns the type of objects at depth"""
        try:
            return C.get_depth_type(self._topology, depth)
        except C.ArgError:
            raise ArgError('get_depth_tpye')

    def get_nbobjs_by_depth(self, depth):
        return C.get_nbobjs_by_depth(self._topology, depth)

    def get_nbobjs_by_type(self, type_):
        return C.get_nbobjs_by_type(self._topology, type_)

    @property
    def is_thissystem(self):
        return bool(C.topology_is_thissystem(self._topology))

    # CPU binding
    def set_cpubind(self, set1, flags):
        """Bind current process or thread on cpus given in physical bitmap"""
        C.set_cpubind(self._topology, set1._bitmap, flags)

    def get_cpubind(self, flags):
        """Get current process or thread binding"""
        return Bitmap(C.get_cpubind(self._topology, flags))

    def set_proc_cpubind(self, pid, set1, flags):
        """Bind a process on cpus given in physical bitmap"""
        C.set_proc_cpubind(self._topology, pid, set1._bitmap, flags)

    def get_proc_cpubind(self, pid, flags):
        """Get the current physical binding of process"""
        return Bitmap(C.get_proc_cpubind(self._topology, pid, flags))

    def set_thread_cpubind(self, thread, set1, flags):
        """Bind a thread on cpus given in physical bitmap"""
        C.set_thread_cpubind(self._topology, thread, set1._bitmap, flags)

    def get_thread_cpubind(self, thread, flags):
        """Get the current physical binding of thread"""
        return Bitmap(C.get_thread_cpubind(self._topology, thread, flags))

    def get_last_cpu_location(self, flags):
        """Get the last physical CPU where the current process or thread ran"""
        return Bitmap(C.get_last_cpu_location(self._topology, flags))

    def get_proc_last_cpu_location(self, pid, flags):
        """Get the last physical CPU where a process ran"""
        return Bitmap(C.get_proc_last_cpu_location(self._topology, pid, flags))

    # Memory binding
    def set_membind_nodeset(self, nodeset, policy, flags):
        """Set the default memory binding policy of the current process or
        thread to prefer the NUMA node(s) specified by physical nodeset"""
        C.set_membind_nodeset(self._topology, nodeset._bitmap, policy, flags)

    def set_membind(self, set_, policy, flags):
        """Set the default memory binding policy of the current process or
        thread to prefer the NUMA node(s) near the specified physical cpuset
        or nodeset"""
        C.set_membind(self._topology, set_._bitmap, policy, flags)

    def get_membind_nodeset(self, flags):
        """Query the default memory binding policy and physical locality of the
        current process or thread
        Returns: (Bitmap, policy)"""
        b, p = C.get_membind_nodeset(self._topology, flags)
        return Bitmap(b), p

    def get_membind(self, flags):
        """Query the default memory binding policy and physical locality of the
        current process or thread (the locality is returned in cpuset as CPUs near the locality's actual NUMA node(s))
        Returns: (Bitmap, policy)"""
        b, p = C.get_membind(self._topology, flags)
        return Bitmap(b), p

    def set_proc_membind_nodeset(self, pid, nodeset, policy, flags):
        """Set the default memory binding policy of the specified process to prefer the NUMA node(s) specified by physical nodeset"""
        C.set_proc_membind_nodeset(self._topology, pid, nodeset._bitmap, policy,
                                   flags)

    def set_proc_membind(self, pid, set_, policy, flags):
        """Set the default memory binding policy of the specified process to prefer the NUMA node(s) near the specified physical cpuset or nodeset"""
        C.set_proc_membind(self._topology, pid, set_._bitmap, policy, flags)

    def get_proc_membind_nodeset(self, pid, flags):
        """Query the default memory binding policy and physical locality of the specified process"""
        b, p = C.get_proc_membind_nodeset(self._topology, pid, flags)
        return Bitmap(b), p

    def get_proc_membind(self, pid, flags):
        """Query the default memory binding policy and physical locality of the specified process (the locality is returned in cpuset as CPUs near the locality's actual NUMA node(s))
        Returns: (Bitmap, policy)"""
        b, p = C.get_proc_membind(self._topology, pid, flags)
        return Bitmap(b), p

    def set_area_membind_nodeset(self, addr, len1, nodeset, policy, flags):
        """Bind the already-allocated memory identified by (addr, len1) to the NUMA node(s) in physical nodeset"""
        C.set_area_membind_nodeset(self._topology, addr, len1, nodeset._bitmap,
                                   policy, flags)

    def set_area_membind(self, addr, len1, cpuset, policy, flags):
        """Bind the already-allocated memory identified by (addr, len1) to the NUMA node(s) near physical cpuset or nodeset"""
        C.set_area_membind(self._topology, addr, len1,
                           cpuset._bitmap, policy, flags)

    def get_area_membind_nodeset(self, addr, len1, flags):
        """Query the physical NUMA node(s) and binding policy of the memory identified by (addr, len1)
        Returns: (Bitmap, policy)"""
        b, p = C.get_area_membind_nodeset(self._topology, addr, len1, flags)
        return Bitmap(b), p

    def get_area_membind(self, addr, len1, flags):
        """Query the CPUs near the physical NUMA node(s) and binding policy of the memory identified by (addr, len1)
        Returns: (Bitmap, policy)"""
        b, p = C.get_area_membind(self._topology, addr, len1, flags)
        return Bitmap(b), p

    def get_area_memlocation(self, addr, len_, flags):
        """Get the NUMA nodes where memory identified by ( addr, len ) is physically allocated."""
        return Bitmap(C.get_area_memlocation(self._topology, addr, len_, flags))

    def alloc(self, len1):
        """Allocate some memory"""
        return C.alloc(self._topology, len1)

    def alloc_membind_nodeset(self, len1, nodeset, policy, flags):
        """Allocate some memory on the given physical nodeset"""
        return C.alloc_membind_nodeset(self._topology, len1, nodeset._bitmap,
                                       policy, flags)

    def alloc_membind(self, len1, cpuset, policy, flags):
        """Allocate some memory on memory nodes near the given physical cpuset or nodeset"""
        return C.alloc_membind(self._topology, len1, cpuset._bitmap, policy, flags)

    def free(self, addr, len1):
        """Free memory that was previously allocated by hwloc_alloc() or hwloc_alloc_membind*()"""
        C.free(self._topology, addr, len1)

    # Modifying a loaded topology
    def insert_misc_object_by_cpuset(self, cpuset, name) -> Obj:
        """Add a MISC object to the topology"""
        return Obj(C.topology_insert_misc_object_by_cpuset(self._topology,
                                                           cpuset._bitmap,
                                                           name))

    def insert_misc_object_by_parent(self, parent, name) -> Obj:
        """Add a MISC object as a leaf of the topology"""
        return Obj(C.topology_insert_misc_object_by_parent(self._topology,
                                                           parent._obj,
                                                           name))

    def restrict(self, cpuset, flags=0):
        """Restrict the topology to the given CPU set"""
        C.topology_restrict(self._topology, cpuset._bitmap, flags)

    def insert_misc_object(self, parent: Obj, name: str) -> Optional[Obj]:
        """Add a MISC object as a leaf of the topology"""
        try:
            return Obj(
                C.topology_insert_misc_object(self._topology, parent._obj, name)
            )
        except C.NULLError:
            return None

    def alloc_group_object(self) -> Obj:
        return Obj(C.topology_alloc_group_object(self._topology))

    def insert_group_object(self, obj: Obj) -> Obj:
        try:
            return Obj(C.insert_group_object(self._topology, obj._obj))
        except C.NULLError:
            return None

    def dup(self):
        """Duplicate a topology"""
        return Topology(C.topology_dup(self._topology))

    # Object Type Helpers
    def get_type_or_below_depth(self, type_):
        """Returns the depth of objects of type type\_ or below"""
        return C.get_type_or_below_depth(self._topology, type_)

    def get_type_or_above_depth(self, type_):
        """Returns the depth of objects of type type\_ or above"""
        return C.get_type_or_above_depth(self._topology, type_)

    # Retrieve Objects
    def get_obj_by_depth(self, depth, index) -> Obj:
        try:
            return Obj(C.get_obj_by_depth(self._topology, depth, index))
        except C.NULLError:
            return None

    def get_obj_by_type(self, type_, index) -> Obj:
        try:
            return Obj(C.get_obj_by_type(self._topology, type_, index))
        except C.NULLError:
            return None

    def type_sscanf_as_depth(self, string: str) -> Tuple[int, int]:
        return C.type_sscanf_as_depth(self._topology, string)

    # Basic Traversal Helpers
    @property
    def root_obj(self):
        return Obj(C.get_root_obj(self._topology))

    def get_next_obj_by_depth(self, depth, prev=None) -> Optional[Obj]:
        """Returns the next object at depth"""
        if prev is not None:
            prev = prev._obj
        try:
            # return Obj(C.get_next_obj_by_depth(self._topology, depth, prev))
            obj = C.get_next_obj_by_depth(self._topology, depth, prev)
            return Obj(obj)
        except C.NULLError:
            return None

    def objs_by_depth(self, depth: int) -> Generator[Obj, None, None]:
        """Generator of Obj instances at depth"""
        prev = self.get_next_obj_by_depth(depth, None)
        while prev is not None:
            yield prev
            prev = self.get_next_obj_by_depth(depth, prev)

    def get_next_obj_by_type(self, type_, prev=None) -> Optional[Obj]:
        """ Returns the next object of type"""
        if prev is not None:
            prev = prev._obj
        try:
            return Obj(C.get_next_obj_by_type(self._topology, type_, prev))
        except C.NULLError:
            return None

    def objs_by_type(self, type_, prev=None) -> Generator[Obj, None, None]:
        prev = self.get_next_obj_by_type(type_, prev)
        while prev:
            yield prev
            prev = self.get_next_obj_by_type(type_, prev)

    def obj_is_in_subtree(self, obj, subtree_root):
        return bool(C.obj_is_in_subtree(self._topology, obj._obj, subtree_root._obj))

    # Finding Objects Inside a CPU set
    def get_first_largest_obj_inside_cpuset(self, set1):
        """Get the first largest object included in the given cpuset"""
        try:
            return Obj(C.get_first_largest_obj_inside_cpuset(self._topology,
                                                             set1._bitmap))
        except C.NULLError:
            return None

    def get_largest_objs_inside_cpuset(self, set1, max1):
        """Get the set of largest objects covering exactly a given cpuset"""
        try:
            l = [Obj(x) for x in C.get_largest_objs_inside_cpuset(self._topology,
                                                                  set1._bitmap,
                                                                  max1)
                 ]
            return tuple(l)
        except C.ArgError:
            raise ArgError('get_largest_objs_inside_cpuset')

    def get_next_obj_inside_cpuset_by_depth(self, set1, depth, prev=None):
        """Next same-depth object covering at least CPU set"""
        if prev is not None:
            prev = prev._obj
        try:
            return Obj(C.get_next_obj_inside_cpuset_by_depth(self._topology,
                                                             set1._bitmap, depth,
                                                             prev))
        except C.NULLError:
            return None

    def objs_inside_cpuset_by_depth(self, set1, depth, prev=None):
        """Iterate same-depth objects covering at least the cpuset"""
        prev = self.get_next_obj_inside_cpuset_by_depth(set1, depth, prev)
        while prev:
            yield prev
            prev = self.get_next_obj_inside_cpuset_by_depth(set1, depth, prev)

    def get_next_obj_inside_cpuset_by_type(self, set1, type_, prev=None):
        """Next same-type object covering at least CPU set"""
        if prev is not None:
            prev = prev._obj
        try:
            return Obj(C.get_next_obj_inside_cpuset_by_type(self._topology,
                                                            set1._bitmap, type_,
                                                            prev))
        except C.NULLError:
            return None

    def objs_inside_cpuset_by_type(self, set1, type_, prev=None):
        """Iterate through same-type objects covering at least the cpuset"""
        prev = self.get_next_obj_inside_cpuset_by_type(set1, type_, prev)
        while prev:
            yield prev
            prev = self.get_next_obj_inside_cpuset_by_type(set1, type_, prev)

    def get_obj_inside_cpuset_by_depth(self, set1, depth, idx):
        """ Return the index-th object at depth included in CPU set"""
        try:
            return Obj(C.get_obj_inside_cpuset_by_depth(self._topology,
                                                        set1._bitmap, depth, idx))
        except C.NULLError:
            return None

    def get_obj_inside_cpuset_by_type(self, set1, type_, idx):
        """Return the idx-th object of type included in CPU set"""
        try:
            return Obj(C.get_obj_inside_cpuset_by_type(self._topology, set1._bitmap,
                                                       type_, idx))
        except C.NULLError:
            return None

    def get_nbobjs_inside_cpuset_by_depth(self, set1, depth):
        return C.get_nbobjs_inside_cpuset_by_depth(self._topology, set1._bitmap,
                                                   depth)

    def get_nbobjs_inside_cpuset_by_type(self, set1, type_):
        return C.get_nbobjs_inside_cpuset_by_type(self._topology, set1._bitmap,
                                                  type_)

    def get_obj_index_inside_cpuset(self, set1, obj):
        """Return the logical index among the objects included in CPU set"""
        return C.get_obj_index_inside_cpuset(self._topology, set1._bitmap, obj._obj)

    # Finding a single Object covering at least a CPU set
    def get_child_covering_cpuset(self, set1, parent):
        """Get the child covering at least the CPU set"""
        try:
            return Obj(C.get_child_covering_cpuset(self._topology, set1._bitmap,
                                                   parent._obj))
        except C.NULLError:
            return None

    def get_obj_covering_cpuset(self, set1):
        """Get the lowest object covering at least the CPU set"""
        try:
            return Obj(C.get_obj_covering_cpuset(self._topology, set1._bitmap))
        except C.NULLError:
            return None

    # Finding a set of similar Objects covering at least a CPU set
    def get_next_obj_covering_cpuset_by_depth(self, set1, depth, prev=None):
        """Next same-depth object covering at least this CPU set"""
        if prev is not None:
            prev = prev._obj
        try:
            return Obj(C.get_next_obj_covering_cpuset_by_depth(self._topology,
                                                               set1._bitmap, depth,
                                                               prev))
        except C.NULLError:
            return None

    def objs_covering_cpuset_by_depth(self, set1, depth, prev=None):
        """Iterate through same-depth objects covering at least this CPU set"""
        prev = self.get_next_obj_covering_cpuset_by_depth(set1, depth, prev)
        while prev:
            yield prev
            prev = self.get_next_obj_covering_cpuset_by_depth(
                set1, depth, prev)

    def get_next_obj_covering_cpuset_by_type(self, set1, type_, prev=None):
        """Next same-type object covering at least this CPU set"""
        if prev is not None:
            prev = prev._obj
        try:
            return Obj(
                C.get_next_obj_covering_cpuset_by_type(self._topology,
                                                       set1._bitmap,
                                                       _typeFromString(type_),
                                                       prev))
        except C.NULLError:
            return None

    def objs_covering_cpuset_by_type(self, set1, type_, prev=None):
        """Iterate through same-type objects covering at least this CPU set"""
        prev = self.get_next_obj_covering_cpuset_by_type(
            set1, type_, prev)
        while prev:
            yield prev
            prev = self.get_next_obj_covering_cpuset_by_type(set1, type_, prev)

    # Cache-specific Finding Helpers
    def get_cache_type_depth(self, cachelevel, cachetype):
        """Find the depth of cache objects matching cache depth and type"""
        return C.get_cache_type_depth(self._topology, cachelevel, cachetype)

    def get_cache_covering_cpuset(self, set1):
        """Get the first cache covering a cpuset"""
        try:
            return Obj(C.get_cache_covering_cpuset(self._topology, set1._bitmap))
        except C.NULLError:
            return None

    def get_shared_cache_covering_obj(self, obj):
        """Get the first cache shared between an object and somebody else"""
        try:
            return Obj(C.get_shared_cache_covering_obj(self._topology, obj._obj))
        except C.NULLError:
            return None

    # Finding objects, miscellaneous helpers
    def get_pu_obj_by_os_index(self, os_index):
        """Returns the object of type HWLOC_OBJ_PU with os_index"""
        return Obj(C.get_pu_obj_by_os_index(self._topology, os_index))

    def get_numanode_obj_by_os_index(self, os_index):
        """Returns the object of type HWLOC_OBJ_NODE with os_index"""
        return Obj(C.get_numanode_obj_by_os_index(self._topology, os_index))

    def get_closest_objs(self, src: Obj, max1: int) -> Tuple[Obj]:
        """Do a depth-first traversal of the topology to find and sort
           all objects that are at the same depth as src"""
        l = C.get_closest_objs(self._topology, src._obj, max1)
        return tuple(Obj(x) for x in l)

    def get_obj_below_by_type(self, type1, idx1, type2, idx2) -> Obj:
        """ Find an object below another object, both specified by types and indexes"""
        try:
            return Obj(C.get_obj_below_by_type(self._topology, type1, idx1, type2,
                                               idx2))
        except C.NULLError:
            return None

    def get_obj_below_array_by_type(self, *pairs) -> Obj:
        """Find an object below a chain of objects specified by a list of type and index pairs"""
        typev = [x[0] for x in pairs]
        idxv = [x[1] for x in pairs]
        try:
            return Obj(C.get_obj_below_array_by_type(self._topology, typev, idxv))
        except C.NULLError:
            return None
        except C.ArgError:
            raise ArgError('get_obj_below_array_by_type')

    # Distributing items over a topology
    def distrib(self, roots, n, until, flags=0):
        """Distribute n items over the topology under roots"""
        l = tuple([x._obj for x in roots])
        l = C.distrib(self._topology, l, n, until, flags)
        return tuple([Bitmap(x) for x in l])

    # deprecated
    def distribute(self, root, n, until):
        """Distribute n items over the topology under root"""
        l = C.distribute(self._topology, root._obj, n, until)
        return tuple([Bitmap(x) for x in l])

    # deprecated
    def distributev(self, roots, n, until):
        """Distribute n items over the topology under roots"""
        l = tuple([x._obj for x in roots])
        l = C.distributev(self._topology, l, n, until)
        return tuple([Bitmap(x) for x in l])

    # def alloc_membind_policy_nodeset(self, len1, nodeset, policy, flags):
    #     """Allocate some memory on the given nodeset
    #     Caller must free"""
    #     return C.alloc_membind_policy_nodeset(
    #         self._topology, len1, nodeset, policy, flags
    #     )

    def alloc_membind_policy(self, len1, set_, policy, flags):
        """Allocate some memory on the memory nodes near given cpuset or nodeset
        Caller must free"""
        return C.alloc_membind_policy(self._topology, len1, set_._bitmap, policy,
                                      flags)

    # Cpuset Helpers
    @property
    def complete_cpuset(self) -> Bitmap:
        try:
            return Bitmap(C.topology_get_complete_cpuset(self._topology))
        except C.NULLError:
            return None

    @property
    def cpuset(self) -> Bitmap:
        try:
            return Bitmap(C.topology_get_topology_cpuset(self._topology))
        except C.NULLError:
            return None

    @property
    def allowed_cpuset(self) -> Bitmap:
        try:
            return Bitmap(C.topology_get_allowed_cpuset(self._topology))
        except C.NULLError:
            return None

    # Nodeset Helpers
    @property
    def complete_nodeset(self) -> Bitmap:
        try:
            return Bitmap(C.topology_get_complete_nodeset(self._topology))
        except C.NULLError:
            return None

    @property
    def nodeset(self) -> Bitmap:
        try:
            return Bitmap(C.topology_get_topology_nodeset(self._topology))
        except C.NULLError:
            return None

    @property
    def allowed_nodeset(self) -> Bitmap:
        try:
            return Bitmap(C.topology_get_allowed_nodeset(self._topology))
        except C.NULLError:
            return None

    # Converting between CPU sets and node sets
    def cpuset_to_nodeset(self, cpuset) -> Bitmap:
        """Convert a CPU set into a NUMA node set and handle non-NUMA cases"""
        return Bitmap(C.cpuset_to_nodeset(self._topology, cpuset._bitmap))

    def cpuset_to_nodeset_strict(self, cpuset) -> Bitmap:
        """Convert a CPU set into a NUMA node set
        without handling non-NUMA cases"""
        return Bitmap(C.cpuset_to_nodeset_strict(self._topology, cpuset._bitmap))

    def cpuset_from_nodeset(self, nodeset) -> Bitmap:
        """Convert a NUMA node set into a CPU set and handle non-NUMA cases"""
        return Bitmap(C.cpuset_from_nodeset(self._topology, nodeset._bitmap))

    def cpuset_from_nodeset_strict(self, nodeset) -> Bitmap:
        """Convert a NUMA node set into a CPU set
        without handling non-NUMA cases"""
        return Bitmap(
            C.cpuset_from_nodeset_strict(self._topology, nodeset._bitmap)
        )

    # Finding I/O objects
    def get_non_io_ancestor_obj(self, obj):
        """Get the first non-I/O ancestor object"""
        try:
            return Obj(C.get_non_io_ancestor_obj(self._topology, obj._obj))
        except C.NULLError:
            return None

    def get_next_pcidev(self, prev) -> Optional[Obj]:
        """ Get the next PCI device in the system"""
        if prev is not None:
            prev = prev._obj
        try:
            return Obj(C.get_next_pcidev(self._topology, prev))
        except C.NULLError:
            return None

    @property
    def pcidevs(self) -> Generator[Obj, None, None]:
        prev = self.get_next_pcidev(None)
        while prev:
            yield prev
            prev = self.get_next_pcidev(prev)

    def get_pcidev_by_busid(self, domain, bus, dev, func):
        """Find the PCI device object matching the PCI bus id
        given domain, bus device and function PCI bus id"""
        try:
            return Obj(C.get_pcidev_by_busid(self._topology, domain, bus, dev,
                                             func))
        except C.NULLError:
            return None

    def get_pcidev_by_busidstring(self, busid):
        """Find the PCI device object matching the PCI bus id
        given as a string xxxx:yy:zz.t or yy:zz.t"""
        try:
            return Obj(C.get_pcidev_by_busidstring(self._topology, busid))
        except C.NULLError:
            return None

    def get_next_osdev(self, prev) -> Optional[Obj]:
        """ Get the next OS device in the system"""
        if prev is not None:
            prev = prev._obj
        try:
            return Obj(C.get_next_osdev(self._topology, prev))
        except C.NULLError:
            return None

    @property
    def osdevs(self) -> Generator[Obj, None, None]:
        prev = self.get_next_osdev(None)
        while prev:
            yield prev
            prev = self.get_next_osdev(prev)

    def get_next_bridge(self, prev) -> Optional[Obj]:
        """Get the next bridge in the system"""
        if prev is not None:
            prev = prev._obj
        try:
            return Obj(C.get_next_bridge(self._topology, prev))
        except C.NULLError:
            return None

    @property
    def bridges(self) -> Generator[Obj, None, None]:
        prev = self.get_next_bridge(None)
        while prev:
            yield prev
            prev = self.get_next_bridge(prev)

    def bridge_covers_pcibus(self, domain, bus):
        """Checks whether a given bridge covers a given PCI bus"""
        return bool(C.bridge_covers_pcibus(self._topology, domain, bus))

    def get_hostbridge_by_pcibus(self, domain, bus):
        """Find the hostbridge that covers the given PCI bus"""
        try:
            return Obj(C.get_hostbridge_by_pcibus(self._topology, domain, bus))
        except C.NULLError:
            return None

    # Topology differences
    def diff_build(self, newtopology, flags=0):
        """returns TopologyDiff, (boolean) is too complex"""
        diff, too = C.topology_diff_build(self._topology, newtopology._topology,
                                          flags)
        if diff is None:
            return diff, False
        return TopologyDiff(diff), bool(too)

    def diff_apply(self, diff, flags=0):
        return C.topology_diff_apply(self._topology, diff._diff, flags)

    def diff_load_xml(self, xmlpath):
        """returns TopologyDiff, refname"""
        t, r = C.topology_diff_load_xml(self._topology, xmlpath)
        return TopologyDiff(t), r

    def diff_export_xml(self, diff, refname, xmlpath):
        C.topology_diff_export_xml(diff._diff, refname, xmlpath)

    def diff_load_xmlbuffer(self, xmlbuffer):
        """returns TopologyDiff, refname"""
        t, r = C.topology_diff_load_xmlbuffer(xmlbuffer)
        return TopologyDiff(t), r

    def diff_export_xmlbuffer(self, diff, refname=None):
        return C.topology_diff_export_xmlbuffer(diff._diff, refname)

    # Linux-only helpers
    def linux_set_tid_cpubind(self, tid, set1):
        C.linux_set_tid_cpubind(self._topology, tid, set1._bitmap)

    def linux_get_tid_cpubind(self, tid):
        return Bitmap(C.linux_get_tid_cpubind(self._topology, tid))

    def linux_get_tid_last_cpu_location(self, tid):
        return Bitmap(C.linux_get_tid_last_cpu_location(self._topology,
                                                        tid))

    @classmethod
    def linux_read_path_as_cpumask(cls, path: str) -> Bitmap:
        return Bitmap(C.linux_read_path_as_cpumask(path))

    # OpenGL display specific functions
    def gl_get_display_osdev_by_port_device(self, port, device):
        """Get the hwloc OS device object corresponding to the OpenGL display given by port and device index"""
        return Obj(C.gl_get_display_osdev_by_port_device(self._topology, port,
                                                         device))

    def gl_get_display_osdev_by_name(self, name):
        """Get the hwloc OS device object corresponding to the OpenGL display given by name"""
        return Obj(C.gl_get_display_osdev_by_name(self._topology, name))

    def gl_get_display_by_osdev(self, osdev):
        """Get the OpenGL display port and device corresponding
        to the given hwloc OS object.
        Returns (port, device)"""
        return C.gl_get_display_by_osdev(self._topology, osdev._obj)

#	# CUDA Driver API Specific Functions
#	def cuda_get_device_pci_ids(self, cudevice):
#		"""Return the domain, bus and device IDs of the CUDA device"""
#		return C.cuda_get_device_pci_ids(self._topology, cudevice)
#
#	def cuda_get_device_cpuset(self, cudevice):
#		return Bitmap(C.cuda_get_device_cpuset(self._topology, cudevice))
#
#	def cuda_get_device_pcidev(self, cudevice):
#		try:
#			o = C.cuda_get_device_pcidev(self._topology, cudevice)
#		except C.NULLError:
#			return None
#		return Obj(o)
#
#	def cuda_get_device_osdev(self, cudevice):
#		try:
#			o = C.cuda_get_device_osdev(self._topology, cudevice)
#		except C.NULLError:
#			return None
#		return Obj(o)

    # Helpers for manipulating Linux libnuma unsigned long masks
    def cpuset_to_linux_libnuma_ulongs(self, cpuset):
        """Convert hwloc CPU set into a tuple of unsigned long
        Returns: tuple of ints, maxnode"""
        return C.cpuset_to_linux_libnuma_ulongs(self._topology, cpuset._bitmap)

    def nodeset_to_linux_libnuma_ulongs(self, nodeset):
        """Convert hwloc NUMA node set into the array of unsigned long
        Returns: tuple of ints, maxnode"""
        return C.nodeset_to_linux_libnuma_ulongs(self._topology, nodeset._bitmap)

    def cpuset_from_linux_libnuma_ulongs(self, mask, maxnode=None):
        """Convert a list of unsigned long into hwloc CPU set"""
        return Bitmap(C.cpuset_from_linux_libnuma_ulongs(self._topology, mask,
                                                         maxnode))

    def nodeset_from_linux_libnuma_ulongs(self, mask, maxnode=None):
        """Convert a list of unsigned long into hwloc NUMA node set"""
        return Bitmap(C.nodeset_from_linux_libnuma_ulongs(self._topology, mask,
                                                          maxnode))

    def cpuset_to_linux_libnuma_bitmask(self, cpuset):
        """Convert hwloc CPU set into a python-libnuma Bitmask"""
        return C.cpuset_to_linux_libnuma_bitmask(self._topology, cpuset._bitmap)

    def nodeset_to_linux_libnuma_bitmask(self, nodeset):
        """Convert hwloc NUMA node set into a python-libnuma Bitmask"""
        return C.nodeset_to_linux_libnuma_bitmask(self._topology, nodeset._bitmap)

    def cpuset_from_linux_libnuma_bitmask(self, bitmask):
        """Convert python-libnuma Bitmask into hwloc CPU set"""
        return Bitmap(C.cpuset_from_linux_libnuma_bitmask(self._topology, bitmask))

    def nodeset_from_linux_libnuma_bitmask(self, bitmask):
        """Convert python-libnuma Bitmask into hwloc NUMA node set"""
        return Bitmap(C.nodeset_from_linux_libnuma_bitmask(self._topology, bitmask))


# NVIDIA Management Library Specific Functions
#	def nvml_get_device_osdev_by_index(self, idx):
#		try:
#			return Obj(C.nvml_get_device_osdev_by_index(self._topology, idx))
#		except C.NULLError:
#			return None
#
#	@property
#	def nvml_device_osdevs(self):
#		i = 0
#		while True:
#			osdev = self.nvml_get_device_osdev_by_index(i)
#			if osdev is None:
#				break
#			yield osdev
#			i += 1
#		return


def compare_types(type1, type2):
    """Compare the depth of two object types"""
    return C.compare_types(_typeFromString(type1), _typeFromString(type2))
