# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

"""
Copyright 2019 Guy Streeter

python bindings for hwloc
"""
__author__ = "Guy Streeter <guy.streeter@gmail.com>"
__license__ = "GPLv2+"
__version__ = "2.0-0.1"

from .hwloc import *
