#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This tests the python hwloc_bitmap implementation
#

import hwloc

# allocation is a class method
b1 = hwloc.Bitmap.alloc()

# freeing is handled by the object
del b1

# you can populate at the same time
b1 = hwloc.Bitmap.alloc('0xf')
b2 = hwloc.Bitmap.alloc(list(range(4)))
b3 = hwloc.Bitmap(b2)
b4 = hwloc.Bitmap()
b4.list_sscanf('0-3')
assert b4 == b3
assert b3 == b2
assert b2 == b1

# equality means the same set bits, not the same allocated space
b2.clr(0)
assert b1 != b2
# assigning to a new variable does not make a copy (deep copy methods would).
b3 = b1
b3.clr(0)
assert b1 == b2

# assigning a bitmap variable to some other object reduces the original
# object's use count. when the use count is zero the object gets deleted
b3 = b1
b1 = hwloc.Bitmap.alloc()
assert b3 == b2
del b3
assert b1 != b2
b2 = hwloc.Bitmap.alloc_full()
assert b1 != b2

# to make a new copy of a bitmap, use dup() instead of assignment
b1 = b2.dup()
b1.clr(1)
assert b2.isset(1)
b2.clr(1)
assert b1 == b2

# any place a string is expected, the stringified bitmap is used
s1 = '%s' % b1
s2 = str(b2)
assert s1 == s2

# explicit use of stringified forms
s1 = b1.asprintf()
b2.zero()
b2.sscanf(s1)
assert b1 == b2

# taskset-format strings:
b1.only(0)
s1 = b1.taskset_asprintf()
b2.taskset_sscanf(s1)
assert b1 == b2

# where a boolean is expected, whether the bitmap is zero is used
b2.zero()
assert not b2

# first and last are properties, next() is a method
assert b2.first == -1
assert b1.last == 0
b2.only(1)
assert b2.first == b2.last
assert b2.next(b2.first) == -1
b1.allbut(1)
assert b1.next(0) == b1.next(1)
# weight is not a property just so nobody thinks it's a cheap operation
assert b1.weight() == -1
assert b2.weight() == 1

# all_set_bits returns a tuple
b1 = hwloc.Bitmap.alloc('0xa0a')
for i in b1.all_set_bits:
    assert b1.isset(i)
    assert i % 2 == 1

# 'for X in...' allows iteration over set bits
j = 0
for i in b1:
    assert b1.isset(i)
    j += 1
assert j == b1.weight()

# set() can take a single index or a sequence of them
b2.zero()
# list, tuple, or anything iterable
b2.set(b1.all_set_bits)
assert b1 == b2

# ulong() can take an index, which defaults to zero.
assert b2.ulong() == 0xa0a
assert b1.ulong(1) == 0

# set_long() can take an index (defaults to zero). Note the order of arguments
# does not match the C library function
b2.zero()
assert b2.iszero
b2.set_ulong(0xa0a, 0)
assert b1 == b2

# from_ulong can also take an index (defaults to zero)
b2.fill()
assert b2.isfull
b2.from_ulong(0xa0a)
assert b1 == b2
b1.from_ulong(0xa0a, 1)
# you can create and throw away a bitmap very easily
assert b1 == hwloc.Bitmap.alloc('0xa0a,,0')

# logical 'in' tests if all the bits set in this are set in the other
b1.only(0)
assert b1.isset(0)
assert not b1.isset(1)
b2.zero()
b2.set((0, 1))
assert b1 in b2
assert b1.isincluded(b2)


# arithmetic and/or/xor/not create new bitmaps
b3 = b1 | b2
assert b3 == b2
b3 = b1 & b2
assert b3 == b1
b1 |= b2
assert b1 == b2
b1.only(0)
b2.allbut(0)
b3 = b1 ^ b2
assert b3.isfull
b1.zero()
b2.zero()
b1.set((0, 1))
b2.set((1, 2, 33))
b3 = b2.dup()
b3.set(0)
b3.clr(1)
assert b1 ^ b2 == b3

# The inversion operator does negation.
# It is not available as an assignment operator
b1 = ~b2
assert b1.isset(3)
assert not b1.isset(1)

assert b3.intersects(b1)
assert b3.intersects(hwloc.Bitmap.alloc('0xf...f,0xfffffffd,0xfffffff9'))
assert not b1.intersects(b2)

b1.only(1)
b2.zero()
b2.set((1, 2))
assert b1.compare_first(b2) == 0
assert b1.compare_first(hwloc.Bitmap.alloc(0x3))
assert b1.compare(b2) < 0
assert b1.compare(hwloc.Bitmap.alloc('3')) < 0
