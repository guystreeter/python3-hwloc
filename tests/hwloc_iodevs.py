#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_iodevs.c
# from the hwloc package.
#

import hwloc

topo = hwloc.Topology()
topo.set_io_types_filter(hwloc.TYPE_FILTER_KEEP_ALL)
topo.load()

print('Found', topo.get_nbobjs_by_type(hwloc.OBJ_BRIDGE), 'bridges')
for obj in topo.bridges:
    assert obj.type == hwloc.OBJ_BRIDGE
    # only host->pci and pci->pci bridge supported so far
    if obj.attr.bridge.upstream_type == hwloc.OBJ_BRIDGE_HOST:
        assert obj.attr.bridge.downstream_type == hwloc.OBJ_BRIDGE_PCI
        print(' Found host->PCI bridge for domain %04x bus %02x-%02x' % (
            obj.attr.bridge.downstream.pci.domain,
            obj.attr.bridge.downstream.pci.secondary_bus,
            obj.attr.bridge.downstream.pci.subordinate_bus))
    else:
        assert obj.attr.bridge.upstream_type == hwloc.OBJ_BRIDGE_PCI
        assert obj.attr.bridge.downstream_type == hwloc.OBJ_BRIDGE_PCI
        print(' Found PCI->PCI bridge [%04x:%04x] for domain %04x bus %02x-%02x' % (
            obj.attr.bridge.upstream.pci.vendor_id,
            obj.attr.bridge.upstream.pci.device_id,
            obj.attr.bridge.downstream.pci.domain,
            obj.attr.bridge.downstream.pci.secondary_bus,
            obj.attr.bridge.downstream.pci.subordinate_bus))

print('Found', topo.get_nbobjs_by_type(hwloc.OBJ_PCI_DEVICE), 'PCI devices')
for obj in topo.pcidevs:
    assert obj.type == hwloc.OBJ_PCI_DEVICE
    print(' Found PCI device class %04x vendor %04x model %04x' % (
        obj.attr.pcidev.class_id,
        obj.attr.pcidev.vendor_id,
        obj.attr.pcidev.device_id))

print('Found', topo.get_nbobjs_by_type(hwloc.OBJ_OS_DEVICE), 'OS devices')
for obj in topo.osdevs:
    assert obj.type == hwloc.OBJ_OS_DEVICE
    print('Found OS device %s subtype %d' % (
        obj.name,
        obj.attr.osdev.type))

assert hwloc.TYPE_DEPTH_BRIDGE == topo.get_type_depth(hwloc.OBJ_BRIDGE)
assert hwloc.TYPE_DEPTH_PCI_DEVICE == topo.get_type_depth(hwloc.OBJ_PCI_DEVICE)
assert hwloc.TYPE_DEPTH_OS_DEVICE == topo.get_type_depth(hwloc.OBJ_OS_DEVICE)
assert hwloc.compare_types(hwloc.OBJ_BRIDGE, hwloc.OBJ_PCI_DEVICE) < 0
assert hwloc.compare_types(hwloc.OBJ_BRIDGE, hwloc.OBJ_OS_DEVICE) < 0
assert hwloc.compare_types(hwloc.OBJ_PCI_DEVICE, hwloc.OBJ_OS_DEVICE) < 0
