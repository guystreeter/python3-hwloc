#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/xmlbuffer.c from the
# hwloc package.
#

import hwloc
import os
import sys


def one_test():
    s = [ord(' ') for _i in range(32)] + list(range(32, 127))
    s[ord('\t')] = ord('\t')
    s[ord('\n')] = ord('\n')
    s[ord('\r')] = ord('\r')
    s = ''.join([chr(i) for i in s]) + ' '

    t = 'x' + ''.join([chr(i) for i in range(1, 8)]) + 'y'

    topology = hwloc.Topology()
    topology.set_all_types_filter(hwloc.TYPE_FILTER_KEEP_ALL)
    topology.load()
    assert topology.is_thissystem
    topology.root_obj.add_info('UglyString', s)
    topology.root_obj.add_info('UberUglyString', t)
    buf1 = topology.export_xmlbuffer()
    del topology
    print('exported to buffer of length', len(buf1))

    topology = hwloc.Topology()
    topology.set_all_types_filter(hwloc.TYPE_FILTER_KEEP_ALL)
    topology.set_xmlbuffer(buf1)
    topology.load()
    assert not topology.is_thissystem
    assert topology.root_obj.get_info_by_name('UglyString') == s
    assert topology.root_obj.get_info_by_name('UberUglyString') == 'xy'
    buf2 = topology.export_xmlbuffer()
    print('re-exported to buffer of length', len(buf2))

    if buf1 != buf2:
        print('### First exported buffer is:')
        print(buf1)
        print('### End of first export buffer')
        print('### Second exported buffer is:')
        print(buf2)
        print('### End of second export buffer')
        return 1

    return 0

print('using default import and export')
os.environ['HWLOC_NO_LIBXML_IMPORT'] = '0'
os.environ['HWLOC_NO_LIBXML_EXPORT'] = '0'
err = one_test()
if err < 0:
    sys.exit(err)

print('using minimalist import and default export')
os.environ['HWLOC_NO_LIBXML_IMPORT'] = '1'
os.environ['HWLOC_NO_LIBXML_EXPORT'] = '0'
err = one_test()
if err < 0:
    sys.exit(err)

print('using default import and minimalist export')
os.environ['HWLOC_NO_LIBXML_IMPORT'] = '0'
os.environ['HWLOC_NO_LIBXML_EXPORT'] = '1'
err = one_test()
if err < 0:
    os.exit(err)

print('using minimalist import and export')
os.environ['HWLOC_NO_LIBXML_IMPORT'] = '1'
os.environ['HWLOC_NO_LIBXML_EXPORT'] = '1'
err = one_test()
if err < 0:
    os.exit(err)
