#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_distances.c
# from the hwloc package.
#

LEN = 1048576

import hwloc
import sys
import ctypes

topo = hwloc.Topology()
topo.load()

support = topo.support
if not support.membind.get_area_memlocation:
    print('get_area_memlocation not supported')
    sys.exit()

buffer_ = topo.alloc(LEN)
assert buffer_
print('buffer 0x{:X} length {:d}'.format(buffer_, LEN))

set_ = topo.get_area_memlocation(buffer_, LEN, hwloc.MEMBIND_BYNODESET)
print('address 0x{:X} length {:d} allocated in nodeset'.format(
    buffer_, LEN), str(set_))

total = set_.dup()

node = None
for n in topo.objs_by_type(hwloc.OBJ_NUMANODE):
    if n.attr.numanode.local_memory:
        node = n
        break
if node is None:
    topo.free(buffer_, LEN)
    sys.exit()

print('binding to 1st node and touching 1st quarter')
topo.set_area_membind(buffer_, LEN, node.nodeset,
                      hwloc.MEMBIND_BIND, hwloc.MEMBIND_BYNODESET)

# instead of memset...
buf = ctypes.cast(buffer_, ctypes.POINTER(ctypes.c_ubyte))
buf[0] = 0
buf[LEN // 4 - 1] = 0

set_ = topo.get_area_memlocation(buffer_, 1, hwloc.MEMBIND_BYNODESET)
print('address 0x{:X} length {:d} allocated in nodeset'.format(
    buffer_, LEN // 4), str(set_))
total |= set_

n = node
node = None
for n in topo.objs_by_type(hwloc.OBJ_NUMANODE, n):
    if n.attr.numanode.local_memory:
        node = n
        break
if node is None:
    topo.free(buffer_, LEN)
    sys.exit()

print('binding 2nd node and touching 2nd quarter')
topo.set_area_membind(ctypes.byref(
    buf[LEN // 4]), LEN // 4, node.nodeset, hwloc.MEMBIND_BIND, hwloc.MEMBIND_BYNODESET)
buf[LEN // 4] = 0
buf[LEN // 2 - 1] = 0

set_ = topo.get_area_memlocation(ctypes.byref(
    buf[LEN // 4]), 1, hwloc.MEMBIND_BYNODESET)
print('address 0x{:X} length {:d} allocated in nodeset'.format(
    ctypes.byref(buf[LEN // 4]), LEN // 4), str(set_))
total |= set_

n = node
node = None
for n in topo.objs_by_type(hwloc.OBJ_NUMANODE, n):
    if n.atrr.numanode.local_memory:
        node = n
        break
if node is None:
    topo.free(buffer_, LEN)
    sys.exit()

print('binding 3rd node and touching 3rd quarter')
topo.set_area_membind(ctypes.byref(
    buf[LEN // 2]), LEN // 4, node.nodeset, hwloc.MEMBIND_BIND, hwloc.MEMBIND_BYNODESET)
buf[LEN // 2] = 0
buf[LEN // 2 + LEN // 4 - 1] = 0

set_ = topo.get_area_memlocation(ctypes.byref(
    buf[LEN // 2]), 1, hwloc.MEMBIND_BYNODESET)
print('address 0x{:X} length {:d} allocated in nodeset'.format(
    ctypes.byref(buf[LEN // 2]), LEN // 4), str(set_))
total |= set_

n = node
node = None
for n in topo.objs_by_type(hwloc.OBJ_NUMANODE, n):
    if n.attr.numanode.local_memory:
        node = n
        break
if node is None:
    topo.free(buffer_, LEN)
    sys.exit()

print('binding 4th node and touching 4th quarter')
topo.set_area_membind(ctypes.byref(buf[LEN // 2 + LEN // 4]), LEN //
                      4, node.nodeset, hwloc.MEMBIND_BIND, hwloc.MEMBIND_BYNODESET)
buf[LEN // 2 + LEN // 4] = 0
buf[LEN - 1] = 0

set_ = topo.get_area_memlocation(ctypes.byref(
    buf[LEN // 2 + LEN // 4]), 1, hwloc.MEMBIND_BYNODESET)
print('address 0x{:X} length {:d} allocated in nodeset'.format(
    ctypes.byref(buf[LEN // 2 + LEN // 4]), LEN // 4), str(set_))
total |= set_

set_ = topo.get_area_memlocation(buffer_, LEN, hwloc.MEMBIND_BYNODESET)
print('address 0x{:X} length {:d} allocated in nodeset'.format(
    buffer_, LEN), str(set_))
assert total in set_

del topo
