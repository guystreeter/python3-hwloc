#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_get_obj_inside_cpuset.c
# from the hwloc package.
#

import hwloc

topo = hwloc.Topology()

topo.set_synthetic('node:2 pack:3 l2:4 core:5 6')

topo.load()

# there is no second system object
root = topo.root_obj
obj = topo.get_obj_inside_cpuset_by_type(root.cpuset, hwloc.OBJ_MACHINE, 1)
assert not obj

# first system object is the top-level object of the topology
obj = topo.get_obj_inside_cpuset_by_type(root.cpuset, hwloc.OBJ_MACHINE, 0)
assert obj == topo.root_obj

# first next-object object is the top-level object of the topology
obj = topo.get_next_obj_inside_cpuset_by_type(
    root.cpuset, hwloc.OBJ_MACHINE, None)
assert obj == topo.root_obj
# there is no next object after the system object
obj = topo.get_next_obj_inside_cpuset_by_type(
    root.cpuset, hwloc.OBJ_MACHINE, obj)
assert not obj

# check last PU
obj = topo.get_obj_inside_cpuset_by_type(
    root.cpuset,
    hwloc.OBJ_PU,
    2 * 3 * 4 * 5 * 6 - 1)
assert obj == topo.get_obj_by_depth(5, 2 * 3 * 4 * 5 * 6 - 1)
# there is no next PU after the last one
obj = topo.get_next_obj_inside_cpuset_by_type(root.cpuset, hwloc.OBJ_PU, obj)
assert not obj

# check there are 20 cores inside first package
root = topo.get_obj_by_depth(2, 0)
assert topo.get_nbobjs_inside_cpuset_by_type(root.cpuset, hwloc.OBJ_CORE) == 20

# check there are 12 caches inside the last node
root = topo.get_obj_by_depth(hwloc.TYPE_DEPTH_NUMANODE, 1)
assert topo.get_nbobjs_inside_cpuset_by_type(
    root.cpuset,
    hwloc.OBJ_L2CACHE) == 12

# check first PU of second package
root = topo.get_obj_by_depth(2, 1)
obj = topo.get_obj_inside_cpuset_by_type(root.cpuset, hwloc.OBJ_PU, 0)
assert obj == topo.get_obj_by_depth(5, 4 * 5 * 6)
index = topo.get_obj_index_inside_cpuset(root.cpuset, obj)
assert index == 0

# check third core of third package
root = topo.get_obj_by_depth(2, 2)
obj = topo.get_obj_inside_cpuset_by_type(root.cpuset, hwloc.OBJ_CORE, 2)
assert obj == topo.get_obj_by_depth(4, 2 * 4 * 5 + 2)
index = topo.get_obj_index_inside_cpuset(root.cpuset, obj)
assert index == 2

# check first package of second node
root = topo.get_obj_by_depth(hwloc.TYPE_DEPTH_NUMANODE, 1)
obj = topo.get_obj_inside_cpuset_by_type(root.cpuset, hwloc.OBJ_PACKAGE, 0)
assert obj == topo.get_obj_by_depth(2, 3)
index = topo.get_obj_index_inside_cpuset(root.cpuset, obj)
assert index == 0

# there is no node inside packages
root = topo.get_obj_by_depth(2, 0)
obj = topo.get_obj_inside_cpuset_by_type(root.cpuset, hwloc.OBJ_NUMANODE, 0)
assert not obj
