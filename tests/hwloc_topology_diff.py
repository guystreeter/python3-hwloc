#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_topology_diff.c
# from the hwloc package.
#

import hwloc
import os


os.environ['HWLOC_LIBXML_CLEANUP'] = '1'

topo1 = hwloc.Topology()
topo1.load()
print('duplicate topo1 into topo2')
topo2 = topo1.dup()

print('check that topo2 is identical')
diff, ret = topo1.diff_build(topo2)
assert not ret
assert diff is None

print('add a new info to topo2')
obj = topo1.root_obj
obj.add_info('Foo', 'Bar')
print('check that topo2 cannot be diff\'ed from topo1')
diff, ret = topo1.diff_build(topo2)
assert ret
assert diff is not None
assert diff.generic.type == hwloc.TOPOLOGY_DIFF_TOO_COMPLEX
assert diff.generic.next is None
assert diff.too_complex.obj_depth == 0
assert diff.too_complex.obj_index == 0
del diff  # must be destroyed *before* the topo it was built with

print('add a similar info to topo1, and change memory sizes')
obj = topo2.root_obj
obj.add_info('Foo', 'Bar2')

obj = topo2.get_obj_by_type(hwloc.OBJ_NUMANODE, 0)
obj.attr.numanode.local_memory += 32 * 4096

print('check that topo2 is now properly diff\'ed')
diff, ret = topo1.diff_build(topo2)
assert not ret
tmpdiff = diff
del diff  # tmpdiff should not go away
diff = tmpdiff
assert tmpdiff.generic.type == hwloc.TOPOLOGY_DIFF_OBJ_ATTR
assert tmpdiff.obj_attr.obj_depth == 0
assert tmpdiff.obj_attr.obj_index == 0
assert tmpdiff.obj_attr.diff.generic.type == hwloc.TOPOLOGY_DIFF_OBJ_ATTR_INFO
assert tmpdiff.obj_attr.diff.string.name == 'Foo'
assert tmpdiff.obj_attr.diff.string.oldvalue == 'Bar'
assert tmpdiff.obj_attr.diff.string.newvalue == 'Bar2'
tmpdiff = tmpdiff.generic.next
assert tmpdiff.generic.type == hwloc.TOPOLOGY_DIFF_OBJ_ATTR
assert tmpdiff.obj_attr.obj_depth == topo1.get_type_depth(hwloc.OBJ_NUMANODE)
assert tmpdiff.obj_attr.obj_index == 0
assert tmpdiff.obj_attr.diff.generic.type == hwloc.TOPOLOGY_DIFF_OBJ_ATTR_SIZE
assert tmpdiff.obj_attr.diff.uint64.newvalue - \
    tmpdiff.obj_attr.diff.uint64.oldvalue == 32 * 4096
assert tmpdiff.generic.next is None

del tmpdiff  # diff should not go away

print('apply the diff to new duplicate topo3 of topo1')
topo3 = topo1.dup()
assert topo3.diff_apply(diff) == 0
print('check that topo2 and topo3 are identical')
diff2, ret = topo2.diff_build(topo3)
assert not ret
assert diff2 is None

print('apply the reverse diff to topo2')
assert topo2.diff_apply(diff, hwloc.TOPOLOGY_DIFF_APPLY_REVERSE) == 0
print('check that topo2 and topo1 are identical')
diff2, ret = topo1.diff_build(topo2)
assert not ret
assert diff2 is None

print('exporting and reloading diff from XML buffer without refname')
xmlbuffer = topo1.diff_export_xmlbuffer(diff)
del diff
diff2, refname = topo1.diff_load_xmlbuffer(xmlbuffer)
assert diff2 is not None
assert refname is None
del xmlbuffer

print('exporting and reloading diff from XML buffer with refname')
xmlbuffer = topo1.diff_export_xmlbuffer(diff2, 'foobar')
del diff2
diff, refname = topo1.diff_load_xmlbuffer(xmlbuffer)
assert diff is not None
assert refname == 'foobar'
del refname
del xmlbuffer

tmpdiff = diff
assert tmpdiff.generic.type == hwloc.TOPOLOGY_DIFF_OBJ_ATTR
assert tmpdiff.obj_attr.obj_depth == 0
assert tmpdiff.obj_attr.obj_index == 0
assert tmpdiff.obj_attr.diff.generic.type == hwloc.TOPOLOGY_DIFF_OBJ_ATTR_INFO
assert tmpdiff.obj_attr.diff.string.name == 'Foo'
assert tmpdiff.obj_attr.diff.string.oldvalue == 'Bar'
assert tmpdiff.obj_attr.diff.string.newvalue == 'Bar2'
tmpdiff = tmpdiff.generic.next
assert tmpdiff.generic.type == hwloc.TOPOLOGY_DIFF_OBJ_ATTR
assert tmpdiff.obj_attr.obj_depth == topo1.get_type_depth(hwloc.OBJ_NUMANODE)
assert tmpdiff.obj_attr.obj_index == 0
assert tmpdiff.obj_attr.diff.generic.type == hwloc.TOPOLOGY_DIFF_OBJ_ATTR_SIZE
assert tmpdiff.obj_attr.diff.uint64.newvalue - \
    tmpdiff.obj_attr.diff.uint64.oldvalue == 32 * 4096
assert tmpdiff.generic.next is None

print('reapplying to topo2')
assert topo2.diff_apply(diff) == 0
print('check that topo2 and topo3 are again identical')
diff2, ret = topo2.diff_build(topo3)
assert not ret
assert diff2 is None

del diff, diff2

print('adding new key to the bottom of topo3')
obj = topo3.get_obj_by_type(hwloc.OBJ_PU, 0)
assert obj
obj.add_info('Bar', 'Baz3')
print('check that diff fails at the last entry')
diff, ret = topo1.diff_build(topo3)
assert ret
assert diff
tmpdiff = diff
assert tmpdiff.generic.type == hwloc.TOPOLOGY_DIFF_OBJ_ATTR
tmpdiff = tmpdiff.generic.next
assert tmpdiff.generic.type == hwloc.TOPOLOGY_DIFF_TOO_COMPLEX
tmpdiff = tmpdiff.generic.next
assert tmpdiff.generic.type == hwloc.TOPOLOGY_DIFF_OBJ_ATTR
assert tmpdiff.generic.next is None
del diff

print('adding similar key to topo1')
obj = topo1.get_obj_by_type(hwloc.OBJ_PU, 0)
assert obj
obj.add_info('Bar', 'Baz1')
print('checking that topo3 diff fails to reverse apply to topo2')
diff, ret = topo1.diff_build(topo3)
assert not ret
assert diff
err = topo2.diff_apply(diff, hwloc.TOPOLOGY_DIFF_APPLY_REVERSE)
assert err == -2
del diff

del topo3
del topo2
del topo1
