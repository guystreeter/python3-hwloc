#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019-2020 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_topology_restrict.c
# from the hwloc package.
#

import hwloc
import errno


def print_distances(distances: hwloc.Distances):
    nbobjs = distances.nbobjs

    print('     ', end='')
    print(' % 5d' * nbobjs % tuple(
        distances.objs[j].os_index for j in range(nbobjs))
    )

    for i in range(nbobjs):
        print('% 5d' % distances.objs[i].os_index, end='')
        print(' % 5d' * nbobjs % tuple(
            distances.values[i * nbobjs + j] for j in range(nbobjs))
        )


def check(has_groups: int, nbnodes: int, nbcores: int, nbpus: int):
    global topology
    # sanity checks
    depth = topology.depth
    assert depth == 3 + has_groups
    depth = topology.get_type_depth(hwloc.OBJ_NUMANODE)
    assert depth == hwloc.TYPE_DEPTH_NUMANODE
    depth = topology.get_type_depth(hwloc.OBJ_GROUP)
    assert depth == (1 if has_groups else hwloc.TYPE_DEPTH_UNKNOWN)
    depth = topology.get_type_depth(hwloc.OBJ_CORE)
    assert depth == 1 + has_groups
    depth = topology.get_type_depth(hwloc.OBJ_PU)
    assert depth == 2 + has_groups

    # actual checks
    nb = topology.get_nbobjs_by_type(hwloc.OBJ_NUMANODE)
    assert nb == nbnodes
    nb = topology.get_nbobjs_by_type(hwloc.OBJ_GROUP)
    assert nb == (nbnodes if has_groups else 0)
    nb = topology.get_nbobjs_by_type(hwloc.OBJ_CORE)
    assert nb == nbcores
    nb = topology.get_nbobjs_by_type(hwloc.OBJ_PU)
    assert nb == nbpus
    total_memory = topology.root_obj.total_memory
    assert total_memory == (
        nbnodes * 1024 * 1024 * 1024
      )  # synthetic topology puts 1GB per node


def check_distances(nbnodes: int, nbcores: int):
    global topology
    # node distance
    distance = topology.distances_get_by_type(hwloc.OBJ_NUMANODE, 0, 0)
    if nbnodes >= 2:
        assert len(distance) == 1
        assert distance[0]
        assert distance[0].nbobjs == nbnodes
        print_distances(distance[0])
    else:
        assert len(distance) == 0
    del distance

    # core distance
    distance = topology.distances_get_by_type(hwloc.OBJ_CORE, 0, 0)
    if nbnodes >= 2:
        assert len(distance) == 1
        assert distance[0].nbobjs == nbcores
        print_distances(distance[0])
    else:
        assert len(distance) == 0
    del distance


topology = hwloc.Topology()
topology.set_synthetic('node:3 core:2 pu:4')
topology.load()

nodes = [topology.get_obj_by_type(hwloc.OBJ_NUMANODE, i) for i in range(3)]
node_distances = [None] * 3 * 3
for i in range(3):
    for j in range(3):
        node_distances[i * 3 + j] = 10 if i == j else 20
kind = (
    hwloc.DISTANCES_KIND_MEANS_LATENCY | hwloc.DISTANCES_KIND_FROM_USER
)
flag = hwloc.DISTANCES_ADD_FLAG_GROUP
topology.distances_add(nodes, node_distances, kind, flag)

cores = [topology.get_obj_by_type(hwloc.OBJ_CORE, i) for i in range(6)]
core_distances = [None] * 6 * 6
for i in range(6):
    for j in range(6):
        core_distances[i * 6 + j] = 4 if i == j else 8
topology.distances_add(cores, core_distances, kind, flag)

# entire topology
print('starting from full topology')
check(1, 3, 6, 24)
check_distances(3, 6)

# restrict to nothing, impossible
print('restricting to nothing, must fail')
cpuset = hwloc.Bitmap.alloc()
try:
    topology.restrict(cpuset, 0)
    assert False
except OSError as err:
    assert err.errno == errno.EINVAL
print('restricting to unexisting PU:24, must fail')
cpuset.only(24)
try:
    topology.restrict(cpuset, 0)
    assert False
except OSError as err:
    assert err.errno == errno.EINVAL
check(1, 3, 6, 24)
check_distances(3, 6)

# restrict to everything, will do nothing
print('restricting to everything, does nothing')
# will throw an exception on error
cpuset.fill()
topology.restrict(cpuset, 0)
check(1, 3, 6, 24)
check_distances(3, 6)

# remove a single pu (second PU of second core of second node)
print('removing second PU of second core of second node')
cpuset.fill()
cpuset.clr(13)
# will throw an exception on error
topology.restrict(cpuset, 0)
check(1, 3, 6, 23)
check_distances(3, 6)

# remove the entire second core of first node
print('removing entire second core of first node')
cpuset.fill()
cpuset.clr_range(4, 7)
# will throw an exception on error
topology.restrict(cpuset, 0)
check(1, 3, 5, 19)
check_distances(3, 5)

# remove the entire third node
print('removing all PUs under third node, but keep that CPU-less node')
cpuset.fill()
cpuset.clr_range(16, 23)
# will throw an exception on error
topology.restrict(cpuset, 0)
check(1, 3, 3, 11)
check_distances(3, 3)

# only keep three PUs (first and last of first core, and last of last core
# of second node)
print(
    'restricting to 3 PUs in 2 cores in 2 nodes, and remove the '
    'CPU-less node, and auto-merge groups'
)
cpuset.zero()
# cpuset.set((0, 3, 15))
cpuset.set(0)
cpuset.set(3)
cpuset.set(15)
topology.restrict(cpuset, hwloc.RESTRICT_FLAG_REMOVE_CPULESS)
check(0, 2, 2, 3)
check_distances(2, 2)

# restrict to the third node, impossible
print('restricting to only some already removed node, must fail')
cpuset.zero()
cpuset.set_range(16, 23)
try:
    topology.restrict(cpuset, 0)
    assert False
except OSError as err:
    assert err.errno == errno.EINVAL
check(0, 2, 2, 3)
check_distances(2, 2)

del topology

# check that restricting exactly on a Group object keeps things coherent
print('restricting to a Group covering only one of the PU level')
topology = hwloc.Topology()
topology.set_synthetic('pu:4')
topology.load()
cpuset.zero()
cpuset.set_range(1, 2)
obj = topology.alloc_group_object()
# This automatically duplicates the source cpuset
obj.cpuset = cpuset
obj.name = 'toto'
assert obj.name == 'toto'
topology.insert_group_object(obj)
topology.restrict(cpuset, 0)
topology.check()

del topology

# check that restricting PUs maintains ordering of normal children
print('restricting so that PUs get reordered')
topology = hwloc.Topology()
topology.set_synthetic('node:1 core:2 pu:2(indexes=0,2,1,3)')
topology.load()
cpuset.zero()
cpuset.set_range(1, 2)
topology.restrict(cpuset)
topology.check()
