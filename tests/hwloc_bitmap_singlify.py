#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_bitmap_singlify.c from
# the hwloc package.
#

import hwloc

orig = hwloc.Bitmap.alloc()
expected = hwloc.Bitmap.alloc()

# empty set gives empty set
orig.singlify()
assert orig.iszero

# full set gives first bit only
orig.fill()
orig.singlify()
expected.zero()
expected.set(0)
assert orig == expected
assert not orig.compare(expected)

# actual non-trivial set
orig.zero()
orig.set((45, 46, 517))
orig.singlify()
expected.zero()
expected.set(45)
assert orig == expected
assert not orig.compare(expected)
