#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_get_cache_covering_cpuset.c
# from the hwloc package.
#

import hwloc

SYNTHETIC_TOPOLOGY_DESCRIPTION = "numa:6 pack:5 l2:4 core:3 pu:2"  # 736bits wide topology

topo = hwloc.Topology()
topo.set_synthetic(SYNTHETIC_TOPOLOGY_DESCRIPTION)
topo.load()

# check the cache above a given cpu
CPUINDEX = 180
set1 = hwloc.Bitmap.alloc()
obj = topo.get_obj_by_depth(5, CPUINDEX)
assert obj
set1 |= obj.cpuset
cache = topo.get_cache_covering_cpuset(set1)
assert cache
assert hwloc.Obj.type_is_dcache(cache.type)
assert cache.logical_index == CPUINDEX // 2 // 3
assert obj.is_in_subtree(cache)

# check the cache above two nearby cpus
CPUINDEX1 = 180
CPUINDEX2 = 183
set1 = hwloc.Bitmap.alloc()
obj = topo.get_obj_by_type(hwloc.OBJ_PU, CPUINDEX1)
assert obj
set1 |= obj.cpuset
obj = topo.get_obj_by_type(hwloc.OBJ_PU, CPUINDEX2)
assert obj
set1 |= obj.cpuset
cache = topo.get_cache_covering_cpuset(set1)
assert cache
assert hwloc.Obj.type_is_dcache(cache.type)
assert cache.logical_index == CPUINDEX1 // 2 // 3
assert cache.logical_index == CPUINDEX2 // 2 // 3
assert obj.is_in_subtree(cache)

# check no cache above two distant cpus
CPUINDEX1 = 300
set1 = hwloc.Bitmap.alloc()
obj = topo.get_obj_by_type(hwloc.OBJ_PU, CPUINDEX1)
assert obj
set1 |= obj.cpuset
obj = topo.get_obj_by_type(hwloc.OBJ_PU, CPUINDEX2)
assert obj
set1 |= obj.cpuset
cache = topo.get_cache_covering_cpuset(set1)
assert not cache

# check no cache above higher level
set1 = hwloc.Bitmap.alloc()
obj = topo.get_obj_by_type(hwloc.OBJ_PACKAGE, 0)
assert obj
set1 |= obj.cpuset
cache = topo.get_cache_covering_cpuset(set1)
assert not cache
