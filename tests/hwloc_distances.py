#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_distances.c
# from the hwloc package.
#

from __future__ import print_function
import hwloc
import sys


def print_distances(distances: hwloc.Distances) -> None:
    nbobjs = distances.nbobjs

    # column header
    print('     ', end=' ')
    for j in range(nbobjs):
        print(' % 5d' % distances.objs[j].os_index, end=' ')
    print()

    # each line
    for i in range(nbobjs):
        # row header
        print('% 5d' % distances.objs[i].os_index, end=' ')
        # eacj value
        for j in range(nbobjs):
            print(' % 5d' % distances.values[i * nbobjs + j], end=' ')
        print()


def check_distances(topo: hwloc.Topology, depth: int, expected: int) -> None:
    distances = topo.distances_get_by_depth(depth, 0, 0)
    assert len(distances) == expected
    print('distance matrix for depth {depth:d}:'.format(depth=depth))
    for distance in distances:
        print_distances(distance)


topology = hwloc.Topology()
topology.set_synthetic('node:4 core:4 pu:1')
topology.load()

distances = topology.distances_get(0, 0)
if len(distances) == 0:
    print('No distance')

print('\nInserting NUMA distances')
objs = [topology.get_obj_by_type(hwloc.OBJ_NUMANODE, i) for i in range(4)]
values = [8]*16
values[0 + 4 * 1] = 4
values[1 + 4 * 0] = 4
values[2 + 4 * 3] = 4
values[3 + 4 * 2] = 4
for i in range(4):
    values[i + 4 * i] = 1
kind = (
    hwloc.DISTANCES_KIND_MEANS_LATENCY |
    hwloc.DISTANCES_KIND_FROM_USER
)
topology.distances_add(objs, values, kind, hwloc.DISTANCES_ADD_FLAG_GROUP)

topodepth = topology.depth
assert topodepth == 5
check_distances(topology, 0, 0)
check_distances(topology, 1, 0)
check_distances(topology, 2, 0)
check_distances(topology, 3, 0)
check_distances(topology, 4, 0)
check_distances(topology, hwloc.TYPE_DEPTH_NUMANODE, 1)

# check numa distances
print('Checking NUMA distances')
distances = topology.distances_get_by_type(hwloc.OBJ_NUMANODE, 0, 0)
assert len(distances) == 1
assert distances[0].kind == kind
# check helpers
assert distances[0].obj_index(
        topology.get_obj_by_type(hwloc.OBJ_NUMANODE, 2)
    ) == 2
value1, value2 = distances[0].obj_pair_values(
    topology.get_obj_by_type(hwloc.OBJ_NUMANODE, 1),
    topology.get_obj_by_type(hwloc.OBJ_NUMANODE, 2)
)
assert value1 == values[1 * 4 + 2]
assert value2 == values[2 * 4 + 1]
# check helpers on errors
assert distances[0].obj_index(topology.get_obj_by_type(hwloc.OBJ_PU, 0)) == -1
value1, value2 = distances[0].obj_pair_values(
    topology.get_obj_by_type(hwloc.OBJ_PU, 1),
    topology.get_obj_by_type(hwloc.OBJ_PU, 2)
)
assert value1 is None
assert value2 is None
# check that some random values are ok
assert distances[0].values[0] == 1  # diagonal
assert distances[0].values[4] == 4  # same group
assert distances[0].values[6] == 8  # different group
assert distances[0].values[9] == 8  # different group
assert distances[0].values[10] == 1  # diagonal
assert distances[0].values[14] == 4  # same group
del distances

print('Inserting PU distances')
# matrix 4*2*2
objs = [topology.get_obj_by_type(hwloc.OBJ_PU, i) for i in range(16)]
values = [8]*256
for i in range(4):
    for j in range(4):
        for k in range(4):
            values[i * 64 + i * 4 + 16 * j + k] = 4
    values[i * 64 + i * 4 + 1] = 2
    values[i * 64 + i * 4 + 16] = 2
    values[i * 64 + i * 4 + 2 * 16 + 3] = 2
    values[i * 64 + i * 4 + 3 * 16 + 2] = 2
for i in range(16):
    values[i + 16 * i] = 1
topology.distances_add(objs, values, kind, hwloc.DISTANCES_ADD_FLAG_GROUP)

topodepth = topology.depth
assert topodepth == 6
check_distances(topology, 0, 0)
check_distances(topology, 1, 0)
check_distances(topology, 2, 0)
check_distances(topology, 3, 0)
check_distances(topology, 4, 0)
check_distances(topology, 5, 1)
check_distances(topology, hwloc.TYPE_DEPTH_NUMANODE, 1)

# check PU distances
print('Checking PU distances')
distances = topology.distances_get_by_type(hwloc.OBJ_PU, 0, 0)
assert len(distances) == 1
assert distances[0].kind == kind
# check that some random values are OK
assert distances[0].values[0] == 1  # diagonal
assert distances[0].values[1] == 2  # same group
assert distances[0].values[3] == 4  # same biggroup
assert distances[0].values[15] == 8  # different biggroup
assert distances[0].values[250] == 8  # different biggroup
assert distances[0].values[253] == 4  # same group
assert distances[0].values[254] == 2  # same biggroup
assert distances[0].values[255] == 1  # diagonal
del distances

print('Inserting 2nd PU distances')
# matrix 4*1
objs = [topology.get_obj_by_type(hwloc.OBJ_PU, i) for i in range(4)]
values = [3] * 16
for i in range(4):
    values[i + 4 * i] = 7
kind = (
    hwloc.DISTANCES_KIND_MEANS_BANDWIDTH | hwloc.DISTANCES_KIND_FROM_USER
)
topology.distances_add(objs, values, kind, hwloc.DISTANCES_ADD_FLAG_GROUP)
topodepth = topology.depth
assert topodepth == 6
check_distances(topology, 0, 0)
check_distances(topology, 1, 0)
check_distances(topology, 2, 0)
check_distances(topology, 3, 0)
check_distances(topology, 4, 0)
check_distances(topology, 5, 2)
check_distances(topology, hwloc.TYPE_DEPTH_NUMANODE, 1)

# check PU distances
print('Checking 2nd PU distances')
distances = topology.distances_get_by_type(hwloc.OBJ_PU, 0, 0)
assert len(distances) == 2
assert distances[1].kind == kind
# check some random values are OK
assert distances[1].values[0] == 7  # diagonal
assert distances[1].values[1] == 3  # other
assert distances[1].values[3] == 3  # other
assert distances[1].values[15] == 7  # diagonal
del distances

# check distances by kind
distances = topology.distances_get(hwloc.DISTANCES_KIND_MEANS_BANDWIDTH, 0)
assert len(distances) == 1
del distances
kind = (
    hwloc.DISTANCES_KIND_MEANS_LATENCY | hwloc.DISTANCES_KIND_FROM_OS
)
distances = topology.distances_get(kind, 0)
assert len(distances) == 0
kind = (
    hwloc.DISTANCES_KIND_MEANS_LATENCY | hwloc.DISTANCES_KIND_FROM_USER
)
distances = topology.distances_get(kind, 0)
assert len(distances) == 2
del distances

# remove distances
print('Removing distances')
topology.distances_remove_by_type(hwloc.OBJ_PU)
distances = topology.distances_get_by_type(hwloc.OBJ_PU, 0, 0)
assert len(distances) == 0
distances = topology.distances_get_by_type(hwloc.OBJ_NUMANODE, 0, 0)
assert len(distances) == 1
topology.distances_remove()
distances = topology.distances_get_by_type(hwloc.OBJ_PU, 0, 0)
assert len(distances) == 0
distances = topology.distances_get_by_type(hwloc.OBJ_NUMANODE, 0, 0)
assert len(distances) == 0
