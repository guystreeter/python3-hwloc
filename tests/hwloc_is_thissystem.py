#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_is_thissystem.c
# from the hwloc package.
#

import hwloc


def result(msg: str, run):
    try:
        ret = run()
        print('%-30s: OK' % (msg,))
        return ret
    except OSError as err:
        print('%-30s: FAILED (%d, %s)' % (msg, err.errno, err.message))


topo = hwloc.Topology()
topo.load()

# check the OS topology
assert topo.is_thissystem

cpuset = topo.complete_cpuset.dup()
result("Binding with OS backend", lambda: topo.set_cpubind(cpuset, 0))

del topo

# We're assume there is a real processor numbered 0
cpuset.zero()
cpuset.set(0)

# check a synthetic topology
topo = hwloc.Topology()
topo.set_synthetic("1")
topo.load()
assert not topo.is_thissystem

result("Binding with synthetic backend", lambda: topo.set_cpubind(cpuset, 0))

del topo

# check a synthetic topology but assuming it's the system topology
topo = hwloc.Topology()
topo.set_flags(hwloc.TOPOLOGY_FLAG_IS_THISSYSTEM)
topo.set_synthetic("1")
topo.load()
assert topo.is_thissystem

result(
    "Binding with synthetic backend faking is_thissystem",
    lambda: topo.set_cpubind(cpuset, 0),
)
