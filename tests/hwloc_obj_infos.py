#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_obj_infos.c
# from the hwloc package.
#

import hwloc

NAME1 = "foobar"
VALUE1 = "myvalue"
NAME2 = "foobaz"
VALUE2 = "myothervalue"

topology = hwloc.Topology()
topology.load()

obj = topology.root_obj

assert not obj.get_info_by_name(NAME1)
assert not obj.get_info_by_name(NAME2)

obj.add_info(NAME1, VALUE1)
assert not obj.get_info_by_name(NAME2)
obj.add_info(NAME2, VALUE2)

assert obj.get_info_by_name(NAME1) == VALUE1
assert obj.get_info_by_name(NAME2) == VALUE2
