#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/hwloc_bitmap.c
# from the hwloc package.
#

import hwloc

# check an empty bitmap
myset = hwloc.Bitmap.alloc()
assert myset.weight() == 0
assert myset.first == -1
assert myset.last == -1
assert myset.ulong() == 0
assert myset.ulong(0) == 0
assert myset.ulong(1) == 0
assert myset.ulong(23) == 0
# check a non-empty bitmap
myset.from_ulong(0xff, 4)
assert myset.ulong(4) == 0xff
assert myset.ulong() == 0
assert myset.ulong(0) == 0
assert myset.ulong(1) == 0
assert myset.ulong(23) == 0
# check a two-long bitmap
myset.from_ulong(0xfe, 4)
myset.set_ulong(0xef, 6)
assert myset.ulong(4) == 0xfe
assert myset.ulong(6) == 0xef
assert myset.ulong() == 0
assert myset.ulong(0) == 0
assert myset.ulong(1) == 0
assert myset.ulong(23) == 0
# check a zeroed bitmap
myset.zero()
assert myset.weight() == 0
assert myset.first == -1
assert myset.last == -1
assert myset.ulong() == 0
assert myset.ulong(0) == 0
assert myset.ulong(1) == 0
assert myset.ulong(4) == 0
assert myset.ulong(23) == 0
del myset

# check a full bitmap
myset = hwloc.Bitmap.alloc_full()
assert myset.weight() == -1
assert myset.first == 0
assert myset.last == -1
assert myset.ulong() == hwloc.ULONG_MAX
assert myset.ulong(0) == hwloc.ULONG_MAX
assert myset.ulong(1) == hwloc.ULONG_MAX
assert myset.ulong(23) == hwloc.ULONG_MAX
# check a almost full bitmap
myset.set_ulong(0xff, 4)
assert myset.ulong(4) == 0xff
assert myset.ulong() == hwloc.ULONG_MAX
assert myset.ulong(0) == hwloc.ULONG_MAX
assert myset.ulong(1) == hwloc.ULONG_MAX
assert myset.ulong(23) == hwloc.ULONG_MAX
# check a almost empty bitmap
myset.from_ulong(0xff, 4)
assert myset.ulong(4) == 0xff
assert myset.ulong() == 0
assert myset.ulong(0) == 0
assert myset.ulong(1) == 0
assert myset.ulong(23) == 0
# check a filled bitmap
myset.fill()
assert myset.weight() == -1
assert myset.first == 0
assert myset.last == -1
assert myset.ulong(4) == hwloc.ULONG_MAX
assert myset.ulong() == hwloc.ULONG_MAX
assert myset.ulong(0) == hwloc.ULONG_MAX
assert myset.ulong(1) == hwloc.ULONG_MAX
assert myset.ulong(23) == hwloc.ULONG_MAX
del myset

# check ranges
myset = hwloc.Bitmap.alloc()
assert myset.iszero
assert not myset.isfull
assert myset.weight() == 0
assert myset.first == -1
assert myset.last == -1
assert myset.first_unset == 0
assert myset.last_unset == -1
assert myset.next_unset(-1) == 0
# 20-39
myset.set_range(20, 39)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == 20
assert myset.first == 20
assert myset.last == 39
assert myset.first_unset == 0
assert myset.last_unset == -1
assert myset.next_unset(19) == 40
# 20-39,80-
myset.set_range(80, -1)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == -1
assert myset.first == 20
assert myset.next(39) == 80
assert myset.last == -1
assert myset.first_unset == 0
assert myset.last_unset == 79
assert myset.next_unset(79) == -1
# 20-39,80-359
myset.clr_range(360, -1)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == 300
assert myset.first == 20
assert myset.next(39) == 80
assert myset.last == 359
assert myset.first_unset == 0
assert myset.last_unset == -1
assert myset.next_unset(100) == 360
# 20-39,80-179,280-359
myset.clr_range(180, 279)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == 200
assert myset.first == 20
assert myset.next(39) == 80
assert myset.next(179) == 280
assert myset.last == 359
assert myset.first_unset == 0
assert myset.last_unset == -1
assert myset.next_unset(100) == 180
# 20-
myset.set_range(35, -1)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == -1
assert myset.first == 20
assert myset.last == -1
assert myset.first_unset == 0
assert myset.last_unset == 19
assert myset.next_unset(100) == -1
# 20-419
myset.clr_range(420, -1)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == 400
assert myset.first == 20
assert myset.last == 419
assert myset.first_unset == 0
assert myset.last_unset == -1
assert myset.next_unset(100) == 420
# 20-419,1000-
myset.set_range(1000, -1)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == -1
assert myset.first == 20
assert myset.next(419) == 1000
assert myset.last == -1
assert myset.first_unset == 0
assert myset.last_unset == 999
assert myset.next_unset(1000) == -1
# 20-
myset.set_range(420, 999)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == -1
assert myset.first == 20
assert myset.last == -1
assert myset.first_unset == 0
assert myset.last_unset == 19
assert myset.next_unset(1000) == -1
# 0-
myset.set_range(0, 25)
assert not myset.iszero
assert myset.isfull
assert myset.weight() == -1
assert myset.first == 0
assert myset.last == -1
assert myset.first_unset == -1
assert myset.last_unset == -1
assert myset.next_unset(1000) == -1
# 0-99,1500-
myset.clr_range(100, 1499)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == -1
assert myset.first == 0
assert myset.next(99) == 1500
assert myset.last == -1
assert myset.first_unset == 100
assert myset.last_unset == 1499
assert myset.next_unset(99) == 100
# 0-99,1500-1999
myset.clr_range(2000, -1)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == 600
assert myset.first == 0
assert myset.next(99) == 1500
assert myset.last == 1999
assert myset.first_unset == 100
assert myset.last_unset == -1
assert myset.next_unset(1800) == 2000
# 0-99,1500-
myset.set_range(1500, -1)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == -1
assert myset.first == 0
assert myset.next(99) == 1500
assert myset.last == -1
assert myset.first_unset == 100
assert myset.last_unset == 1499
assert myset.next_unset(1000) == 1001
# 0-99,1500-2199
myset.clr_range(2200, -1)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == 800
assert myset.first == 0
assert myset.next(99) == 1500
assert myset.last == 2199
assert myset.first_unset == 100
assert myset.last_unset == -1
assert myset.next_unset(1800) == 2200
# 0-99,1500-1999
myset.clr_range(2000, -1)
assert not myset.iszero
assert not myset.isfull
assert myset.weight() == 600
assert myset.first == 0
assert myset.next(99) == 1500
assert myset.last == 1999
assert myset.first_unset == 100
assert myset.last_unset == -1
assert myset.next_unset(1800) == 2000

del myset

# check miscellaneous other functions
myset = hwloc.Bitmap.alloc()
# from_ulong
myset.from_ulong(0x0ff0)
assert myset.first == 4
assert myset.last == 11
assert myset.weight() == 8
assert myset.ulong(0) == 0x0ff0
assert myset.ulong(1) == 0
# from_ith_ulong
myset.zero()
assert myset.weight() == 0
myset.from_ulong(0xff00, 2)
assert myset.weight() == 8
assert myset.ulong(0) == 0
assert myset.ulong(1) == 0
assert myset.ulong(2) == 0xff00
assert myset.ulong(0) == 0
# allbut and not
myset.allbut(153)
assert myset.weight() == -1
myset = ~myset
assert myset.weight() == 1
assert myset.first == 153
assert myset.last == 153
# clr_range
myset.fill()
myset.clr_range(178, 3589)
myset = ~myset
assert myset.weight() == 3589 - 178 + 1
assert myset.first == 178
assert myset.last == 3589
# singlify
myset.zero()
myset.set_range(0, 127)
assert myset.weight() == 128
myset = ~myset
assert myset.weight() == -1
myset.singlify()
assert myset.weight() == 1
assert myset.first == 128
assert myset.last == 128

del myset
