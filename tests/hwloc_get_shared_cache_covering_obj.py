#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_get_shared_cache_covering_obj.c
# from the hwloc package.
#

import hwloc

SYNTHETIC_TOPOLOGY_DESCRIPTION_SHARED = 'numa:1 group:6 pack:5 l2:4 core:3 pu:2'  # 736bits wide topology
SYNTHETIC_TOPOLOGY_DESCRIPTION_NONSHARED = 'numa:1 group:6 pack:5 l2:4 core:1 pu:2'  # 736bits wide topology

topo = hwloc.Topology()

topo.set_synthetic(SYNTHETIC_TOPOLOGY_DESCRIPTION_SHARED)
topo.load()

# check the cache above a given cpu
CPUINDEX = 180
obj = topo.get_obj_by_depth(5, CPUINDEX)
assert obj
cache = obj.get_shared_cache_covering()
assert cache
assert hwloc.Obj.type_is_dcache(cache.type)
assert cache.logical_index == CPUINDEX // 2 // 3
assert topo.obj_is_in_subtree(obj, cache)

# check no shared cache above the L2 cache
obj = topo.get_obj_by_depth(3, 0)
assert obj
cache = obj.get_shared_cache_covering()
assert not cache

# check no shared cache above the node
obj = topo.get_obj_by_depth(hwloc.TYPE_DEPTH_NUMANODE, 0)
assert obj
cache = obj.get_shared_cache_covering()
assert not cache

topo = hwloc.Topology()

topo.set_synthetic(SYNTHETIC_TOPOLOGY_DESCRIPTION_NONSHARED)
topo.load()

# check the cache above a given cpu
CPUINDEX = 180
obj = topo.get_obj_by_depth(5, CPUINDEX)
assert obj
cache = obj.get_shared_cache_covering()
assert cache
assert hwloc.Obj.type_is_dcache(cache.type)
assert cache.logical_index == CPUINDEX // 2 // 1
assert obj.is_in_subtree(cache)

# check no shared-cache above the core
obj = topo.get_obj_by_depth(4, CPUINDEX // 2)
assert obj
cache = obj.get_shared_cache_covering()
assert not cache
