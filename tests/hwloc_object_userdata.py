#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_object_userdata.c
# from the hwloc package.
#

RANDOMSTRINGLENGTH = 128
RANDOMSTRINGSHORTTESTS = 5
RANDOMSTRINGLONGTESTS = 9

import hwloc


def check(t: hwloc.Topology):
    for i in range(t.depth):
        for j in range(t.get_nbobjs_by_depth(i)):
            assert t.get_obj_by_depth(i, j).userdata == 0


def export_cb(reserved, topology: hwloc.C.topology_ptr, obj: hwloc.C.obj_ptr):
    topology = hwloc.Topology(topology)
    obj = hwloc.Obj(obj)
    tmp = '%016x' % (obj.userdata,)
    topology.export_obj_userdata(reserved, obj, 'MyName', tmp)
    for i in range(1, RANDOMSTRINGSHORTTESTS + 1):
        tmp = 'EncodedShort%u' % (i,)
        randomstring = str(chr(i)) * (RANDOMSTRINGLENGTH - i)
        topology.export_obj_userdata_base64(reserved, obj, tmp, randomstring)
    for i in range(1, RANDOMSTRINGLONGTESTS + 1):
        tmp = 'EncodedLong%u' % (i,)
        randomstring = str(chr(i)) * (RANDOMSTRINGLENGTH - i)
        topology.export_obj_userdata_base64(reserved, obj, tmp, randomstring)


def import_cb(topology: hwloc.C.topology_ptr, obj: hwloc.C.obj_ptr, name, buf):
    if name is None:
        assert buf[0] == chr(0)

    elif name == 'MyName':
        assert len(buf) == 16
        val = int(buf)
        if val == 0x1:
            assert obj.type == hwloc.OBJ_MACHINE
            obj.userdata = 0x4
        elif val == 0x2:
            assert obj.type == hwloc.OBJ_L2CACHE
            obj.userdata = 0x5
        elif val == 0x3:
            assert obj.type == hwloc.OBJ_PU
            obj.userdata = 0x6
        else:
            assert False

    elif name.startswith('EncodedShort'):
        i = int(name[len('EncodedShort'):])
        assert i > 0 and i <= RANDOMSTRINGSHORTTESTS
        assert len(buf) == RANDOMSTRINGLENGTH - i
        assert buf == str(chr(i)) * (RANDOMSTRINGLENGTH - i)

    elif name.startswith('EncodedLong'):
        i = int(name[len('EncodedLong') :])
        assert i > 0 and i <= RANDOMSTRINGLONGTESTS
        assert len(buf) == RANDOMSTRINGLENGTH - i
        assert buf == str(chr(i)) * (RANDOMSTRINGLENGTH - i)

    else:
        assert False


# check the real topology
topo = hwloc.Topology()
topo.load()
check(topo)
assert topo.userdata is None
del topo

# check a synthetic topology
topo = hwloc.Topology()
topo.userdata = 0x987654
topo.set_synthetic('6 5 4 3 2')
topo.load()
check(topo)

# now place some userdata and see if importing/exporting works well
#
# NB: user data can only be integer type.
#
obj1 = topo.root_obj
assert obj1
obj1.userdata = 0
assert obj1.userdata == 0
try:
    obj1.userdata = 'x'
    assert False
except hwloc.ArgError:
    pass
obj1.userdata = 0x1
obj2 = topo.get_obj_by_depth(3, 13)
assert obj2
obj2.userdata = 0x2
obj3 = topo.get_obj_by_depth(5, 2 * 3 * 4 * 5 * 6 - 1)
assert obj3
obj3.userdata = 0x3
assert obj1.userdata == 1

# export/import without callback, we get nothing
xmlbuf = topo.export_xmlbuffer()
reimport = hwloc.Topology()
reimport.set_xmlbuffer(xmlbuf)
reimport.load()
check(reimport)  # there should be no userdata
del reimport

# export/import with callback, we should get three userdata
topo.set_userdata_export_callback(export_cb)
xmlbuf = topo.export_xmlbuffer()
reimport = hwloc.Topology()
reimport.set_userdata_import_callback(import_cb)
reimport.set_xmlbuffer(xmlbuf)
reimport.load()
obj1 = reimport.root_obj
assert obj1
assert obj1.userdata == 0x4
obj2 = reimport.get_obj_by_depth(3, 13)
assert obj2
assert obj2.userdata == 0x5
obj3 = reimport.get_obj_by_depth(5, 2 * 3 * 4 * 5 * 6 - 1)
assert obj3
assert obj3.userdata == 0x6
# topology userdata is not exported
assert reimport.userdata == None
del reimport

assert topo.userdata == 0x987654

del topo
