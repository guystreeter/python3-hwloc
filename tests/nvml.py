#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/nvml.c from the
# hwloc package. This test is limited for lack of python bindings for the
# nvml library.
#

import hwloc

print('This doesn\'t work. No nvml bindings are available')
assert False

topology = hwloc.Topology()
topology.set_io_types_filter(hwloc.TYPE_FILTER_KEEP_IMPORTANT)
topology.load()

i = 0
while True:
    osdev = topology.nvml_get_device_osdev_by_index(i)
    if osdev is None:
        break

    ancestor = osdev.non_io_ancestor
    print('found OSDev', osdev.name)
    assert osdev.name.startswith('nvml')
    assert int(osdev.name[4:]) == i

    assert osdev.get_info_by_name('Backend') == 'NVML'

    assert osdev.attr.osdev.type == hwloc.OBJ_OSDEV_GPU

    print('found OSDev model', osdev.get_info_by_name('GPUModel'))

    i += 1
