#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019-2020 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/glibc-sched.c from the
# hwloc package.
#

from __future__ import print_function
import hwloc
from hwloc import linuxsched

topo = hwloc.Topology()
topo.load()
depth = topo.depth

hwlocset = topo.complete_cpuset.dup()

linuxsched.Setaffinity(0, hwlocset.all_set_bits)

schedset = linuxsched.Getaffinity(0)
hwlocset = hwloc.Bitmap.alloc(schedset)
assert hwlocset in topo.complete_cpuset
hwlocset = hwlocset.andnot(topo.allowed_cpuset)
assert hwlocset.iszero

obj = topo.get_obj_by_depth(depth - 1, topo.get_nbobjs_by_depth(depth - 1) - 1)
assert obj
assert obj.type == hwloc.OBJ_PU

hwlocset = obj.cpuset.dup()
linuxsched.Setaffinity(0, hwlocset.all_set_bits)

schedset = linuxsched.Getaffinity(0)
hwlocset = hwloc.Bitmap.alloc(schedset)
assert hwlocset == obj.cpuset
