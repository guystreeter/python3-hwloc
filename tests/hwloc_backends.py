#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/hwloc_backends.c from the
# hwloc package.
#

import hwloc
import tempfile
import os


def get_backend_name(topo: hwloc.Topology) -> str:
    root = topo.root_obj
    return root.get_info_by_name('Backend')


def assert_backend_name(topo: hwloc.Topology, wanted: str):
    found = get_backend_name(topo)
    assert found is not None
    assert found == wanted


def assert_foo_bar(topo: hwloc.Topology, setoronot: bool):
    root = topo.root_obj
    found = root.get_info_by_name('Foo')
    if setoronot:
        assert found == 'Bar'
    else:
        assert found is None


xmlbuf = None
xmlfileok = False

print('trying to export topology to XML buffer and file for later...')
topology1 = hwloc.Topology()
topology1.load()
orig_backend_name = get_backend_name(topology1)
topology1.root_obj.add_info('Foo', 'Bar')
assert topology1.is_thissystem

try:
    xmlbuf = topology1.export_xmlbuffer()
except OSError as e:
    print('XML buffer export failed (%s), ignoring' % str(e))

with tempfile.NamedTemporaryFile(prefix='hwloc_backends.tmpxml') as xmlfile:
    try:
        topology1.export_xml(xmlfile.name)
        xmlfileok = True
    except OSError as e:
        print('XML file export failed (%s), ignoring' % str(e))

    print('init...')
    topology2 = hwloc.Topology()
    if xmlfileok:
        print('switching to xml...')
        topology2.set_xml(xmlfile.name)
    if xmlbuf:
        print('switching to xmlbuffer...')
        topology2.set_xmlbuffer(xmlbuf)
    print('switching to synthetic...')
    topology2.set_synthetic('pack:2 node:3 l1:2 pu:4')
    del topology2

    if xmlfileok:
        topology2 = hwloc.Topology()
        print('switching to xml and loading...')
        topology2.set_xml(xmlfile.name)
        topology2.load()
        assert_backend_name(topology2, orig_backend_name)
        assert_foo_bar(topology2, True)
        topology2.check()
        assert not topology2.is_thissystem
        del topology2

    if xmlbuf:
        topology2 = hwloc.Topology()
        print('switching to xmlbuffer and loading...')
        topology2.set_xmlbuffer(xmlbuf)
        topology2.load()
        assert_backend_name(topology2, orig_backend_name)
        assert_foo_bar(topology2, True)
        topology2.check()
        assert not topology2.is_thissystem
        del topology2
        topology2 = hwloc.Topology()

    topology2 = hwloc.Topology()
    print('switching to synthetic and loading...')
    topology2.set_synthetic('pack:2 node:3 l3i:2 pu:4')
    topology2.load()
    assert_backend_name(topology2, 'Synthetic')
    assert_foo_bar(topology2, False)
    assert topology2.get_nbobjs_by_type(hwloc.OBJ_PU) == 2*3*2*4
    topology2.check()
    assert not topology2.is_thissystem
    del topology2

    print('switching to xml by env and loading...')
    os.environ['HWLOC_XMLFILE'] = xmlfile.name
    topology2 = hwloc.Topology()
    topology2.load()
    assert_backend_name(topology2, orig_backend_name)
    assert_foo_bar(topology2, True)
    topology2.check()
    del topology2

topology2 = hwloc.Topology()
print('switching to synthetic by env and loading...')
os.environ['HWLOC_SYNTHETIC'] = 'node:3 pu:3'
topology2.load()
assert_backend_name(topology2, 'Synthetic')
assert_foo_bar(topology2, False)
assert topology2.get_nbobjs_by_type(hwloc.OBJ_PU) == 3 * 3
topology2.check()
assert not topology2.is_thissystem
del topology2

topology2 = hwloc.Topology()
print('switching to default components by env and loading...')
os.environ['HWLOC_COMPONENTS'] = ','
topology2.load()
assert_backend_name(topology2, orig_backend_name)
assert_foo_bar(topology2, False)
topology2.check()
assert topology2.is_thissystem
del topology2
del topology1
