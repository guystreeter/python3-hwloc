#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_type_sscanf.c
# from the hwloc package.
#

import hwloc


def check(topology: hwloc.Topology, obj: hwloc.Obj, verbose: int):
    buffer_ = hwloc.Obj.type_asprintf(obj, verbose)
    assert len(buffer_) > 0
    type_, attr = hwloc.Obj.type_sscanf(buffer_)
    assert obj.type == type_
    if hwloc.Obj.type_is_cache(type_):
        assert attr.cache.type == obj.attr.cache.type
        assert attr.cache.depth == obj.attr.cache.depth
    elif type_ == hwloc.OBJ_GROUP:
        assert attr.group.depth == obj.attr.group.depth
    elif type_ == hwloc.OBJ_BRIDGE:
        assert attr.bridge.upstream_type == obj.attr.bridge.upstream_type
        assert attr.bridge.downstream_type == obj.attr.bridge.downstream_type
    elif type_ == hwloc.OBJ_OS_DEVICE:
        assert attr.osdev.type == obj.attr.osdev.type

    depth = topology.type_sscanf_as_depth(buffer_)[1]
    assert depth == obj.depth

    for child in obj.children:
        check(topology, child, verbose)
    for child in obj.memory_children:
        check(topology, child, verbose)
    for child in obj.io_children:
        check(topology, child, verbose)
    for child in obj.misc_children:
        check(topology.chiild, verbose)


topology = hwloc.Topology()
topology.set_all_types_filter(hwloc.TYPE_FILTER_KEEP_ALL)
topology.load()

check(topology, topology.root_obj, 0)
check(topology, topology.root_obj, 1)
