#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_get_obj_below_array_by_type.c
# from the hwloc package.
#

import hwloc

topo = hwloc.Topology()

topo.set_synthetic('numa:1 pack:3 l2:3 core:3 pu:3')

topo.load()

# find the first thread
obj = topo.get_obj_below_array_by_type((hwloc.OBJ_PACKAGE, 0),
                                       (hwloc.OBJ_L2CACHE, 0),
                                       (hwloc.OBJ_CORE, 0),
                                       (hwloc.OBJ_PU, 0))
assert obj == topo.get_obj_by_depth(4, 0)

# find the last core
obj = topo.get_obj_below_array_by_type((hwloc.OBJ_PACKAGE, 2),
                                       (hwloc.OBJ_L2CACHE, 2),
                                       (hwloc.OBJ_CORE, 2))
assert obj == topo.get_obj_by_depth(3, 26)

# misc tests

obj = topo.get_obj_below_array_by_type((hwloc.OBJ_L2CACHE, 2))
assert obj == topo.get_obj_by_depth(2, 2)

obj = topo.get_obj_below_array_by_type((hwloc.OBJ_PACKAGE, 2),
                                       (hwloc.OBJ_CORE, 2))
assert obj == topo.get_obj_by_depth(3, 20)
# check that hwloc_get_obj_below_by_type works as well
obj = topo.get_obj_below_by_type(hwloc.OBJ_PACKAGE, 2, hwloc.OBJ_CORE, 2)
assert obj == topo.get_obj_by_depth(3, 20)

obj = topo.get_obj_below_array_by_type((hwloc.OBJ_L2CACHE, 1),
                                       (hwloc.OBJ_PU, 1))
assert obj == topo.get_obj_by_depth(4, 10)
# check that hwloc_get_obj_below_by_type works as well
obj = topo.get_obj_below_by_type(hwloc.OBJ_L2CACHE, 1, hwloc.OBJ_PU, 1)
assert obj == topo.get_obj_by_depth(4, 10)
