#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019-2020 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/linux-libnuma.c
# from the hwloc package.
#

import hwloc
import libnuma

assert libnuma.Available()

topology = hwloc.Topology()
topology.load()

Set = hwloc.Bitmap.alloc()
nocpunomemnodeset = hwloc.Bitmap.alloc()
nocpubutmemnodeset = hwloc.Bitmap.alloc()
nomembutcpunodeset = hwloc.Bitmap.alloc()
nomembutcpucpuset = hwloc.Bitmap.alloc()

if topology.get_nbobjs_by_type(hwloc.OBJ_NUMANODE):
    for node in topology.objs_by_type(hwloc.OBJ_NUMANODE):
        Set |= node.cpuset
        if node.cpuset.iszero:
            if node.attr.numanode.local_memory:
                nocpubutmemnodeset.set(node.os_index)
            else:
                nocpunomemnodeset.set(node.os_index)
        elif not node.attr.numanode.local_memory:
            nomembutcpunodeset.set(node.os_index)
            nomembutcpucpuset |= node.cpuset
# else:
#     Set |= topology.complete_cpuset

set2 = topology.cpuset_from_linux_libnuma_bitmask(libnuma.AllNodes)
# numa_all_nodes_ptr doesn't contain NODES with CPU but no memory
set2 |= nomembutcpucpuset
assert Set == set2

bitmask = topology.cpuset_to_linux_libnuma_bitmask(Set)
# numa_all_nodes_ptr contains NODES with no CPU but with memory
for i in nocpubutmemnodeset:
    bitmask.setbit(i)
assert bitmask == libnuma.AllNodes

# convert full stuff between nodeset and libnuma
Set = topology.root_obj.complete_nodeset.dup()

set2 = topology.nodeset_from_linux_libnuma_bitmask(libnuma.AllNodes)
# numa_all_nodes_ptr doesn't contain NODES with no CPU and no memory
set2.set(tuple(nocpunomemnodeset))
set2 |= nomembutcpunodeset
assert Set == set2

bitmask = topology.nodeset_to_linux_libnuma_bitmask(Set)
assert bitmask == libnuma.AllNodes

# convert empty stuff between cpuset and libnuma
bitmask = libnuma.BitmaskAlloc(1)
Set = topology.cpuset_from_linux_libnuma_bitmask(bitmask)
assert Set.iszero

Set = topology.cpuset_from_linux_libnuma_ulongs([0])
assert Set.iszero

Set = hwloc.Bitmap.alloc()
bitmask = topology.cpuset_to_linux_libnuma_bitmask(Set)
bitmask2 = libnuma.BitmaskAlloc(1)
assert bitmask == bitmask2

Set = hwloc.Bitmap.alloc()
mask, maxnode = topology.cpuset_to_linux_libnuma_ulongs(Set)
assert len(mask) == 0
assert maxnode == 0

# convert empty stuff between nodeset and libnuma
bitmask = libnuma.BitmaskAlloc(1)
Set = topology.nodeset_from_linux_libnuma_bitmask(bitmask)
assert Set.iszero

Set = hwloc.Bitmap.alloc()
mask, maxnode = topology.nodeset_to_linux_libnuma_ulongs(Set)
assert len(mask) == 0
assert maxnode == 0

Set = hwloc.Bitmap.alloc()
bitmask = topology.nodeset_to_linux_libnuma_bitmask(Set)
bitmask2 = libnuma.BitmaskAlloc(1)
assert bitmask == bitmask2

# convert first node (with CPU and memory) between cpuset/nodeset and libnuma
node = topology.get_next_obj_by_type(hwloc.OBJ_NUMANODE)
while node and (not node.attr.numanode.local_memory or node.cpuset.iszero):
    # skip nodes with no cpus or no memory to avoid strange libnuma behaviors
    node = node.sibling
if node:
    # convert first node between cpuset and libnuma
    bitmask = topology.cpuset_to_linux_libnuma_bitmask(node.cpuset)
    assert bitmask.isbitset(node.os_index)
    bitmask.clearbit(node.os_index)
    bitmask2 = libnuma.BitmaskAlloc(node.os_index + 1)
    assert bitmask == bitmask2

    mask, maxnode = topology.cpuset_to_linux_libnuma_ulongs(node.cpuset)
    assert mask is None or isinstance(mask, tuple)
    assert maxnode == node.os_index + 1
    assert mask[0] == 1 << node.os_index

    bitmask = libnuma.BitmaskAlloc(node.os_index + 1)
    bitmask.setbit(node.os_index)
    Set = topology.cpuset_from_linux_libnuma_bitmask(bitmask)
    assert Set == node.cpuset

    mask = [0]
    if node.os_index < maxnode:
        mask[0] = 1 << node.os_index
    Set = topology.cpuset_from_linux_libnuma_ulongs(mask)
    assert Set == node.cpuset

    # convert first node between nodeset and libnuma
    bitmask = topology.nodeset_to_linux_libnuma_bitmask(node.nodeset)
    assert bitmask.isbitset(node.os_index)
    bitmask.clearbit(node.os_index)
    bitmask2 = libnuma.BitmaskAlloc(node.os_index + 1)
    assert bitmask == bitmask2

    mask, maxnode = topology.nodeset_to_linux_libnuma_ulongs(node.nodeset)
    assert mask is None or isinstance(mask, tuple)
    if node.os_index >= maxnode:
        assert mask[0] == 0
    else:
        assert mask[0] == 1 << node.os_index

    bitmask = libnuma.BitmaskAlloc(node.os_index + 1)
    bitmask.setbit(node.os_index)
    Set = topology.nodeset_from_linux_libnuma_bitmask(bitmask)
    assert Set == node.nodeset

    mask = [0]
    if node.os_index < maxnode:
        mask[0] = 1 << node.os_index
    Set = topology.nodeset_from_linux_libnuma_ulongs(mask)
    assert Set == node.nodeset
