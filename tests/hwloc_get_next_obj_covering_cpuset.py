#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_get_next_obj_covering_cpuset.c
# from the hwloc package.
#

import hwloc

topo = hwloc.Topology()

set1 = hwloc.Bitmap.alloc()

topo.set_synthetic('pack:8 core:2 1')
topo.load()

set1.sscanf('00008f18')

obj = topo.get_next_obj_covering_cpuset_by_type(set1, hwloc.OBJ_PACKAGE, None)
assert obj == topo.get_obj_by_depth(1, 1)
obj = topo.get_next_obj_covering_cpuset_by_type(set1, hwloc.OBJ_PACKAGE, obj)
assert obj == topo.get_obj_by_depth(1, 2)
obj = topo.get_next_obj_covering_cpuset_by_type(set1, hwloc.OBJ_PACKAGE, obj)
assert obj == topo.get_obj_by_depth(1, 4)
obj = topo.get_next_obj_covering_cpuset_by_type(set1, hwloc.OBJ_PACKAGE, obj)
assert obj == topo.get_obj_by_depth(1, 5)
obj = topo.get_next_obj_covering_cpuset_by_type(set1, hwloc.OBJ_PACKAGE, obj)
assert obj == topo.get_obj_by_depth(1, 7)
obj = topo.get_next_obj_covering_cpuset_by_type(set1, hwloc.OBJ_PACKAGE, obj)
assert not obj

del topo

topo = hwloc.Topology()
topo.set_synthetic('node:2 pack:5 core:3 4')
topo.load()

set1.sscanf('0ff08000')

depth = topo.get_type_depth(hwloc.OBJ_PACKAGE)
assert depth == 2
obj = topo.get_next_obj_covering_cpuset_by_depth(set1, depth, None)
assert obj == topo.get_obj_by_depth(depth, 1)
obj = topo.get_next_obj_covering_cpuset_by_depth(set1, depth, obj)
assert obj == topo.get_obj_by_depth(depth, 2)
obj = topo.get_next_obj_covering_cpuset_by_depth(set1, depth, obj)
assert not obj
