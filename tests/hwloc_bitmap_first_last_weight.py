#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_bitmap_first_last_weight.c
# from the hwloc package.
#

from __future__ import print_function
import hwloc

# empty set
myset = hwloc.Bitmap.alloc()
assert myset.first == -1
assert myset.last == -1
assert myset.next(0) == -1
assert myset.next(-1) == -1
assert myset.weight() == 0
assert myset.first_unset == 0
assert myset.last_unset == -1
assert myset.next_unset(-1) == 0
assert myset.next_unset(0) == 1
assert myset.next_unset(12345) == 12346

# full set
myset.fill()
assert myset.first == 0
assert myset.last == -1
assert myset.next(-1) == 0
assert myset.next(0) == 1
assert myset.next(1) == 2
assert myset.next(2) == 3
assert myset.next(30) == 31
assert myset.next(31) == 32
assert myset.next(32) == 33
assert myset.next(62) == 63
assert myset.next(63) == 64
assert myset.next(64) == 65
assert myset.next(12345) == 12346
assert myset.weight() == -1
assert myset.first_unset == -1
assert myset.last_unset == -1
assert myset.next_unset(-1) == -1
assert myset.next_unset(0) == -1
assert myset.next_unset(12345) == -1

# custom sets
myset.zero()
myset.set_range(36, 59)
assert myset.first == 36
assert myset.last == 59
assert myset.next(-1) == 36
assert myset.next(0) == 36
assert myset.next(36) == 37
assert myset.next(59) == -1
assert myset.weight() == 24
assert myset.first_unset == 0
assert myset.last_unset == -1
assert myset.next_unset(-1) == 0
assert myset.next_unset(0) == 1
assert myset.next_unset(35) == 60
assert myset.next_unset(12345) == 12346
myset.set_range(136, 259)
assert myset.first == 36
assert myset.last == 259
assert myset.next(59) == 136
assert myset.next(259) == -1
assert myset.weight() == 148
assert myset.first_unset == 0
assert myset.last_unset == -1
assert myset.next_unset(-1) == 0
assert myset.next_unset(0) == 1
assert myset.next_unset(35) == 60
assert myset.next_unset(60) == 61
assert myset.next_unset(135) == 260
assert myset.next_unset(12345) == 12346
myset.clr(199)
assert myset.first == 36
assert myset.last == 259
assert myset.next(198) == 200
assert myset.next(199) == 200
assert myset.weight() == 147
assert myset.first_unset == 0
assert myset.last_unset == -1
assert myset.next_unset(-1) == 0
assert myset.next_unset(0) == 1
assert myset.next_unset(35) == 60
assert myset.next_unset(60) == 61
assert myset.next_unset(135) == 199
assert myset.next_unset(199) == 260
assert myset.next_unset(12345) == 12346

i = 0
for cpu in myset:
    if 0 <= i and i < 24:
        expected_cpu = i + 36
    elif 24 <= i and i < 87:
        expected_cpu = i + 112
    elif 87 <= i and i < 147:
        expected_cpu = i + 113

    assert expected_cpu == cpu

    i += 1
