#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_topology_dup.c
# from the hwloc package.
#

import hwloc


kind = hwloc.DISTANCES_KIND_MEANS_LATENCY | hwloc.DISTANCES_KIND_FROM_USER

oldtopology = hwloc.Topology()
oldtopology.set_synthetic('node:3 core:2 pu:4')
oldtopology.load()

nodes = [oldtopology.get_obj_by_type(hwloc.OBJ_NUMANODE, i) for i in range(3)]
node_distances = [None] * 3 * 3
for i in range(3):
    for j in range(3):
        node_distances[i * 3 + j] = 10 if i == j else 20
oldtopology.distances_add(
    nodes,
    node_distances,
    kind,
    hwloc.DISTANCES_ADD_FLAG_GROUP)

cores = [oldtopology.get_obj_by_type(hwloc.OBJ_CORE, i) for i in range(6)]
core_distances = [None] * 6 * 6
for i in range(6):
    for j in range(6):
        core_distances[i * 6 + j] = 4 if i == j else 8
oldtopology.distances_add(
    cores,
    core_distances,
    kind,
    hwloc.DISTANCES_ADD_FLAG_GROUP)

print('duplicating')
topology = oldtopology.dup()
print('destroying the old topology')
del oldtopology

# remove the entire third node
cpuset = hwloc.Bitmap.alloc()
cpuset.fill()
cpuset.clr_range(16, 23)
topology.restrict(cpuset, hwloc.RESTRICT_FLAG_REMOVE_CPULESS)
print('checking the result')
assert topology.get_nbobjs_by_type(hwloc.OBJ_NUMANODE) == 2

distances = topology.distances_get_by_type(hwloc.OBJ_NUMANODE, 0, 0)
assert len(distances) == 1
assert distances[0].nbobjs == 2
assert distances[0].kind == kind
del distances

distances = topology.distances_get_by_type(hwloc.OBJ_CORE, 0, 0)
assert len(distances) == 1
assert distances[0].nbobjs == 4
assert distances[0].kind == kind
del distances
