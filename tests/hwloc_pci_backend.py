#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_pci_backend.c
# from the hwloc package.
#

import hwloc
import os


def get_nb_pcidev(iolevel: int, thissystem: bool, xmlbuf: str) -> int:
    topology = hwloc.Topology()
    if iolevel == 0:
        filter = hwloc.TYPE_FILTER_KEEP_NONE
    elif iolevel == 1:
        filter = hwloc.TYPE_FILTER_KEEP_IMPORTANT
    else:
        filter = hwloc.TYPE_FILTER_KEEP_ALL

    os.environ['HWLOC_THISSYSTEM'] = '1' if thissystem else '0'
    topology.set_io_types_filter(filter)
    if xmlbuf is not None:
        topology.set_xmlbuffer(xmlbuf)
    topology.load()

    nb = topology.get_nbobjs_by_depth(hwloc.TYPE_DEPTH_PCI_DEVICE)

    del topology

    return nb


topology = hwloc.Topology()
topology.set_io_types_filter(hwloc.TYPE_FILTER_KEEP_ALL)
topology.load()
try:
    xmlbuf = topology.export_xmlbuffer()
except OSError as err:
    print('XML buffer export failed (%s), ignoring' % str(err))

# with HWLOC_THISSYSTEM=1
nb = get_nb_pcidev(0, True, None)
assert nb == 0
nbnormal = get_nb_pcidev(1, True, None)
assert nbnormal >= 0  # may get more objects
nbwhole = get_nb_pcidev(2, True, None)
assert nbwhole >= nbnormal  # will get at least as much objects

# XML with with HWLOC_THISSYSTEM=1, should get as many object as a native load
nb = get_nb_pcidev(0, True, xmlbuf)
assert nb == 0
nb = get_nb_pcidev(1, True, xmlbuf)
assert nb == nbnormal
nb = get_nb_pcidev(2, True, xmlbuf)
assert nb == nbwhole

# XML with with HWLOC_THISSYSTEM=0,  should get as many object as a native load
nb = get_nb_pcidev(0, False, xmlbuf)
assert nb == 0
nb = get_nb_pcidev(1, False, xmlbuf)
assert nb == nbnormal
nb = get_nb_pcidev(2, False, xmlbuf)
assert nb == nbwhole
