#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This tests the python hwloc_obj implementation
#

import hwloc

topo = hwloc.Topology()
topo.set_synthetic('numa:1 group:6 pack:5 l2:4 core:3 pu:2')
topo.load()

# root_obj is a topology property
obj = topo.root_obj

# properties of the Obj

assert not obj.parent
assert obj.attr
# objs are equal if they encapsulate the same hwloc_obj structure
assert obj.first_child.parent == obj
assert not obj.next_cousin
assert not obj.userdata

# children is an iterator
for c in obj.children:
    assert c.parent == obj

last = topo.get_obj_by_depth(
    topo.depth - 1, topo.get_nbobjs_by_depth(topo.depth - 1) - 1)
other = last.prev_sibling
assert other

assert last.type_string == other.type_string
assert last.type == hwloc.Obj.type_sscanf(other.type_asprintf(0))[0]

other = last.parent
print(other.type_string, other.attr_asprintf(';', 1))

topo = hwloc.Topology()
topo.load()
obj = topo.root_obj
for i in obj.infos:
    print(i.name, i.value)
