#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/hwloc_bind.c from the
# hwloc package.
#

import hwloc
import os
import ctypes


def result_set(msg, run, supported):
    try:
        ret = run()
        print('%-40s: OK' % (msg,))
        return ret
    except OSError as err:
        prefix = ''
        if not supported:
            prefix = 'X'
        print('%-40s: %sFAILED (%d, %s)' %
              (msg, prefix, err.errno, err.strerror))


def result_get(msg, run, expected, supported):
    try:
        result = run()
        if not expected or (expected == result):
            print('%-40s: OK' % (msg,))
        else:
            print('%-40s: expected' % msg, expected, 'got', result)
    except OSError as err:
        prefix = ''
        if not supported:
            prefix = 'X'
        print('%-40s: %sFAILED (%d, %s)' %
              (msg, prefix, err.errno, err.strerror))


def test(cpuset, flags):
    result_set('Bind this singlethreaded process',
               lambda: topo.set_cpubind(cpuset, flags),
               support.cpubind.set_thisproc_cpubind or support.cpubind.set_thisthread_cpubind)
    result_get('Get  this singlethreaded process',
               lambda: topo.get_cpubind(flags),
               cpuset,
               support.cpubind.get_thisproc_cpubind or support.cpubind.get_thisthread_cpubind)
    result_set('Bind this thread',
               lambda: topo.set_cpubind(cpuset, flags | hwloc.CPUBIND_THREAD),
               support.cpubind.set_thisthread_cpubind)
    result_get('Get  this thread',
               lambda: topo.get_cpubind(flags | hwloc.CPUBIND_THREAD),
               cpuset,
               support.cpubind.get_thisthread_cpubind)
    result_set('Bind this whole process',
               lambda: topo.set_cpubind(cpuset, flags | hwloc.CPUBIND_PROCESS),
               support.cpubind.set_thisproc_cpubind)
    result_get('Get  this whole process',
               lambda: topo.get_cpubind(flags | hwloc.CPUBIND_PROCESS),
               cpuset,
               support.cpubind.get_thisproc_cpubind)
    result_set('Bind whole process',
               lambda: topo.set_proc_cpubind(
                   os.getpid(), cpuset, flags | hwloc.CPUBIND_PROCESS),
               support.cpubind.set_proc_cpubind)
    result_get('Get  whole process',
               lambda: topo.get_proc_cpubind(
                   os.getpid(), flags | hwloc.CPUBIND_PROCESS),
               cpuset,
               support.cpubind.get_proc_cpubind)
    result_set('Bind process',
               lambda: topo.set_proc_cpubind(os.getpid(), cpuset, flags),
               support.cpubind.set_proc_cpubind)
    result_get('Get  process',
               lambda: topo.get_proc_cpubind(os.getpid(), flags),
               cpuset,
               support.cpubind.get_proc_cpubind)
#
# TODO: thread test. How to do pthread in python?
#
    print()


def testmem(nodeset, policy, flags, expected):
    area_size = 1024
    result_set('Bind this singlethreaded process memory',
               lambda: topo.set_membind(nodeset, policy, flags),
               (support.membind.set_thisproc_membind or support.membind.set_thisthread_membind) and expected)
    result_get('Get  this singlethreaded process memory',
               lambda: topo.get_membind(flags)[0],
               nodeset,
               (support.membind.get_thisproc_membind or support.membind.get_thisthread_membind) and expected)
    result_set('Bind this thread memory',
               lambda: topo.set_membind(
                   nodeset, policy, flags | hwloc.MEMBIND_THREAD),
               support.membind.set_thisproc_membind and expected)
    result_get('Get  this thread memory',
               lambda: topo.get_membind(flags | hwloc.MEMBIND_THREAD)[0],
               nodeset,
               support.membind.get_thisproc_membind and expected)
    result_set('Bind this whole process memory',
               lambda: topo.set_membind(
                   nodeset, policy, flags | hwloc.MEMBIND_PROCESS),
               support.membind.set_thisproc_membind and expected)
    result_get('Get  this whole process memory',
               lambda: topo.get_membind(flags | hwloc.MEMBIND_PROCESS)[0],
               nodeset,
               support.membind.get_thisproc_membind and expected)
    result_set('Bind process memory',
               lambda: topo.set_proc_membind(
                   os.getpid(), nodeset, policy, flags),
               support.membind.set_proc_membind and expected)
    result_get('Get  process memory',
               lambda: topo.get_proc_membind(os.getpid(), flags)[0],
               nodeset,
               support.membind.get_proc_membind and expected)
    temp = ctypes.create_string_buffer(area_size)
    result_set('Bind area',
               lambda: topo.set_area_membind(int(ctypes.addressof(
                   temp)), ctypes.sizeof(temp), nodeset, policy, flags),
               support.membind.set_area_membind and expected)
    result_get('Get  area',
               lambda: topo.get_area_membind(
                   int(ctypes.addressof(temp)), ctypes.sizeof(temp), flags)[0],
               nodeset,
               support.membind.get_area_membind and expected)
    if not flags & hwloc.MEMBIND_MIGRATE:
        area = result_set('Alloc bound area',
                          lambda: topo.alloc_membind(
                              area_size, nodeset, policy, flags),
                          (support.membind.alloc_membind and expected) or not flags & hwloc.MEMBIND_STRICT)
        if area:
            b = ctypes.cast(area, ctypes.POINTER(ctypes.c_ubyte))
            _a = b[area_size - 1]  # touch it to confirm it worked
            result_get('Get   bound area',
                       lambda: topo.get_area_membind(
                           area, area_size, flags)[0],
                       nodeset,
                       support.membind.alloc_membind and expected)
            topo.free(area, area_size)
    print()


def testmem2(set1, flags):  # unfinished
    print('  default')
    testmem(set1, hwloc.MEMBIND_DEFAULT, flags, 1)
    print('  firsttouch')
    testmem(set1, hwloc.MEMBIND_FIRSTTOUCH, flags,
            support.membind.firsttouch_membind)
    print('  bound')
    testmem(set1, hwloc.MEMBIND_BIND, flags, support.membind.bind_membind)
    print('  interleave')
    testmem(set1, hwloc.MEMBIND_INTERLEAVE, flags,
            support.membind.interleave_membind)
    print('  nexttouch')
    testmem(set1, hwloc.MEMBIND_NEXTTOUCH, flags,
            support.membind.nexttouch_membind)


def testmem3(set1):
    testmem2(set1, 0)
    print('now strict\n')
    testmem2(set1, hwloc.MEMBIND_STRICT)
    print('now migrate\n')
    testmem2(set1, hwloc.MEMBIND_MIGRATE)
    print('now strictly migrate')
    testmem2(set1, hwloc.MEMBIND_STRICT | hwloc.MEMBIND_MIGRATE)


topo = hwloc.Topology()
topo.load()

support = topo.support

obj = topo.root_obj
set1 = obj.cpuset.dup()

while obj.cpuset == set1:
    if not obj.arity:
        break
    obj = obj.first_child

print('system set is', set1)

test(set1, 0)
print('now strict')
test(set1, hwloc.CPUBIND_STRICT)

set1 = obj.cpuset.dup()
print('obj set is', set1)

test(set1, 0)
print('now strict')
test(set1, hwloc.CPUBIND_STRICT)

set1.singlify()
print('singlified to', set1)

test(set1, 0)
print('now strict')
test(set1, hwloc.CPUBIND_STRICT)

# TODO: I don't know if it is reasonable to do
# in python anyway.

print('\nmemory tests\n\ncomplete node set')
set1 = topo.root_obj.cpuset.dup()
print('i.e. cupset', set1)
testmem3(set1)

obj = topo.get_obj_by_type(hwloc.OBJ_NUMANODE, 0)
assert obj is not None
set1 = obj.cpuset.dup()
print('cpuset set is', str(set1))
testmem3(set1)

obj = topo.get_obj_by_type(hwloc.OBJ_NUMANODE, 1)
if obj is not None:
    set1 |= obj.cpuset
    print('cpuset set is', str(set1))
    testmem3(set1)
