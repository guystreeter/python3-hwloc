#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2017-2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This tests the python hwloc_distrib implementation
#

import hwloc

topology = hwloc.Topology()
topology.set_synthetic('node:2 core:8 pu:1')
topology.load()

cpusets = topology.distrib((topology.root_obj, ), 16, hwloc.INT_MAX, 0)
assert len(cpusets) == 16

for pos, set_ in enumerate(cpusets):
    assert set_.weight() == 1
    assert pos in set_

cpusets = topology.distrib((topology.root_obj, ), 8, hwloc.INT_MAX, 0)
assert len(cpusets) == 8

for pos, set_ in enumerate(cpusets):
    b = hwloc.Bitmap()
    b.set_range(pos * 2, pos * 2 + 1)
    assert set_ == b

cpusets = topology.distrib((topology.root_obj, ), 15, hwloc.INT_MAX, 0)
assert len(cpusets) == 15

for pos, set_ in enumerate(cpusets[:-1]):
    assert set_.weight() == 1
    assert pos in set_

set_ = cpusets[14]
assert set_.weight() == 2
assert set_.isset(14)
assert set_.isset(15)
