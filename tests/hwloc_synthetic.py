#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_synthetic.c
# from the hwloc package.
#

import hwloc
import errno


def check_level(topology: hwloc.Topology, depth: int, width: int, arity: int):
    assert topology.get_nbobjs_by_depth(depth) == width
    for j in range(width):
        obj = topology.get_obj_by_depth(depth, j)
        assert obj is not None
        assert obj.arity == arity


# check a synthetic topology
topology = hwloc.Topology()
topology.set_synthetic('pack:2 numa:3 l2:4 core:5 pu:6')
topology.load()

assert topology.memory_parents_depth == 2

# internal checks

topology.check()

# local checks
assert topology.depth == 6

check_level(topology, 0, 1, 2)
check_level(topology, 1, 2, 3)
check_level(topology, 2, 6, 4)
check_level(topology, 3, 24, 5)
check_level(topology, 4, 120, 6)
check_level(topology, 5, 720, 0)
check_level(topology, hwloc.TYPE_DEPTH_NUMANODE, 6, 0)

buffer_ = topology.export_synthetic(0)
assert len(buffer_) == 83
assert buffer_ == (
    'Package:2 Group:3 [NUMANode(memory=1073741824)] '
    'L2Cache:4(size=4194304) Core:5 PU:6'
)

assert topology.memory_parents_depth == 2

buffer_ = topology.export_synthetic(
        hwloc.TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_EXTENDED_TYPES |
        hwloc.TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_ATTRS |
        hwloc.TOPOLOGY_EXPORT_SYNTHETIC_FLAG_V1
    )
assert len(buffer_) == 47
assert buffer_ == 'Socket:2 Group:3 NUMANode:1 Cache:4 Core:5 PU:6'

del topology


topology: hwloc.Topology = hwloc.Topology()
topology.set_type_filter(hwloc.OBJ_L1ICACHE, hwloc.TYPE_FILTER_KEEP_ALL)
topology.set_synthetic(
    'pack:2(indexes=3,5) numa:2(memory=256GB indexes=pack) l3u:1(size=20mb) '
    'l2:2 l1i:1(size=16kB) l1dcache:2 core:1 pu:2(indexes=l2)')
topology.load()

assert topology.memory_parents_depth == 2

buffer_ = topology.export_synthetic(0)
assert len(buffer_) == 181
assert buffer_ == (
    'Package:2 '
    'L3Cache:2(size=20971520) '
    '[NUMANode(memory=274877906944 indexes=2*2:1*2)] '
    'L2Cache:2(size=4194304) '
    'L1iCache:1(size=16384) '
    'L1dCache:2(size=32768) '
    'Core:1 '
    'PU:2(indexes=4*8:1*4)'
)

assert topology.get_obj_by_type(hwloc.OBJ_PACKAGE, 1).os_index == 5
assert topology.get_obj_by_type(hwloc.OBJ_NUMANODE, 1).os_index == 2
assert topology.get_obj_by_type(hwloc.OBJ_PU, 12).os_index == 3
assert topology.get_obj_by_type(hwloc.OBJ_PU, 13).os_index == 11
assert topology.get_obj_by_type(hwloc.OBJ_PU, 14).os_index == 19
assert topology.get_obj_by_type(hwloc.OBJ_PU, 15).os_index == 27
assert topology.get_obj_by_type(hwloc.OBJ_PU, 16).os_index == 4
assert topology.get_obj_by_type(hwloc.OBJ_PU, 17).os_index == 12
assert topology.get_obj_by_type(hwloc.OBJ_PU, 18).os_index == 20
assert topology.get_obj_by_type(hwloc.OBJ_PU, 19).os_index == 28

del topology


topology = hwloc.Topology()
topology.set_synthetic('pack:2 core:2 pu:2(indexes=0,4,2,6,1,5,3,7)')
topology.load()

assert topology.memory_parents_depth == 0

buffer_ = topology.export_synthetic(0)
assert len(buffer_) == 72
assert buffer_ == '[NUMANode(memory=1073741824)] Package:2 Core:2 PU:2(indexes=4*2:2*2:1*2)'

assert topology.get_obj_by_type(hwloc.OBJ_PU, 0).os_index == 0
assert topology.get_obj_by_type(hwloc.OBJ_PU, 1).os_index == 4
assert topology.get_obj_by_type(hwloc.OBJ_PU, 2).os_index == 2
assert topology.get_obj_by_type(hwloc.OBJ_PU, 3).os_index == 6
assert topology.get_obj_by_type(hwloc.OBJ_PU, 4).os_index == 1
assert topology.get_obj_by_type(hwloc.OBJ_PU, 5).os_index == 5
assert topology.get_obj_by_type(hwloc.OBJ_PU, 6).os_index == 3
assert topology.get_obj_by_type(hwloc.OBJ_PU, 7).os_index == 7

del topology


topology = hwloc.Topology()
topology.set_synthetic('pack:2 numa:2 core:1 pu:2(indexes=0,4,2,6,1,3,5,7)')
topology.load()

assert topology.memory_parents_depth == 2

buffer_ = topology.export_synthetic(0)
assert len(buffer_) == 76
assert buffer_ == (
        'Package:2 '
        'Core:2 '
        '[NUMANode(memory=1073741824)] '
        'PU:2(indexes=0,4,2,6,1,3,5,7)'
    )

assert topology.get_obj_by_type(hwloc.OBJ_PU, 0).os_index == 0
assert topology.get_obj_by_type(hwloc.OBJ_PU, 1).os_index == 4
assert topology.get_obj_by_type(hwloc.OBJ_PU, 2).os_index == 2
assert topology.get_obj_by_type(hwloc.OBJ_PU, 3).os_index == 6
assert topology.get_obj_by_type(hwloc.OBJ_PU, 4).os_index == 1
assert topology.get_obj_by_type(hwloc.OBJ_PU, 5).os_index == 3
assert topology.get_obj_by_type(hwloc.OBJ_PU, 6).os_index == 5
assert topology.get_obj_by_type(hwloc.OBJ_PU, 7).os_index == 7

del topology


topology = hwloc.Topology()
topology.set_synthetic(
        'pack:2 '
        '[numa(memory=1GB)] '
        '[numa(memory=1MB)] '
        'core:2 '
        '[numa(indexes=8,7,5,6,4,3,1,2)] '
        'pu:4'
    )
topology.load()

assert topology.memory_parents_depth == hwloc.TYPE_DEPTH_MULTIPLE

buffer_ = topology.export_synthetic(0)
assert len(buffer_) == 114
assert buffer_ == (
        'Package:2'
        ' [NUMANode(memory=1073741824)]'
        ' [NUMANode(memory=1048576)]'
        ' Core:2'
        ' [NUMANode(indexes=8,7,5,6,4,3,1,2)]'
        ' PU:4'
    )

try:
    buffer_ = topology.export_synthetic(hwloc.TOPOLOGY_EXPORT_SYNTHETIC_FLAG_V1)
    assert False
except OSError as err:
    assert err.errno == errno.EINVAL

buffer_ = topology.export_synthetic(
        hwloc.TOPOLOGY_EXPORT_SYNTHETIC_FLAG_IGNORE_MEMORY
    )
assert len(buffer_) == 21
assert buffer_ == ('Package:2 Core:2 PU:4')
