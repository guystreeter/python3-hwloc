#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019-2020 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This tests the python linux helper implementation
#

import hwloc

cpuset = hwloc.Topology.linux_read_path_as_cpumask(
    '/sys/devices/system/cpu/cpu0/topology/thread_siblings')

print(cpuset)
