#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/gl.c from the
# hwloc package.
#

from __future__ import print_function
import hwloc
import re

topology = hwloc.Topology()

# Flags used for loading the I/O devices, bridges and their relevant info
topology.set_io_types_filter(hwloc.TYPE_FILTER_KEEP_IMPORTANT)

topology.load()

# Case 1: Get the cpusets of the sockets connecting the PCI devices in the
# topology
nr_pcidev = topology.get_nbobjs_by_type(hwloc.OBJ_PCI_DEVICE)
for pcidev in topology.objs_by_type(hwloc.OBJ_PCI_DEVICE):
    parent = topology.get_non_io_ancestor_obj(pcidev)
    # Print the cpuset corresponding to each pci device
    print(str(parent.cpuset), '|', pcidev.name)
    nr_pcidev -= 1

assert nr_pcidev == 0

# Case 2: Get the number of connected GPUs in the topology and their
# attached displays

nr_osdev = topology.get_nbobjs_by_type(hwloc.OBJ_OS_DEVICE)
firstgpu = None
lastgpu = None
nr_gpus = 0
for osdev in topology.objs_by_type(hwloc.OBJ_OS_DEVICE):
    backend = osdev.get_info_by_name('Backend')
    model = osdev.get_info_by_name('GPUModel')

    port, device = osdev.gl_get_display()
    if port is not None:
        assert backend == 'GL'
        assert osdev.attr.osdev.type == hwloc.OBJ_OSDEV_GPU

        if not firstgpu:
            firstgpu = osdev
        lastgpu = osdev
        print('GPU #%u (%s) is connected to DISPLAY:%u.%u' %
              (nr_gpus, model, port, device))
        nr_gpus += 1
    else:
        if backend:
            assert backend != 'GL'

scan = re.compile(':(\\d+).(\\d+)')

# Case 3: Get the first GPU connected to a valid display, specified by its
# port and device
if firstgpu:
    m = scan.match(firstgpu.name)
    assert m
    port = int(m.group(1))
    device = int(m.group(2))
    osdev = topology.gl_get_display_osdev_by_port_device(port, device)
    assert osdev == firstgpu
    pcidev = osdev.parent
    parent = topology.get_non_io_ancestor_obj(pcidev)
    print('GPU %s (PCI %04x:%02x:%02x.%01x) is connected to DISPLAY:%u.%u close to %s' % (osdev.name, pcidev.attr.pcidev.domain,
                                                                                          pcidev.attr.pcidev.bus, pcidev.attr.pcidev.dev, pcidev.attr.pcidev.func, port, device, str(parent.cpuset)))

# Case 4: Get the last GPU connected to a valid display, specified by its name
if lastgpu:
    m = scan.match(lastgpu.name)
    assert m
    port = int(m.group(1))
    device = int(m.group(2))
    osdev = topology.gl_get_display_osdev_by_name(lastgpu.name)
    assert osdev == lastgpu
    pcidev = osdev.parent
    parent = topology.get_non_io_ancestor_obj(pcidev)
    print('GPU %s (PCI %04x:%02x:%02x.%01x) is connected to DISPLAY:%u.%u close to %s' % (osdev.name, pcidev.attr.pcidev.domain,
                                                                                          pcidev.attr.pcidev.bus, pcidev.attr.pcidev.dev, pcidev.attr.pcidev.func, port, device, str(parent.cpuset)))
