#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This tests the python hwloc_topology implementation
#

import hwloc
import tempfile

assert hwloc.get_api_version() >= 0x00020000  # earliest supported

# hwloc_topology is the class representing a topology context.
# getting an instance of it does topology_init() automatically
topo = hwloc.Topology()

# you can then (optionally) do any of:
#  set various type filters
#  set flags
#  set_pid
#  set_synthetic
#  set_xml
#  set_xmlbuffer
#  get_support

# support can also be accessed as a property
support = topo.support

topo.set_synthetic('numa:1 group:6 pack:5 l2:4 core:3 pu:2')

# then load
topo.load()

# check? sure
topo.check()

# depth is a property
assert topo.depth == 6

assert topo.get_depth_type(0) == hwloc.OBJ_MACHINE
assert topo.get_depth_type(5) == hwloc.OBJ_PU

assert topo.get_type_depth(hwloc.OBJ_MACHINE) == 0
assert topo.get_type_depth(hwloc.OBJ_NUMANODE) == hwloc.TYPE_DEPTH_NUMANODE

assert (
    topo.get_type_or_above_depth(hwloc.OBJ_NUMANODE) ==
    hwloc.TYPE_DEPTH_NUMANODE
)
assert (
    topo.get_type_or_below_depth(hwloc.OBJ_NUMANODE) ==
    hwloc.TYPE_DEPTH_NUMANODE
)
assert topo.get_type_or_above_depth(hwloc.OBJ_CACHE_DATA) == 2

assert topo.get_nbobjs_by_depth(0) == 1

assert topo.get_nbobjs_by_type(hwloc.OBJ_PU) == 6 * 5 * 4 * 3 * 2

# is_thissytem is a property
assert not topo.is_thissystem

with tempfile.NamedTemporaryFile(prefix='python_topology.tmpxml') as xmlfile:
    topo.export_xml(xmlfile.name)

print(topo.export_xmlbuffer())

# destroy() is done automatically
