#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_type_depth.c
# from the hwloc package.
#

import hwloc

SYNTHETIC_TOPOLOGY_DESCRIPTION = 'group:2 group:2 core:3 l3:2 l1:2 2'

topo = hwloc.Topology()
topo.set_synthetic(SYNTHETIC_TOPOLOGY_DESCRIPTION)
topo.load()

assert topo.depth == 7

assert topo.get_depth_type(0) == hwloc.OBJ_MACHINE
assert topo.get_depth_type(1) == hwloc.OBJ_GROUP
assert topo.get_depth_type(2) == hwloc.OBJ_GROUP
assert topo.get_depth_type(3) == hwloc.OBJ_CORE
assert topo.get_depth_type(4) == hwloc.OBJ_L3CACHE
assert topo.get_depth_type(5) == hwloc.OBJ_L1CACHE
assert topo.get_depth_type(6) == hwloc.OBJ_PU

assert topo.get_type_depth(hwloc.OBJ_MACHINE) == 0
assert topo.get_type_depth(hwloc.OBJ_CORE) == 3
assert topo.get_type_depth(hwloc.OBJ_PU) == 6

assert topo.get_type_depth(hwloc.OBJ_PACKAGE) == hwloc.TYPE_DEPTH_UNKNOWN
assert topo.get_type_or_above_depth(hwloc.OBJ_PACKAGE) == 2
assert topo.get_type_or_below_depth(hwloc.OBJ_PACKAGE) == 3
assert topo.get_type_depth(hwloc.OBJ_GROUP) == hwloc.TYPE_DEPTH_MULTIPLE
depth = topo.get_type_or_above_depth(hwloc.OBJ_GROUP)
assert depth == hwloc.TYPE_DEPTH_MULTIPLE
depth = topo.get_type_or_below_depth(hwloc.OBJ_GROUP)
assert depth == hwloc.TYPE_DEPTH_MULTIPLE
assert topo.get_type_depth(hwloc.OBJ_L3CACHE) == 4

assert topo.get_type_depth(hwloc.OBJ_NUMANODE) == hwloc.TYPE_DEPTH_NUMANODE
depth = topo.get_type_or_above_depth(hwloc.OBJ_NUMANODE)
assert depth == hwloc.TYPE_DEPTH_NUMANODE
depth = topo.get_type_or_below_depth(hwloc.OBJ_NUMANODE)
assert depth == hwloc.TYPE_DEPTH_NUMANODE

assert topo.get_type_depth(hwloc.OBJ_BRIDGE) == hwloc.TYPE_DEPTH_BRIDGE
assert topo.get_type_or_above_depth(hwloc.OBJ_BRIDGE) == hwloc.TYPE_DEPTH_BRIDGE
assert topo.get_type_or_below_depth(hwloc.OBJ_BRIDGE) == hwloc.TYPE_DEPTH_BRIDGE
assert topo.get_type_depth(hwloc.OBJ_PCI_DEVICE) == hwloc.TYPE_DEPTH_PCI_DEVICE
depth = topo.get_type_or_above_depth(hwloc.OBJ_PCI_DEVICE)
assert depth == hwloc.TYPE_DEPTH_PCI_DEVICE
depth = topo.get_type_or_below_depth(hwloc.OBJ_PCI_DEVICE)
assert depth == hwloc.TYPE_DEPTH_PCI_DEVICE
assert topo.get_type_depth(hwloc.OBJ_OS_DEVICE) == hwloc.TYPE_DEPTH_OS_DEVICE
depth = topo.get_type_or_above_depth(hwloc.OBJ_OS_DEVICE)
assert depth == hwloc.TYPE_DEPTH_OS_DEVICE
depth = topo.get_type_or_below_depth(hwloc.OBJ_OS_DEVICE)
assert depth == hwloc.TYPE_DEPTH_OS_DEVICE

assert topo.get_type_depth(hwloc.OBJ_MISC) == hwloc.TYPE_DEPTH_MISC
assert topo.get_type_or_above_depth(hwloc.OBJ_MISC) == hwloc.TYPE_DEPTH_MISC
assert topo.get_type_or_below_depth(hwloc.OBJ_MISC) == hwloc.TYPE_DEPTH_MISC

assert topo.get_depth_type(hwloc.TYPE_DEPTH_BRIDGE) == hwloc.OBJ_BRIDGE
assert topo.get_depth_type(hwloc.TYPE_DEPTH_PCI_DEVICE) == hwloc.OBJ_PCI_DEVICE
assert topo.get_depth_type(hwloc.TYPE_DEPTH_OS_DEVICE) == hwloc.OBJ_OS_DEVICE
assert topo.get_depth_type(hwloc.TYPE_DEPTH_MISC) == hwloc.OBJ_MISC
assert topo.get_depth_type(hwloc.TYPE_DEPTH_NUMANODE) == hwloc.OBJ_NUMANODE

assert topo.get_type_depth(123) == hwloc.TYPE_DEPTH_UNKNOWN
assert topo.get_type_depth(-14) == hwloc.TYPE_DEPTH_UNKNOWN

try:
    topo.get_depth_type(123)
except hwloc.ArgError:
    pass
try:
    topo.get_depth_type(hwloc.TYPE_DEPTH_UNKNOWN)  # -1
except hwloc.ArgError:
    pass
try:
    topo.get_depth_type(hwloc.TYPE_DEPTH_MULTIPLE)  # -2
except hwloc.ArgError:
    pass
try:
    # special level depth are from -3 to -7
    topo.get_depth_type(-8)
except hwloc.ArgError:
    pass
try:
    topo.get_depth_type(-134)
except hwloc.ArgError:
    pass

assert topo.memory_parents_depth == 0

del topo

for i in range(hwloc.OBJ_TYPE_MIN, hwloc.OBJ_TYPE_MAX):
    assert 1 == (
        hwloc.Obj.type_is_normal(i) +
        hwloc.Obj.type_is_memory(i) +
        hwloc.Obj.type_is_io(i) +
        (1 if i == hwloc.OBJ_MISC else 0)
    )
