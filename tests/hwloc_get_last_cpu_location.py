#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019-2020 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_get_last_cpu_location.c
# from the hwloc package.
#

import hwloc


# check that a bound process execs on a non-empty cpuset included in the
# binding
def check(cpuset, flags):
    print('  binding')
    topology.set_cpubind(cpuset, flags)

    print('  getting cpu location')
    last = topology.get_last_cpu_location(flags)

    assert not last.iszero

    if flags == hwloc.CPUBIND_THREAD:
        print('  checking inclusion')
        assert last in cpuset


def checkall(cpuset):
    if support.cpubind.get_thisthread_last_cpu_location:
        print(' with HWLOC_CPUBIND_THREAD...')
        check(cpuset, hwloc.CPUBIND_THREAD)
    if support.cpubind.get_thisproc_last_cpu_location:
        print(' with HWLOC_CPUBIND_PROCESS...')
        check(cpuset, hwloc.CPUBIND_PROCESS)
    if support.cpubind.get_thisthread_last_cpu_location \
            or support.cpubind.get_thisproc_last_cpu_location:
        print(' with flags 0...')
        check(cpuset, 0)


topology = hwloc.Topology()
topology.load()
support = topology.support

# check at top level
obj = topology.root_obj
checkall(obj.cpuset)

depth = topology.depth
# check at intermediate level if it exists
if depth >= 3:
    print('testing at depth', str((depth - 1) // 2))
    for o in topology.objs_by_depth((depth - 1) // 2):
        checkall(o.cpuset)
# check at bottom level
print('testing at bottom level')
for o in topology.objs_by_type(hwloc.OBJ_PU):
    checkall(o.cpuset)
