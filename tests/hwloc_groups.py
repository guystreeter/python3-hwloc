#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_groups.c
# from the hwloc package.
#

import hwloc
import os

topology = hwloc.Topology()
topology.set_synthetic('pack:4 [numa] pu:4')
topology.load()
root = topology.root_obj
assert topology.depth == 3
# insert a group identical to root, will be merged
group = topology.alloc_group_object()
assert group is not None

# implementation anomally: assigning a bitmap to the cpuset attribute
# of an Obj sets the attr to a *copy* of the bitmap, rather than a reference
# to it. Necessary for making reference counting and cleanup work right.
group.cpuset = root.cpuset
res = topology.insert_group_object(group)

# equality of Obj instances means they both encapsulate the
# same hwloc_obj structure. Note: "obj1 is ob2" does not work for this.
assert res == root
assert topology.depth == 3

# the group Obj instance can go away after insert_group_object().
# the referenence hwloc_obj is handled now by the library
del group

# insert a group identical to a package, will be merged
group = topology.alloc_group_object()
assert group is not None
obj = topology.get_obj_by_type(hwloc.OBJ_PACKAGE, 1)
assert obj is not None
group.cpuset = obj.cpuset
res = topology.insert_group_object(group)
assert res == obj
assert topology.depth == 3
# insert a invalid group of two PUs in different packages, will fail
group = topology.alloc_group_object()
assert group is not None
obj = topology.get_obj_by_type(hwloc.OBJ_PU, 1)
assert obj is not None
group.cpuset = obj.cpuset
obj = topology.get_obj_by_type(hwloc.OBJ_PU, 12)
assert obj is not None
group.cpuset |= obj.cpuset
res = topology.insert_group_object(group)
assert res is None
assert topology.depth == 3
# insert a group of two packages
group = topology.alloc_group_object()
assert group is not None
obj = topology.get_obj_by_type(hwloc.OBJ_PACKAGE, 1)
assert obj is not None
group.cpuset = obj.cpuset
obj = topology.get_obj_by_type(hwloc.OBJ_PACKAGE, 2)
assert obj is not None
group.cpuset |= obj.cpuset
res = topology.insert_group_object(group)
assert res == group
assert topology.depth == 4
# insert a conflict group of two packages by nodeset, will fail
group = topology.alloc_group_object()
assert group is not None
obj = topology.get_obj_by_type(hwloc.OBJ_PACKAGE, 0)
assert obj is not None
group.nodeset = obj.nodeset
obj = topology.get_obj_by_type(hwloc.OBJ_PACKAGE, 2)
assert obj is not None
group.nodeset |= obj.nodeset
res = topology.insert_group_object(group)
assert res is None
# insert a group of three packages by nodeset
group = topology.alloc_group_object()
assert group is not None
obj = topology.get_obj_by_type(hwloc.OBJ_PACKAGE, 0)
assert obj is not None
group.nodeset = obj.nodeset
obj = topology.get_obj_by_type(hwloc.OBJ_PACKAGE, 1)
assert obj is not None
group.nodeset |= obj.nodeset
obj = topology.get_obj_by_type(hwloc.OBJ_PACKAGE, 2)
assert obj is not None
group.nodeset |= obj.nodeset
res = topology.insert_group_object(group)
assert res == group
assert topology.depth == 5

del topology, group, obj

# intensive testing of two grouping cases (2+1 and 2+2+1)

# group 3 numa nodes as 1 group of 2 and 1 on the side
topology = hwloc.Topology()
topology.set_synthetic('node:3 pu:1')
topology.load()
# 3 group at depth 1
assert topology.get_type_depth(hwloc.OBJ_GROUP) == 1
assert topology.get_nbobjs_by_depth(1) == 3
objs = [topology.get_obj_by_type(hwloc.OBJ_PU, i) for i in range(3)]
values = [
    1, 4, 4,
    4, 1, 2,
    4, 2, 1,
]
kind = hwloc.DISTANCES_KIND_MEANS_LATENCY | hwloc.DISTANCES_KIND_FROM_USER
topology.distances_add(objs, values, kind, hwloc.DISTANCES_ADD_FLAG_GROUP)
# 1 groups at depth 1 and 2
assert topology.get_type_depth(hwloc.OBJ_GROUP) == -2
assert topology.get_depth_type(1) == hwloc.OBJ_GROUP
assert topology.get_nbobjs_by_depth(1) == 1
assert topology.get_depth_type(2) == hwloc.OBJ_GROUP
assert topology.get_nbobjs_by_depth(2) == 3
# 3 nodes
depth = topology.get_type_depth(hwloc.OBJ_NUMANODE)
assert depth == hwloc.TYPE_DEPTH_NUMANODE
assert topology.get_nbobjs_by_depth(depth) == 3
# find the root obj
obj = topology.root_obj
assert obj.arity == 2
# check its children
children = list(obj.children)
# first child is a group with PU+NUMA children
child: hwloc.Obj = children[0]
assert child.type == hwloc.OBJ_GROUP
assert child.depth == 2
assert child.arity == 1
assert child.first_child.type == hwloc.OBJ_PU
assert child.memory_arity == 1
assert child.memory_first_child.type == hwloc.OBJ_NUMANODE
# second child is a group with two group children
child = children[1]
assert child.type == hwloc.OBJ_GROUP
assert child.depth == 1
assert child.arity == 2
children = list(child.children)
assert children[0].type == hwloc.OBJ_GROUP
assert children[1].type == hwloc.OBJ_GROUP
assert child.memory_arity == 0
del topology

# group 5 packages as 2 group of 2 and 1 on the side, all of them below
# a common node object
topology = hwloc.Topology()
topology.set_synthetic('node:1 pack:5 pu:1')
topology.load()
objs = [topology.get_obj_by_type(hwloc.OBJ_PACKAGE, i) for i in range(5)]
values = [
    1, 2, 4, 4, 4,
    2, 1, 4, 4, 4,
    4, 4, 1, 4, 4,
    4, 4, 4, 1, 2,
    4, 4, 4, 2, 1,
]
topology.distances_add(objs, values, kind, hwloc.DISTANCES_ADD_FLAG_GROUP)
# 1 node
depth = topology.get_type_depth(hwloc.OBJ_NUMANODE)
assert depth == hwloc.TYPE_DEPTH_NUMANODE
assert topology.get_nbobjs_by_depth(depth) == 1
# 2 groups at depth 1
depth = topology.get_type_depth(hwloc.OBJ_GROUP)
assert topology.get_nbobjs_by_depth(depth) == 2
# 5 packages at depth 2
depth = topology.get_type_depth(hwloc.OBJ_PACKAGE)
assert topology.get_nbobjs_by_depth(depth) == 5
# check root
obj = topology.root_obj
assert obj.arity == 3
assert obj.memory_arity == 1
# check root children
children = list(obj.children)
child = children[0]
assert child.type == hwloc.OBJ_GROUP
assert child.depth == 1
assert child.arity == 2
child = children[1]
assert child.type == hwloc.OBJ_PACKAGE
assert child.depth == 2
assert child.arity == 1
child = children[2]
assert child.type == hwloc.OBJ_GROUP
assert child.depth == 1
assert child.arity == 2
obj = obj.memory_first_child
assert obj.type == hwloc.OBJ_NUMANODE
assert obj.arity == 0
assert obj.memory_arity == 0
del topology

# testing of adding/replacing/removing distance matrices
#  and grouping with/without accuracy

# grouping matrix 4*2*2
values = [None] * 16 * 16
for i in range(16):
    for j in range(16):
        if i == j:
            values[i + 16 * j] = 30
            values[j + 16 * i] = 30
        elif i // 2 == j // 2:
            values[i + 16 * j] = 50
            values[j + 16 * i] = 50
        elif i // 4 == j // 4:
            values[i + 16 * j] = 70
            values[j + 16 * i] = 70
        else:
            values[i + 16 * j] = 90
            values[j + 16 * i] = 90

# default 2*8*1
topology = hwloc.Topology()
topology.set_synthetic('node:2 core:8 pu:1')
topology.load()
assert topology.depth == 4
assert topology.get_nbobjs_by_depth(0) == 1
assert topology.get_nbobjs_by_depth(1) == 2
assert topology.get_nbobjs_by_depth(2) == 16
assert topology.get_nbobjs_by_depth(3) == 16
assert topology.get_nbobjs_by_depth(hwloc.TYPE_DEPTH_NUMANODE) == 2
# group 8cores as 2*2*2
objs = [topology.get_obj_by_type(hwloc.OBJ_CORE, i) for i in range(16)]
topology.distances_add(objs, values, kind, hwloc.DISTANCES_ADD_FLAG_GROUP)
assert topology.depth == 6
assert topology.get_nbobjs_by_depth(0) == 1
assert topology.get_nbobjs_by_depth(1) == 2
assert topology.get_nbobjs_by_depth(2) == 4
assert topology.get_nbobjs_by_depth(3) == 8
assert topology.get_nbobjs_by_depth(4) == 16
assert topology.get_nbobjs_by_depth(hwloc.TYPE_DEPTH_NUMANODE) == 2
del topology

# play with accuracy
values[0] = 29  # diagonal, instead of 3 (0.0333% error)
# smallest group, instead of 5 (0.02% error)
values[1] = 51
values[16] = 52
os.environ['HWLOC_GROUPING_ACCURACY'] = '0.1'  # ok
topology = hwloc.Topology()
topology.set_synthetic('node:2 core:8 pu:1')
topology.load()
objs = [topology.get_obj_by_type(hwloc.OBJ_CORE, i) for i in range(16)]
flags = hwloc.DISTANCES_ADD_FLAG_GROUP | hwloc.DISTANCES_ADD_FLAG_GROUP_INACCURATE
topology.distances_add(objs, values, kind, flags)
assert topology.depth == 6
del topology

os.environ['HWLOC_GROUPING_ACCURACY'] = 'try'  # ok
topology = hwloc.Topology()
topology.set_synthetic('node:2 core:8 pu:1')
topology.load()
objs = [topology.get_obj_by_type(hwloc.OBJ_CORE, i) for i in range(16)]
topology.distances_add(objs, values, kind, flags)
assert topology.depth == 6
del topology

topology = hwloc.Topology()
topology.set_synthetic('node:2 core:8 pu:1')
topology.load()
objs = [topology.get_obj_by_type(hwloc.OBJ_CORE, i) for i in range(16)]
# no inaccurate flag, cannot group
topology.distances_add(objs, values, kind, hwloc.DISTANCES_ADD_FLAG_GROUP)
assert topology.depth == 4
del topology

os.environ['HWLOC_GROUPING_ACCURACY'] = '0.01'  # too small, cannot group
topology = hwloc.Topology()
topology.set_synthetic('node:2 core:8 pu:1')
topology.load()
objs = [topology.get_obj_by_type(hwloc.OBJ_CORE, i) for i in range(16)]
topology.distances_add(objs, values, kind, flags)
assert topology.depth == 4
del topology

os.environ['HWLOC_GROUPING_ACCURACY'] = '0'  # full accuracy, cannot group
topology = hwloc.Topology()
topology.set_synthetic('node:2 core:8 pu:1')
topology.load()
objs = [topology.get_obj_by_type(hwloc.OBJ_CORE, i) for i in range(16)]
topology.distances_add(objs, values, kind, flags)
assert topology.depth == 4
del topology

# default 2*4*5
topology = hwloc.Topology()
topology.set_synthetic('node:2 core:4 pu:4')
topology.load()
assert topology.depth == 4
assert topology.get_nbobjs_by_depth(0) == 1
assert topology.get_nbobjs_by_depth(1) == 2
assert topology.get_nbobjs_by_depth(2) == 8
assert topology.get_nbobjs_by_depth(3) == 32
assert topology.get_nbobjs_by_depth(hwloc.TYPE_DEPTH_NUMANODE) == 2
# apply useless core distances
objs = [topology.get_obj_by_type(hwloc.OBJ_CORE, i) for i in range(8)]
values = [None] * 8 * 8
for i in range(8):
    for j in range(8):
        if i == j:
            values[i+8*j] = 1
            values[j+8*i] = 1
        elif i // 4 == j // 4:
            values[i+8*j] = 4
            values[j+8*i] = 4
        else:
            values[i+8*j] = 8
            values[j+8*i] = 8
topology.distances_add(objs, values, kind, hwloc.DISTANCES_ADD_FLAG_GROUP)
assert topology.depth == 4
# group 4cores as 2*2
objs = [topology.get_obj_by_type(hwloc.OBJ_CORE, i) for i in range(8)]
value = [None] * 8 * 8
for i in range(8):
    for j in range(8):
        if i == j:
            values[i+8*j] = 1
            values[j+8*i] = 1
        elif i // 2 == j // 2:
            values[i+8*j] = 4
            values[j+8*i] = 4
        else:
            values[i+8*j] = 8
            values[j+8*i] = 8
topology.distances_add(objs, values, kind, hwloc.DISTANCES_ADD_FLAG_GROUP)
assert topology.depth == 5
assert topology.get_nbobjs_by_depth(0) == 1
assert topology.get_nbobjs_by_depth(1) == 2
assert topology.get_nbobjs_by_depth(2) == 4
assert topology.get_nbobjs_by_depth(3) == 8
assert topology.get_nbobjs_by_depth(4) == 32
assert topology.get_nbobjs_by_depth(hwloc.TYPE_DEPTH_NUMANODE) == 2
# group 4PUs as 2*2
objs = [topology.get_obj_by_type(hwloc.OBJ_PU, i) for i in range(32)]
values = [None] * 32 * 32
for i in range(32):
    for j in range(32):
        if i == j:
            values[i+32*j] = 1
            values[j+32*i] = 1
        elif i // 2 == j // 2:
            values[i+32*j] = 4
            values[j+32*i] = 4
        else:
            values[i+32*j] = 8
            values[j+32*i] = 8
topology.distances_add(objs, values, kind, hwloc.DISTANCES_ADD_FLAG_GROUP)
assert topology.depth == 6
assert topology.get_nbobjs_by_depth(0) == 1
assert topology.get_nbobjs_by_depth(1) == 2
assert topology.get_nbobjs_by_depth(2) == 4
assert topology.get_nbobjs_by_depth(3) == 8
assert topology.get_nbobjs_by_depth(4) == 16
assert topology.get_nbobjs_by_depth(5) == 32
assert topology.get_nbobjs_by_depth(hwloc.TYPE_DEPTH_NUMANODE) == 2
del topology
