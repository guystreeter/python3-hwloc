#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/hwloc_get_closest_objs.c
# from the hwloc package.
#

import hwloc

topo = hwloc.Topology()

topo.set_synthetic('2 3 4 5')

topo.load()

depth = topo.depth

# get the last object of the last level
numprocs = topo.get_nbobjs_by_depth(depth - 1)
last = topo.get_obj_by_depth(depth - 1, numprocs - 1)

# get the array of closest objects
closest = topo.get_closest_objs(last, numprocs)
assert closest

found = len(closest)
print('looked for %u closest entries, found %u' % (numprocs, found))
assert found == numprocs - 1

# NOTE: equality means they are physically the same structure
# check first found is closest
assert closest[0] == topo.get_obj_by_depth(depth - 1, numprocs - 5)
# check some other expected positions:
#   last of the first half
assert closest[
    found - 1] == topo.get_obj_by_depth(depth - 1, 1 * 3 * 4 * 5 - 1)
#  last of second third of second half
assert closest[found // 2 - 1] ==\
    topo.get_obj_by_depth(depth - 1, 1 * 3 * 4 * 5 + 2 * 4 * 5 - 1)
#  last of third quarter of third third of second half
assert closest[found // 2 // 3 - 1] == topo.get_obj_by_depth(
    depth - 1, 1 * 3 * 4 * 5 + 2 * 4 * 5 + 3 * 5 - 1)

# get ancestor of last and less close object
ancestor = hwloc.Obj.get_common_ancestor(last, closest[found - 1])
assert last.is_in_subtree(ancestor)
assert closest[found - 1].is_in_subtree(ancestor)
assert ancestor == topo.root_obj  # == means physically the same structure
print('ancestor type %u depth %u number %u is system level' %
      (ancestor.type, ancestor.depth, ancestor.logical_index))
