#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_insert_misc.c
# from the hwloc package.
#

import hwloc

topology = hwloc.Topology()
topology.set_synthetic('NUMA:2 pack:2 core:2 pu:2')
topology.set_type_filter(hwloc.OBJ_MISC, hwloc.TYPE_FILTER_KEEP_ALL)
topology.load()
topology.check()
# Misc below root
obj = topology.root_obj
obj = topology.insert_misc_object(obj, 'bewlow root')
assert obj is not None
# Misc below previous Misc
obj = topology.insert_misc_object(obj, 'below Misc below root')
assert obj is not None
# Misc below last NUMA node
obj = topology.get_obj_by_type(hwloc.OBJ_NUMANODE, 1)
assert obj is not None
obj = topology.insert_misc_object(obj, 'below last NUMA')
assert obj is not None
# Misc below last Package
obj = topology.get_obj_by_type(hwloc.OBJ_PACKAGE, 3)
assert obj is not None
obj = topology.insert_misc_object(obj, 'below last Package')
assert obj is not None
# Misc below last Core
obj = topology.get_obj_by_type(hwloc.OBJ_CORE, 7)
assert obj is not None
obj = topology.insert_misc_object(obj, 'below last Core')
assert obj is not None
# Misc below first PU
obj = topology.get_obj_by_type(hwloc.OBJ_PU, 0)
assert obj is not None
obj = topology.insert_misc_object(obj, 'below first PU')
assert obj is not None
topology.check()
# restrict it to only 3 Packages node without dropping Misc objects
cpuset = topology.complete_cpuset.dup()
obj = topology.get_obj_by_type(hwloc.OBJ_PACKAGE, 3)
assert obj is not None
cpuset = cpuset.andnot(obj.cpuset)
topology.restrict(cpuset, hwloc.RESTRICT_FLAG_ADAPT_MISC)
topology.check()

# check that export/reimport/export gives same export buffer
buf1 = topology.export_xmlbuffer()
reload = hwloc.Topology()
reload.set_xmlbuffer(buf1)
reload.set_type_filter(hwloc.OBJ_MISC, hwloc.TYPE_FILTER_KEEP_ALL)
reload.load()
reload.check()
buf2 = reload.export_xmlbuffer()
assert buf1 == buf2
del buf2, reload

# build another restricted topology manually without Packages
topology2 = hwloc.Topology()
# must keep the same topology string to avoid SyntheticDescription info difference
topology2.set_synthetic('NUMA:2 pack:2 core:2 pu:2')
topology2.set_type_filter(hwloc.OBJ_PACKAGE, hwloc.TYPE_FILTER_KEEP_NONE)
topology2.set_type_filter(hwloc.OBJ_MISC, hwloc.TYPE_FILTER_KEEP_ALL)
topology2.load()
topology2.check()
topology2.restrict(cpuset, hwloc.RESTRICT_FLAG_ADAPT_MISC)

# reimport without Packages and Misc, and check they are equal
reload = hwloc.Topology()
reload.set_xmlbuffer(buf1)
reload.set_type_filter(hwloc.OBJ_PACKAGE, hwloc.TYPE_FILTER_KEEP_NONE)
reload.load()
diff = reload.diff_build(topology2)
assert diff[0] is None
del reload

# re-add some Misc now
# Misc below root
obj = topology2.root_obj
obj = topology2.insert_misc_object(obj, 'bewlow root')
assert obj is not None
# Misc below previous Misc
obj = topology2.insert_misc_object(obj, 'below Misc below root')
assert obj is not None
# Misc below last NUMA node
obj = topology2.get_obj_by_type(hwloc.OBJ_NUMANODE, 1)
assert obj is not None
topology2.insert_misc_object(obj, 'below last NUMA')
# Misc below parent Group of this NUMA node (where Package and Core Misc will end up)
assert obj.parent.type == hwloc.OBJ_GROUP
topology2.insert_misc_object(obj.parent, 'below last Package')
obj = topology2.insert_misc_object(obj.parent, 'below last Core')
assert obj is not None
# Misc below first PU
obj = topology2.get_obj_by_type(hwloc.OBJ_PU, 0)
assert obj is not None
obj = topology2.insert_misc_object(obj, 'below first PU')
assert obj is not None

# reimport without Packages and check they are equal
reload = hwloc.Topology()
reload.set_xmlbuffer(buf1)
reload.set_type_filter(hwloc.OBJ_PACKAGE, hwloc.TYPE_FILTER_KEEP_NONE)
reload.set_type_filter(hwloc.OBJ_MISC, hwloc.TYPE_FILTER_KEEP_ALL)
reload.load()
diff = reload.diff_build(topology2)
assert diff[0] is None
del reload

del buf1, topology, topology2, cpuset
