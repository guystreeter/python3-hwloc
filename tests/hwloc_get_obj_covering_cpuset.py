#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_get_obj_covering_cpuset.c
# from the hwloc package.
#

import hwloc
import sys

SYNTHETIC_TOPOLOGY_DESCRIPTION = '6 5 4 3 2'  # 736bits wide topology

GIVEN_CPUSET_STRING = '0x0,0x0fff,0xf0000000'
EXPECTED_CPUSET_STRING = '0x0000ffff,0xff000000'
# first and last(735th) bit set
GIVEN_LARGESPLIT_CPUSET_STRING = '0x8000,,,,,,,,,,,,,,,,,,,,,,0x1'
# 736th bit is too large for the 720-wide topology
GIVEN_TOOLARGE_CPUSET_STRING = '0x10000,,,,,,,,,,,,,,,,,,,,,,0x0'

topo = hwloc.Topology()
topo.set_synthetic(SYNTHETIC_TOPOLOGY_DESCRIPTION)
topo.load()
set1 = hwloc.Bitmap.alloc(GIVEN_CPUSET_STRING)

obj = topo.get_obj_covering_cpuset(set1)

assert obj
print('found covering object type %s covering cpuset %s' %
      (obj.type_string, GIVEN_CPUSET_STRING), file=sys.stderr)
assert set1 in obj.cpuset

print('covering object of %s is %s, expected %s' % (GIVEN_CPUSET_STRING,
                                                    str(obj.cpuset),
                                                    EXPECTED_CPUSET_STRING),
      file=sys.stderr)
assert EXPECTED_CPUSET_STRING == str(obj.cpuset)
assert obj.cpuset == hwloc.Bitmap.alloc(EXPECTED_CPUSET_STRING)

set1.sscanf(GIVEN_LARGESPLIT_CPUSET_STRING)
obj = topo.get_obj_covering_cpuset(set1)
assert obj == topo.root_obj
print('found system as covering object of first+last cpus cpuset',
      GIVEN_LARGESPLIT_CPUSET_STRING, file=sys.stderr)

set1.sscanf(GIVEN_TOOLARGE_CPUSET_STRING)
obj = topo.get_obj_covering_cpuset(set1)
assert not obj
print('found no covering object for too-large cpuset',
      GIVEN_TOOLARGE_CPUSET_STRING, file=sys.stderr)
