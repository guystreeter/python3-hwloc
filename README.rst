====================================
Python3 bindings for hwloc version 2
====================================

**NOTICE: UNDER CONSTRUCTION**

I have retired and am no longer maintaining or developing this.
I could work on it more if anyone is interested in using it.

If there is someone who would like to take it over, please contact me.

For Python3 on Fedora:

.. code:: bash

   dnf copr enable streeter/python-hwloc
   dnf install python3-hwloc
   # try it!
   python3 -c "import hwloc;print(hwloc.version_string())"

For other RPM-based distros, install python3-libnuma from
'https://gitlab.com/guystreeter/python-libnuma', and then clone the gitlab tree at
``https://gitlab.com/guystreeter/python3-hwloc.git`` and then 'make rpm' at the top level.
Then install the ``.rpm`` package found in the ``rpm/RPMS`` folder.

For Python3 on Debian/Ubuntu:

Install `python3-libnuma` from 'https://gitlab.com/guystreeter/python-libnuma', and then

.. code:: bash

   sudo apt-get update
   sudo apt-get install git build-essential libpython3.6-dev \
      libnuma-dev debhelper dh-python python-all python-setuptools python3-all \
        python3-setuptools cython3 libhwloc-dev python3-babel \
        libibverbs1 libibverbs-dev
   git clone https://gitlab.com/guystreeter/python3-hwloc.git
   cd python3-hwloc
   make deb

Then install the ``.deb`` package found in the ``deb_builddir`` folder.
Try it out with ``python3 -c "import hwloc;print(hwloc.version_string())"``
