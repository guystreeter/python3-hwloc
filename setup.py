#!/usr/bin/env python3

#
# Copyright 2019-2020 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

from typing import List
import sys
import os
import subprocess

from setuptools import Command, setup
from distutils.command.build import build
from distutils.extension import Extension
from Cython.Distutils import build_ext


if sys.version_info[0] == 3:
    _package_name = 'python3-hwloc'
    _pythonver = 'python3'
else:
    raise Exception('Python3 only')

__author = 'Guy Streeter <guy.streeter@gmail.com>'
__author_email = 'guy.streeter@gmail.com'
__license = 'GPLv2+'
__description = 'python bindings for hwloc V2'
# pip doesn't want dashes in version numbers, so we put
# a ".0." in here where there's a "-" elsewhere.
__version = '3.1.0.0.2.2.0'
__URL = 'https://gitlab.com/guystreeter/python3-hwloc'
__classifiers = [
    'Environment :: Other Environment',
    'Development Status :: 5 - Production/Stable',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',
    'Operating System :: POSIX :: Linux',
    'Programming Language :: Python',
    'Topic :: System :: Systems Administration',
    ]
try:
    with open('README.rst') as rfile:
        __long_description = rfile.read()
except Exception:
    __long_description = __description

datadir = os.environ.get('DATADIR', 'share')
docdir = os.path.join(datadir, 'doc', _package_name)
licdir = os.environ.get('LICENSEDIR',
                        os.path.join(datadir, 'doc', _package_name))

data_files = [
    (licdir, ['COPYING',
              'LICENSE',
              ]),
    (docdir, ['doc/hwloc-hello.py',
              'doc/guide.md',
              'doc/guide.html',
              ]),
]


class build_doc(Command):
    description = 'build guide.pdf and guide.html'
    user_options: List[str] = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            subprocess.call(
                ['/usr/bin/pandoc', '-o', 'doc/guide.html', 'doc/guide.md'])
            subprocess.call(
                ['/usr/bin/pandoc', '-o', 'doc/guide.pdf', '--toc', '-V',
                 'fontfamily:utopia', 'doc/guide.md'])
        except Exception:
            print(
                'pandoc and texlive are required to build documentation',
                file=sys.stderr)


class all_build(build):
    sub_commands = [
        ('build_ext', None),
        ] + build.sub_commands


setup(
    name=_package_name,
    version=__version,
    description=__description,
    author=__author,
    author_email=__author_email,
    license=__license,
    classifiers=__classifiers,
    long_description=__long_description,
    url=__URL,
    install_requires=[_pythonver+'-libnuma'],
    packages=['hwloc'],
    package_dir={'': 'src'},
    data_files=data_files,
    cmdclass={
        'build': all_build,
        'build_doc': build_doc,
        'build_ext': build_ext,
        },
    ext_modules=[
        Extension(
            'hwloc.chwloc',
                ['src/chwloc.pyx'],
            libraries=['hwloc', 'numa']),
        Extension(
            'hwloc.linuxsched',
            ['src/linuxsched.pyx'])])
