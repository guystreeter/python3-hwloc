#
# Copyright 2019-2020 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#


%global source_date_epoch_from_changelog 1

%{!?_licensedir:%global license %doc}

%global debug_package %{nil}
%global _privatelibs chwloc|linuxsched
%global __provides_exclude ^(%{_privatelibs})\\.so

%global _hwloc_version 2.2.0

Name:           python3-hwloc
Version:        3.1.0
Release:        %{_hwloc_version}%{?dist}
Summary:        Python 3 bindings for hwloc version 2

Group:          Development/Languages
License:        GPLv2+
URL:            https://gitlab.com/guystreeter/python3-hwloc
Source0:        python3-hwloc-%{version}-%{_hwloc_version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  pandoc, texlive
BuildRequires:  python3, python3-devel, python3-Cython, gcc
BuildRequires:  hwloc-devel = %{_hwloc_version} libibverbs-devel numactl-devel

%ifnarch s390 s390x %{arm}
Requires:       python3-libnuma
%endif

%description
Python 3 bindings for hwloc version 2, The Portable Hardware Locality package

%prep
%setup -q -n python3-hwloc-%{version}-%{_hwloc_version}

%{__python3} setup.py build
strip build/lib*/hwloc/*.so
chmod 0755 build/lib*/hwloc/*.so

%install
%if 0%{?_licensedir:1}
 LICENSEDIR=%{_licensedir}/python3-hwloc
 export LICENSEDIR
%endif
%{__python3} setup.py install --skip-build --root %{buildroot}

%files -n python3-hwloc
%license COPYING
%license LICENSE
%{_docdir}/python3-hwloc
%{python3_sitearch}/hwloc
%{python3_sitearch}/*egg-info

%changelog
* Tue Dec  1 2020 Guy Streeter <guy.streeter@gmail.com> - 3.1.0-2.2.0
- various fixes
- hwloc 2.2.0

* Fri Jan 17 2020 Guy Streeter <guy.streeter@gmail.com> - 3.0.1-2.0.4
- remove online_cpuset

* Thu Dec 19 2019 Guy Streeter <guy.streeter@gmail.com> - 3.0-2.0.4
- first packaged build. version 3.0 because the last hwloc version 1
- package was version 2.x

* Mon Mar 19 2018 Guy Streeter <guy.streeter@gmail.com> - 0.1-2.0
- first pass
